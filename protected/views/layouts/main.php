<?php
if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
	$this->redirect("index.php?r=site/login");
}
  $nik  = YII::app()->session['nik'];
  $user = new User();
  $tl   = new Tl();
  $user = new User();
  $f    = $tl->isTl($nik);
  $aso  = $user->IsAso($nik);
  $ho   = $user->IsHo($nik);

  $menu_ex = explode("/",$_SERVER['REQUEST_URI']);
  if(count($menu_ex) > 4){
  	$extends_param_url = explode("&",$menu_ex[4]);

  	if(count($extends_param_url) > 0){
  		$menu = $extends_param_url[0];
  	}else{
  		$menu = $menu_ex[4];
  	}
  }else{
  	$extends_param_url = explode("&",$menu_ex[3]);

  	if(count($extends_param_url) > 0){
  		$menu = $extends_param_url[0];
  	}else{
  		$menu = $menu_ex[3];
  	}
  }

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Report Amalia</title>
    <link rel="apple-touch-icon" href="template/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/cdnmax.css">
    <!-- <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet"> -->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/ui/prism.min.css">
    <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/app.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="template/assets/css/style.css">
    <link href="assets/plugins/chosen/chosen.css" rel="stylesheet" />
    <link href="assets/plugins/chosen/chosen.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datatables/media/css/jquery.dataTables_themeroller.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datatables/media/css/jquery.dataTables.css" />
    <!-- END Custom CSS-->
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    				<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    				<?php
            $cs = Yii::app()->clientScript;
    				//$cs->coreScriptPosition = $cs::POS_END;
            //$cs = Yii::app()->clientScript;
            $cs->coreScriptPosition = CClientScript::POS_END;
            //$cs->registerCoreScript('jquery');

    				$cs->scriptMap = array(
    						'jquery.js'=>false,
    						'jquery.ui.js'=>false,
    				); ?>
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
		<?php
		$namapegawai = Pegawai::model()->getNamaPegawai(Yii::app()->user->id);
		$foto = Yii::app()->session['foto'];
		?>
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
            </ul>
            <ul class="nav navbar-nav float-right">
              <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online"><img src="<?=$foto?>" alt="avatar"></span></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="avatar avatar-online"><img src="<?=$foto?>" alt="avatar"><span class="user-name text-bold-700 ml-1"><?=$namapegawai?></span></span></a>
                    <div class="dropdown-divider"></div><a class="dropdown-item" href="index.php?r=site/logout"><i class="ft-power"></i> Logout</a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="images/bg-side.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mr-auto"><a class="navbar-brand" href="#"><img class="brand-logo" alt="Chameleon admin logo" src="images/new_logo_ta.png"/>
              <h3 class="brand-text">Report Amalia</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class="nav-header">&nbsp;</li>
          <?php
          // $getitemname = Authassignment::model()->findByPk(Yii::app()->user->id);
          // $itemname = $getitemname->itemname;
          // $location = 0;
          // $menus = MenusNew::model()->getDataTopMenus($itemname, $location);
          // echo $menus;
          ?>
          <?php
          // if(strtoupper($menu) == "HOME"){
            echo '<li class="nav-item">';
          // }else{
          //   echo '<li class="has-sub">';
          // }
          ?>
            <a href="index.php?r=report/home">
                <i class="ft ft-home"></i>
                <span>Home </span>
            </a>
          </li>

          <?php
          //if(strtoupper($menu) == "REKAPBANEW"){
            echo '<li class="nav-item">';
          // }else{
          //   echo '<li class="has-sub">';
          // }
          ?>
            <a href="index.php?r=report/RekapBaNew">
                <i class="ft-layout"></i>
                <span>Rekap Approval </span>
            </a>
          </li>

          <?php

          $count_tl =  count($f);
          if($count_tl > 0){
            if(strtoupper($menu) == "INBOXTL" || strtoupper($menu) == "INBOXTLNEEDVALIDATE"){
              echo '<li class="has-sub active">';
            }else{
              echo '<li class="has-sub">';
            }
          ?>
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="ft ft-edit"></i>
                <span>Inbox TL</span>
            </a>
            <ul class="sub-menu">
              <?php
                if(strtoupper($menu) == "INBOXTLNEEDVALIDATE"){
                  echo '<li class="nav-item">';
                }else{
                  echo '<li >';
                }
              ?>
                <a href="index.php?r=report/inboxTlNeedValidate"><font size="2">Validate [ <span id="ibx_need_validate">-</span> ]</font></a>
              </li>
              <?php
                if(strtoupper($menu) == "INBOXTL"){
                  echo '<li class="nav-item">';
                }else{
                  echo '<li >';
                }
              ?>
                <a href="index.php?r=report/inboxTl"><font size="2">Return [ <span id="ibx_return">-</span> ]</font></a>
              </li>
            </ul>
          </li>
          <?php
          }
          ?>



          <?php
            if(count($aso) > 0){

            // if(strtoupper($menu) == "SHOWAPPROVALBA"){
              echo '<li class="nav-item">';
            // }else{
            //   echo '<li class="has-sub">';
            // }

          ?>

            <a href="index.php?r=report/showApprovalBa">
                <i class="ft ft-edit"></i>
                <span>Approval ASO [ <label style="color: #8B0000" id="ibx_aso">-</label> ]</span>
            </a>


          </li>


          <?php
            }

            // if(strtoupper($menu) == "APPROVEDBA"){
              echo '<li class="nav-item">';
            // }else{
            //   echo '<li class="has-sub">';
            // }
          ?>
            <a href="index.php?r=report/approvedBa">
                <i class="ft-check-square"></i>
                <span>Approved</span>
            </a>
          </li>

          <?php
            if(count($ho) > 0){

            // if(strtoupper($menu) == "DAFTARTL"){
              echo '<li class="nav-item">';
            // }else{
            //   echo '<li class="has-sub">';
            // }

          ?>

            <a href="index.php?r=report/daftarTl">
                <i class="ft-user"></i>
                <span>Daftar TL</span>
            </a>


          </li>


          <?php
            }
          ?>
          <?php
            //if(count($ho) > 0){
              if(count($aso) == 0 OR count($ho) > 0)
              {
            // if(strtoupper($menu) == "PERFORMTL"){
              echo '<li class="nav-item">';
            // }else{
            //   echo '<li class="has-sub">';
            // }

          ?>

            <a href="index.php?r=report/performTl">
                <i class="ft-trending-up"></i>
                <span>Performansi TL</span>
            </a>


          </li>


          <?php
        }	//}
          ?>

        </ul>
      </div>
      <div class="navigation-background"></div>
    </div>

    <div class="app-content content">
			<?php echo $content ?>
    </div>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer fixed-bottom footer-dark navbar-border navbar-shadow">
      <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2018  &copy; Copyright <a class="text-bold-800 grey darken-2" href="https://themeselection.com" target="_blank">ThemeSelection</a></span>
        <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
          <li class="list-inline-item"><a class="my-1" href="https://themeselection.com/" target="_blank"> More themes</a></li>
          <li class="list-inline-item"><a class="my-1" href="https://themeselection.com/support" target="_blank"> Support</a></li>
          <li class="list-inline-item"><a class="my-1" href="https://themeselection.com/products/chameleon-admin-modern-bootstrap-webapp-dashboard-html-template-ui-kit/" target="_blank"> Purchase</a></li>
        </ul>
      </div>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="assets/plugins/jquery/jquery-2.2.4.min.js"></script>
    <script src="template/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="template/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="template/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="template/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- <script src="app-assets/js/scripts/modal/components-modal.js" type="text/javascript"></script> -->
    <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="assets/plugins/chosen/chosen.ajaxaddition.jquery.js"></script>
    <script src="css/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dashboard.min.js"></script>
    <script src="assets/js/apps.min.js"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    <script>
      $(document).ready(function() {
        // App.init();
        // Dashboard.init();
        //
        $.get("index.php?r=report/DataInboxTl&nik=<?php echo $nik; ?>", function(data, status){
          var ini = JSON.parse(data)

          $('#ibx_need_validate').html(ini.need_validate)
          $('#ibx_return').html(ini.return)
          // alert("test me !")
        });

        $.get("index.php?r=report/dataInboxAso&nik=<?php echo $nik; ?>", function(data, status){
          var ini = JSON.parse(data)

          $('#ibx_aso').html(ini.need_approve)
          // alert("test me !")
        });

      });
    </script>
  </body>
</html>
