<script>
    function loadingpage() {
//        $("#loadingpage").text("Mohon Tunggu Proses Update Sedang Berlangsung");
//        $("#loadingpage").load;
        $("#loadingpage").html('<p><center><? echo CHtml::image("images/loadingsubmited.gif", 'imageloading', array('height' => 200)) ?><center></p>');
    }
</script>

<div class="container clear_both padding_fix">
    <!--\\\\\\\ container  start \\\\\\-->
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="header" style="background: #e35154; color: white">
                    <h3 class="content-header"><center>Update Data NIK Report Amalia</center></h3>
                </div>
                <div style="padding: 10px">
                    <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'updatedata',
                            'enableAjaxValidation' => false,
                            'htmlOptions' => array('enctype' => 'multipart/form-data',
                                    'onsubmit' => 'return loadingpage()'
                            )
                        ));
                    ?>
                    <?php echo CHtml::hiddenField('nik',Yii::app()->session['nik']);?>
                    <p>
                        Nik Anda merupakan nik baru, silakan klik tombol Update di bawah untuk mengupdate semua transaksi nik lama anda dengan nik baru. <br/>
                        <b>Mohon untuk tidak menutup halaman jika ketika update sedang berlangsung.</b>
                    </p>
                </div>
                <div class="form-actions" style="text-align: left">
                  <button type="submit" class="btn btn-primary">Update Data</button>
                </div>
                <div id="loadingpage"></div>
                <!--/porlets-content-->
            </div>
            <!--/block-web-->
        </div>
        <!--/col-md-12-->
    </div>
    <!--/row-->
    <!--row end-->
</div>
<?php $this->endWidget(); ?>
