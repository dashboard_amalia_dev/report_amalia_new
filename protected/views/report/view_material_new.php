<table>
  <tr>
    <td align="center"><h3>Material Alista</h3></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td align="center"><h3>Material Tambahan</h3></td>
  </tr>
  <tr>
    <td>
          <div class="row">
              <div class="col-md-12">
                  <div class="panel panel-inverse">
                      <div class="panel-body">

              <table class="table table-striped table-bordered" border="1">

                  <tr class="success">
                    <th><center>NO</center></th>
                    <?php
                      if($regional != "-"){
                        echo "<th ><center>REG</center></th>";
                      }

                       if($witel != "-"){
                        echo "<th ><center>WITEL</center></th>";
                      }

                       if($sto != "-"){
                        echo "<th ><center>STO</center></th>";
                      }
                    ?>
                    <th ><center>NO SC</center></th>
                    <th ><center>NAMA BARANG</center></th>
                    <th ><center>DESIGNATOR</center></th>
                    <th><center>JML PEMAKAIAN</center></th>
                    <th><center>Jenis Kabel</center></th>
                  </tr>
                  <?php

                  $no=1;
                  $data_m = $model->excel_material_new($regional,$witel,$sto,$date1,$date2);
                  foreach ($data_m as $d) {
                    $jenis_kabel = "-";
                    if (strpos($d->id_barang, 'AC-OF') !== FALSE)
                    {
                     $jenis_kabel = "Kabel Udara";
                    } else if(strpos($d->id_barang, 'AC-OF') !== FALSE){
                      $jenis_kabel = "Kabel Tanah";
                    }
                    ?>
                    <tr class="info">
                      <td><?= $no ?></td>
                      <?php
                      if($regional != "-"){
                        echo "<td ><center>".$d->reg_tactical."</center></td>";
                      }

                       if($witel != "-"){
                        echo "<td ><center>".$d->witel_tactical."</center></td>";
                      }

                       if($sto != "-"){
                        echo "<td ><center>".$d->sto."</center></td>";
                      }
                    ?>
                      <td><?= $d->no_wo?></td>
                      <td><?= $d->nama_barang?></td>
                      <td><?= $d->id_barang?></td>
                      <td><?= $d->jml_pemakaian?></td>
                      <td><?= $jenis_kabel?></td>
                    </tr>

                  <?php
                  $no++;
                } ?>
                    <!-- </tbody> -->
                  </table>
                      </div>
                  </div>
              </div>
          </div>
    </td>   
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>
      <div class="row">
        <div class="col-md-6">
            <div class="panel panel-inverse">
                <div class="panel-body">

        <table class="table table-striped table-bordered" border="1">

            <tr class="success">
              <th><center>NO</center></th>
               <?php
                  if($regional != "-"){
                    echo "<th ><center>REG</center></th>";
                  }

                   if($witel != "-"){
                    echo "<th ><center>WITEL</center></th>";
                  }

                   if($sto != "-"){
                    echo "<th ><center>STO</center></th>";
                  }
                ?>
              <th ><center>NO WO</center></th>
              <th ><center>DESIGNATOR</center></th>
              <th ><center>SATUAN</center></th>
              <th><center>VOLUME</center></th>
            </tr>
            <?php

            $no=1;
            $data_m = $model2->getMaterialTambahanFromDashboard($regional,$witel,$sto,$date1,$date2);
            foreach ($data_m as $d) {
              ?>
              <tr class="info">
                <td><?= $no ?></td>
                 <?php
                      if($regional != "-"){
                        echo "<td ><center>".$d->reg_tactical."</center></td>";
                      }

                       if($witel != "-"){
                        echo "<td ><center>".$d->witel_tactical."</center></td>";
                      }

                       if($sto != "-"){
                        echo "<td ><center>".$d->sto."</center></td>";
                      }
                    ?>
                <td><?= $d->no_wo?></td>
                <td><?= $d->designator?></td>
                <td><?= $d->satuan?></td>
                <td><?= $d->volume?></td>
              </tr>

            <?php
            $no++;
          } ?>
              <!-- </tbody> -->
            </table>
                </div>
            </div>
        </div>
      </div>
    </td>     
</tr>
</table>


