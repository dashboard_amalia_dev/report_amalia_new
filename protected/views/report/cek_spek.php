

<?php

$psb = "";
$migrasi = "";
$redaman = "";
$html ="";
$usee_tv = "";
$voice = "";

$internet_spek = true;
$tv_spek = true;
$voice_spek = true;

$pemakaian = $model->dataPemakaianRow($id);
// $l_voice = $layanan_amalia->layanan("3");

foreach($pemakaian as $p){
    $psb = $p->psb;
    $migrasi = $p->migrasi;
    $redaman = $p->redaman;
    $redaman_ont = $p->redaman_ont;
    $ukur = $p->ukur;
    $keterangan_usage = $p->keterangan_usage;
    $last_channel = $p->last_channel;
    $registred = $p->register_voice;
    $sn_ont = $p->sn_ont;
    $dsl = $p->dsl;
    $internet = $p->internet;
    $last_update_usage = $p->last_update_usage;
    $status_approve = $p->status_approve;
    $comment_app_aso = $p->comment_check_aso;
    $check_aso = $p->is_check_aso;
    $message_stb = $p->message_stb;
}
//echo "test ".$keterangan_usage;

$val_val = "";
$is_voice = false;
$is_internet = false;
$is_useetv = false;
// echo $id;
// exit();
// echo $psb;
if($psb > 0){
    $l_layanan = $layanan_amalia->layananPsb($psb);

    if(count($l_layanan) > 0){
        foreach($l_layanan as $l){
            $n_psb = $l['is_psb'];
            $n_voice= $l['is_voice'];
            $n_internet= $l['is_internet'];
            $n_useetv= $l['is_tv'];

            $n_layanan = $l['deskripsi'];

            if($n_psb == 1){
                $layanan =  $n_layanan;
                if($n_voice == "1"){
                    $is_voice = true;
                }

                if($n_internet == "1"){
                    $is_internet = true;
                }

                if($n_useetv == "1"){
                    $is_useetv = true;
                }

            }
        }
    }else{
        if($psb == 1){
            if($internet == 1){
                $is_internet = true;
            }

            if($dsl == 1){
                $is_useetv = true;
            }

            if($sn_ont != ""){
                $is_voice = true;
            }

            $layanan = "1P";
        }else{

        }
    }


}else{
  $l_layanan = $layanan_amalia->layananMigrasi($migrasi);
    //if($layanan_amalia->layananMigrasi($migrasi)){
        foreach($l_layanan as $l){
            $n_migrasi = $l['is_migrasi'];
            $n_voice= $l['is_voice'];
            $n_internet= $l['is_internet'];
            $n_useetv= $l['is_tv'];

            $n_layanan = $l['deskripsi'];

            if($n_migrasi == 1){
                $layanan =  $n_layanan;
                if($n_voice == "1"){
                    $is_voice = true;
                }

                if($n_internet == "1"){
                    $is_internet = true;
                }

                if($n_useetv == "1"){
                    $is_useetv = true;
                }

            }
        }
    //}
}

if($ukur == "1"){
    $html = '<label class="btn btn-sm btn-success">SPEK <br/>';

}else if($ukur == "2"){
    $html = ' <label class="btn btn-sm btn-warning">TDK SPEK <br/>';
    $internet_spek = false;
}else{
    $html = ' <label class="btn btn-sm btn-default ">TDK ADA REDAMAN <br/>';
    $internet_spek = false;
    }


if($is_voice){

    if($registred == "NOT REGISTERED"){
        $voice = '
        <tr>
            <td style="width: 150px;">Register Voice</td>
            <td>&nbsp;:&nbsp;</td>
            <td><label class="btn btn-sm btn-danger ">NOT REGISTERED</td>
        </tr>
        ';
        $voice_spek = false;
    }else{
        $voice = '
        <tr>
            <td style="width: 150px;">Register Voice</td>
            <td>&nbsp;:&nbsp;</td>
            <td><label class="btn btn-sm btn-success ">'.$registred.'</td>
        </tr>
        ';
    }

}

$last_update = '
<tr>
    <td style="width: 150px;">Last update</td>
    <td>&nbsp;:&nbsp;</td>
    <td>'.$last_update_usage.'</td>
</tr>
';

if($is_internet){
 $internet = '
<tr>
<td>Redaman OLT</td>
<td>&nbsp;:&nbsp;</td>
<td>'. $redaman .'</td>
</tr>
<tr>
<td>Redaman ONT</td>
<td>&nbsp;:&nbsp;</td>
<td>'.$redaman_ont.'</td>
</tr>

<tr>
<td>Status</td>
<td>&nbsp;:&nbsp;</td>
<td>'.$html .'</td>
</tr>
<tr>
<td style="width: 150px;">Keterangan usage</td>
<td>&nbsp;:&nbsp;</td>
<td>'.$keterangan_usage .'</td>
</tr>
 ';
}

if($is_useetv){
    $usee_tv = '
        <tr>
            <td style="width: 150px;">&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        ';
//disini ya memei
    if($last_channel == "-"){
        // $usee_tv .= '
        // <tr>
        //     <td style="width: 150px;">Last Channel</td>
        //     <td>&nbsp;:&nbsp;</td>
        //     <td><label class="btn btn-sm btn-danger">NOT USAGE</td>
        // </tr>
        // ';
        $tv_spek = false;
        //cek di message stb
        if ($message_stb != '' OR $message_stb != 'NULL') {

        $json_stb = json_decode($message_stb);
        $stb_id = $json_stb->stb_id;
        if (isset($stb_id)) {
          $tv_spek = true;
          $usee_tv .= '
          <tr>
              <td style="width: 150px;">Last Channel</td>
              <td>&nbsp;:&nbsp;</td>
              <td><label class="btn btn-sm btn-danger">USAGE : STB ID = '.$stb_id.'</td>
          </tr>
          ';
        }else {
          $usee_tv .= '
          <tr>
              <td style="width: 150px;">Last Channel</td>
              <td>&nbsp;:&nbsp;</td>
              <td><label class="btn btn-sm btn-danger">NOT USAGE</td>
          </tr>
          ';
        }

      }else {
        $usee_tv .= '
        <tr>
            <td style="width: 150px;">Last Channel</td>
            <td>&nbsp;:&nbsp;</td>
            <td><label class="btn btn-sm btn-danger">NOT USAGE</td>
        </tr>
        ';
      }
    }else{
        $usee_tv .= '
        <tr>
            <td style="width: 150px;">Last Channel</td>
            <td>&nbsp;:&nbsp;</td>
            <td><label class="btn btn-sm btn-success">'.$last_channel.'</td>
        </tr>
        ';
    }

}


$show  ="ok";
if ($internet_spek && $tv_spek && $voice_spek) {
  $show = 'nok';
}

$nik = Yii::app()->session['nik'];
$user = new User();
$aso = $user->IsAso($nik);
//echo "nik ".count($aso);
if ($show == 'ok' && count($aso) > 0) {

$v_show = 'block';

}else {
  $v_show = 'none';
}
//$v_show = 'none';
?>

<script>

$('#check_aso').click(function(event) {
    if(this.checked) {
          $('#approve_modal').show()
    }else{
          $('#approve_modal').hide()
    }
});

    $('#reload_ukur').click(function(){
     var base = '<?php echo Yii::app()->getBaseUrl(true)."/images/icon_loading.gif" ?>'
     var ini_id = '<?php echo $id; ?>'
     $('#content_spek').html('<img style="width:100px" src="'+base+'" />')
     $.post('https://api.telkomakses.co.id/API/amalia/ibooster.php',
        {
            no_wo : ini_id
        },

        function(data,status){
            //alert(data)
            $.get("index.php?r=report/CekSpek&id="+ini_id, function(data, status){

                $.get("index.php?r=report/DataUkurApprove&no_wo="+ini_id, function(datas, status){

					var ini = JSON.parse(datas)
          //alert(ini.internet_spek)
					if(ini.internet_spek == true && ini.tv_spek == true && ini.voice_spek == true){
						$('#approve_modal').show()
            $('#revisi_teknisi').show()
					}else{
						$('#approve_modal').hide()
            $('#revisi_teknisi').hide()
					}
				})
			    $("#content_spek").html(data);
			});
        })
   })
</script>



<!-- <textarea class="form-control" cols="50" > -->
<!-- <table>
  <tr>
      <td><input type="checkbox"  name = "check_aso" id = "check_aso" method="POST"/>&nbsp;Checklist untuk last chanel not usage</td>
  </tr>
  <tr>
      <td><textarea rows="4" cols="50" name="comment_aso" class="form-control" id = "comment_aso" method="POST" placeholder = "Enter comment here ..."></textarea></td>
  </tr>
</table> -->
<!-- <div style ="display:<?=$v_show?>" align="center" id = "aso" >
<table  >
  <tr><td> </td>
      <td><input type="checkbox" name = "check_aso" id ="check_aso"/>&nbsp;Approve dengan catatan</td>
  </tr>
  <tr>
      <td> </td>
      <td><textarea rows="4" cols="50" name="comment_aso" id = "comment_aso" method="POST" placeholder = "Tambah catatan ..."></textarea></td>
  </tr>
</table>
</div> -->
    <table>
        <tr>
            <td colspan="2" >
                <h5><?php echo $layanan; ?></h5>
            </td>
            <td>
                <?php
                    // spek
                    if ($status_approve == '2') {

                    }else {
                        if(!$internet_spek || !$tv_spek || !$voice_spek){


                ?>
                    <div id="reload_ukur" ><img style="width:20px" src="<?php echo Yii::app()->baseUrl ?>/images/icon_reload.png" /></div>
                <?php
              }}
                ?>
            </td>
        </tr>
        <?=$internet?>
        <?=$voice?>
        <?=$usee_tv?>
        <?=$last_update?>
        </table>
        <?php

        if ($status_approve == '2') {
        echo '
        <table>
        <tr>
        <br /><br /><hr/>
          <td align="" colspan="3">
            <font color ="#008080"><b>Comment approval ASO</b></font><br/>
             <!-- <a href="#" >
               <img class="zoom" style="height: 200px;width: 200px;margin: 10px" src="<?php echo $foto1 ?>"> <br/>
             </a> -->
             '
             .$comment_app_aso.
        '
          </td >
          </tr>
          </table>
        ';
        }
        ?>

    <br />
    <br />
    <?php if ($status_approve == '2'){ ?>
    <?php }else{
      ?>
      <div style ="display:<?=$v_show?>"  id = "aso" >
      <table  >
        <tr><td> </td>
            <td><input type="checkbox" name = "check_aso" id ="check_aso" value = 'Y'/>&nbsp;Approve dengan catatan</td>
        </tr>
        <tr>
            <td> </td>
            <td><textarea rows="4" cols="50" name="comment_aso" id = "comment_aso" method="POST" placeholder = "Tambah catatan ..."></textarea></td>
        </tr>
      </table>
      </div>
  <?php  }
  //if diprbaiki teknisi unspek
  $nik = YII::app()->session['nik'];
  $tl = new Tl();
  $f = $tl->isTl($nik);
  $count_tl =  count($f);

    if ($status_approve == '-3' AND $count_tl >= 1) {
      // echo '<button id="revisi_teknisi" style="display:none" class="btn btn-sm btn-warning" data-dismiss="modal">Sudah diperbaiki</button>';
      echo '<a href="index.php?r=report/revisiSpek&id='.$id.'" id="revisi_teknisi" ><button type="button" id="revisi_save" class="btn btn-sm btn-warning" >Sudah diperbaiki</button></a>  </div>';
    }

  ?>

<script>
<?php if ($registred == 'REGISTERED' AND $last_channel != '-' AND $ukur == '1') {?>

  $('#approve_modal').show()
  $('#revisi_teknisi').show()

  <?php

}else {?>
  $('#approve_modal').hide()
  $('#revisi_teknisi').hide()
<?php

}?>

<?php if($internet_spek == true && $tv_spek == true && $voice_spek == true){ ?>
    $('#approve_modal').show()
    $('#revisi_teknisi').show()
    //alert("tampil");
<?php }else{ ?>
    $('#approve_modal').hide()
    $('#revisi_teknisi').hide()
<?php }       ?>
</script>
</textarea>
