
<?php
if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
	$this->redirect("index.php?r=site/login");
	exit();
}

$nik = Yii::app()->session['nik'];
$user = new User();
$aso = $user->IsAso($nik);

$nak = $master_sto->getReg($regional,$changed);
$wit = $master_sto->getWitelNew($regional,$witel,$changed);
$st = $master_sto->getDataSto($regional,$witel,$sto,$changed);

// if($regional != "-" && $witel != "-" && $sto != "-"){
if($changed == "-"){
	// $regional = $nak[0]->reg;
	// $witel = $wit[0]->witel_versi_tactical;
	// $sto = $st[0]->sto;
}

?>


<style type="text/css">


	/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">

	var klikOn = 0;
	var id = "";
	function openCity(evt, cityName) {
	  var i, tabcontent, tablinks;
	  // Get all elements with class="tabcontent" and hide them
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }

	  // Get all elements with class="tablinks" and remove the class "active"
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }

	  document.getElementById(cityName).style.display = "block";
	  evt.currentTarget.className += " active";
	}

	function bukaPopUp(id){
		var base = '<?php echo Yii::app()->getBaseUrl(true)."/images/icon_loading.gif" ?>'
		document.getElementById("content_ba").src=base;

		tablinks = document.getElementsByClassName("tablinks");

		tablinks[0].className = tablinks[0].className.replace("tablinks", "tablinks active");
		tablinks[1].className = tablinks[1].className.replace("tablinks active", "tablinks");
		tablinks[2].className = tablinks[2].className.replace("tablinks active", "tablinks");

		tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }

		  document.getElementById('ba').style.display = "block";

		setTimeout(function () {

		document.getElementById("content_ba").src="https://amalia.telkomakses.co.id/pdf/examples/amalia_for_digital_signatur.php?no_wo="+id;

		}, 1000);


		document.getElementById("id_view").value = id;
		document.getElementById("hd_id").value = id;

		// tablinks = document.getElementsByClassName("tablinks");
		// tablinks[0].className = tablinks[0].className.replace("tablinks", "tablinks active");
		// document.getElementById('ba').style.display = "block";
		// document.getElementById("content_ba").src="http://alista.telkomakses.co.id/amalia/pdf/examples/isi_ba_v3_dev.php?no_wo="+id;
	}

	$(document).ready(function(){

		$("#tab_pelanggan").click(function(){
		  	var ini_id = $("#id_view").val();
			$.get("index.php?r=report/viewMaterial&id="+ini_id, function(data, status){
			    $("#table_material").html(data);
			});
		});

	   $("#approve_modal").click(function(){
	   		var id = $('#hd_id').val()
			$.get("index.php?r=report/ApproveBaUpdate&id="+id, function(data, status){
			    $("#table_material").html(data);
			});
		});

	  $("#tab_foto").click(function(){
	  	var ini_id = $("#id_view").val();
		$.get("index.php?r=report/dataFoto&id="+ini_id, function(data, status){
		    $("#table_foto").html(data);
		});
	   });



	  $("#regional").change(function(){
	  		var regional = $('#regional').val()
	  		var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
			window.location.replace("index.php?r=report/performTl&regional="+regional+"&date1="+date1+"&date2="+date2+"&changed=regional");
	   });

	  $("#witel").change(function(){
			var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
	  		var regional = $('#regional').val()
	  		var witel = $('#witel').val()
			window.location.replace("index.php?r=report/performTl&regional="+regional+"&witel="+witel+"&date1="+date1+"&date2="+date2+"&changed=witel");
	   });

	  $("#sto").change(function(){
			var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
	  		var regional = $('#regional').val()
	  		var witel = $('#witel').val()
	  		var sto = $('#sto').val()
			window.location.replace("index.php?r=report/performTl&regional="+regional+"&witel="+witel+"&sto="+sto+"&date1="+date1+"&date2="+date2+"&changed=sto");
	   });

	  $("#search_date").click(function(){
		var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
	  		var regional = $('#regional').val()
	  		var witel = $('#witel').val()
	  		var sto = $('#sto').val()
			window.location.replace("index.php?r=report/performTl&regional="+regional+"&witel="+witel+"&sto="+sto+"&date1="+date1+"&date2="+date2+"&changed=date");
	   });



	  $("#return_save").click(function(){
		  	var id = $("#hd_id").val();
		  	var karena = $("#keterangan").val();
		  	var karena_detil = $("#keterangan_detil").val();
		  	if(karena_detil == ""){
		  		alert("Isi Terlebih dahulu Detil Keterangan")
		  		return true;
		  	}
				$.get("index.php?r=report/InsertReturn&id="+id+"&karena="+karena+"&karena_detil="+karena_detil, function(data, status){
				    $("#table_foto").html(data);
				});

			});
	});



</script>

<input type="text" id="id_view" hidden>
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->

	<div class="row">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<?php

						function bulan($bulan)
						{
							switch ($bulan){
								case 1 : $bulan="JANUARI";
								break;
								case 2 : $bulan="FEBRUARI";
								break;
								case 3 : $bulan="MARET";
								break;
								case 4 : $bulan="APRIL";
								break;
								case 5 : $bulan="MEI";
								break;
								case 6 : $bulan="JUNI";
								break;
								case 7 : $bulan="JULI";
								break;
								case 8 : $bulan="AGUSTUS";
								break;
								case 9 : $bulan="SEPTEMBER";
								break;
								case 10 : $bulan="OKTOBER";
								break;
								case 11 : $bulan="NOVEMBER";
								break;
								case 12 : $bulan="DESEMBER";
								break;
							}
							return $bulan;
						}

						?>
						<h4 class="panel-title">REKAP BA INSTALASI <?php //echo "TGL ".$start_d." - TGL ".$end_d?></h4>
					</div>

					<div class="panel-body">
						<div class="table-responsive">
						<form  action="" method="POST">

							<div class="form-group">
					<div class="col-md-3">
					<?php
					// hari ini kembali
					echo"<table><tr>
							<td width='60px'><div>Regional</div></td>
								<td> :&nbsp</td>
							<td width='150px'>";
							echo '<select class="form-control" name="regional" id="regional" method="POST">';

							if($level == "1"){
								echo '<option value="All">All</option>';
							}

							$nak = $master_sto->getReg($regional,$level);
							foreach($nak as $s){

								if($s->reg == $regional){
									echo '<option selected value="'.$s->reg.'">'.$s->reg.'</option>';
								}else{
									echo '<option value="'.$s->reg.'">'.$s->reg.'</option>';
								}
							}
							echo'</select>';
							echo "</td>";
							echo "</tr>";
							echo "</table>";
							echo "<br/>";
					 ?>

						 <?php
					 		if($changed == "-" && ($level == 2 || $level == 3 || $level == 4) ){
								$nak = $master_sto->getReg($regional,$level);
								$regional = $nak[0]->reg;

							}

							// hari ini kembali
							echo"<table><tr>
								<td width='60px'><div>Witel</div></td>
									<td> :&nbsp</td>
								<td width='150px'>";
							echo '<select class="form-control" name="witel" id="witel" method="POST">';


							if(count($master_sto->checkWitelFromRegional($regional)) == count( $master_sto->getWitelNew($regional,$witel,$changed))){
							// if($level == "1" || $level == "2"){
								echo '<option value="All">All</option>';
							}



							$wit = $master_sto->getWitelNew($regional,$witel,$changed);
							foreach($wit as $s){
								if($s->witel_versi_tactical == $witel){
									echo '<option selected value="'.$s->witel_versi_tactical.'">'.$s->witel_versi_tactical.'</option>';
								}else{
									echo '<option value="'.$s->witel_versi_tactical.'">'.$s->witel_versi_tactical.'</option>';
								}
							}
							echo'</select>';
							echo "</td>";
							echo "</tr>";
							echo "</table>";
							echo "<br/>";

							// untuk mendapatkan index dari filter witel
							if($witel == "-"){
								if($changed == "-" || ( $level == 3 || $level == 4)){
									$wit = $master_sto->getWitelNew($regional,$witel,$changed);
									$witel = $wit[0]->witel_versi_tactical;
								}else if($changed != "-" && count($master_sto->checkWitelFromRegional($regional)) != count( $master_sto->getWitelNew($regional,$witel,$changed))){
									$wit = $master_sto->getWitelNew($regional,$witel,$changed);
									$witel = $wit[0]->witel_versi_tactical;
								}
							}
					 ?>

					 <?php
					 ?>
					<table align="center" style="margin-left:-5px">
						<tr>
							<td ><label style="width:60px">Start Date</label></td>
							<td>: &nbsp;</td>
							<td><input  id="date1" name="date1" value="<?php if($date1 !="-") {echo $date1;} ?>"  type="date" class="form-control" style="width:150px" /></td>
						</tr>
						<tr>
							<td colspan="3"><br/></td>
						</tr>
						<tr>
							<td ><label style="width:60px">End Date</label></td>
							<td>: &nbsp;</td>
							<td><input id="date2" name="date2" value="<?php if($date2 !="-") {echo $date2;} ?>" type="date" class="form-control" style="width:150px" /></td>
						</tr>
					</table>

					<br/>
					<button type="button" class="form-control btn btn-sm btn-success" id="search_date">Search</button>
					<br/><br/>
				 </div>

				<div class="col-md-2">

			 </div>
			 <div class="col-md-2" >

			 </div>
			 <div class="col-md-2">

				 <!-- <button type="submit" id="filter" class="btn btn-sm btn-success">Filter</button> -->
			 </div>
					</div>

						</form>


					<?php
						if($regional != "-" && $witel != "-" && $sto != "-"){
					?>
						<?php
						// $this->widget('zii.widgets.grid.CGridView',
						// 	array(
						// 		'id' => 'master-aset-grid',
						// 		'dataProvider' =>
						// 			$model->dataRekapNewWithSto($regional,$witel,$sto,$date1,$date2),
						// 					'filter' => $model,
						// 					'columns' => array(
						// 						'regional',
						// 						'witel',
						// 						'sto',
						// 						'no_wo',
				    //             				'no_inet',
						// 		                'nama_pelanggan',
						// 						'jenis_layanan',
						// 						'ukur_usage',
						// 						'status_approve',
						//
						// 		array(
						// 			'header'=>'View BA',
						// 			'type'=>'raw',
						// 			'value'=>
						// 				'TPemakaian::model()->viewMaterialApproved($data->no_wo)'
						// 		),
						//
						// 		),
						// 	));
						$this->widget('zii.widgets.grid.CGridView',
							array(
								'id' => 'master-aset-grid',
								'dataProvider' =>
									$model->dataRekapPerformTlSto($regional,$witel,$sto,$date1,$date2),
											'filter' => $model,
											'columns' => array(
												'regional',
												'witel',
												'sto',
												'nik_tl',
												'nama_tl',
												'need_validate',
												'need_approve',
												'performansi'


								// array(
								// 	'header'=>'View BA',
								// 	'type'=>'raw',
								// 	'value'=>
								// 		'TPemakaian::model()->viewMaterialApproved($data->no_wo)'
								// ),

								),
							));
							?>
							<div class="col-md-3">
							<?php
							$data_reg = $model->dataRekapPerformTlSto($regional,$witel,$sto,$date1,$date2);
							if(count($data_reg) > 0){
								if( $witel != ""){
								//	echo '<a class="btn btn-sm btn-success" href="/report_amalia/index.php?r=report/RekapBaNew&excel=1&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'">Export excel</a>  &nbsp;';

								//	echo '<a class="btn btn-sm btn-success" href="/report_amalia/index.php?r=report/RekapBaNew&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'&material=1">Export Material</a>';

								}
							}
							?>
							</div>
							</div>
						</div>
						</div>
						</div>
						</div>
					<?php
						}else if($regional != "-" && $regional != "" && $witel !="-" && $witel !=""){
					?>
					<?php
					$this->widget('zii.widgets.grid.CGridView',
						array(
							'id' => 'master-aset-grid',
							'dataProvider' =>
								$model->dataRekapPerformTlSto($regional,$witel,$sto,$date1,$date2),
										'filter' => $model,
										'columns' => array(
											'regional',
											'witel',
											'sto',
											'nik_tl',
											'nama_tl',
											'need_validate',
											'need_approve',
											'performansi'


							// array(
							// 	'header'=>'View BA',
							// 	'type'=>'raw',
							// 	'value'=>
							// 		'TPemakaian::model()->viewMaterialApproved($data->no_wo)'
							// ),

							),
						));
						?>
						<!-- <table class="table table-striped table-bordered">

							<tr class="success">
								<th  ><center style="margin-top:30px">NO</center></th>
								<th  ><center style="margin-top:30px">REGIONAL</center></th>
								<th  ><center style="margin-top:30px">WITEL</center></th>
								<th  ><center style="margin-top:30px">STO</center></th>
								<th  ><center style="margin-top:30px">NIK TL</th>
								<th  ><center style="margin-top:30px">NAMA TL</th>
								<th  ><center style="margin-top:30px">Need Validate</center></th>
								<th  ><center style="margin-top:30px">Approved</th>
								<th  ><center style="margin-top:30px">Performance [100%]</center></th>


							</tr>
							<?php
							$data_reg = $model->dataRekapPerformTlSto($regional,$witel,$sto,$date1,$date2);
							$no = 1;
							foreach ($data_reg as $reg) {
								$need_validate 		= $reg->need_validate;
								$need_approve 		= $reg->need_approve;
								$nik_tl 					= $reg->nik_tl;
								$nama_tl 					= $reg->nama_tl;
								$regional 				= $reg->regional;
								$witel 						= $reg->witel;
								$sto 							= $reg->sto;
								if ($reg->need_approve == 0) {
									$performasi = 0;
								}else {
									$performasi 			= ($reg->need_approve / ($reg->need_validate + $reg->need_approve)) * 100 / 100;
								}

								?>
								<tr class="info">
									<td><?= $no ?></td>
									<td><?= $regional?></td>
									<td><?= $witel?></td>
									<td><?= $sto?></td>
									<td><?= $nik_tl?></td>
									<td><?= $nama_tl?></td>
									<td><?= $need_validate?></td>
									<td><?=$need_approve?></td>
									<td><?=$performasi?></td>
								</tr>
							<?php
								$no++;
							}
							?>
							</table> -->
							<?php
							?>
							</div>
							</div>
							</div>
							</div>
							</div>

					<?php
						}else if($regional != "-"){
					?>
					<?php
					$this->widget('zii.widgets.grid.CGridView',
						array(
							'id' => 'master-aset-grid',
							'dataProvider' =>
								$model->dataRekapPerformTlWit($regional,$witel,$sto,$date1,$date2),
										'filter' => $model,
										'columns' => array(
											'regional',
											'witel',
											'sto',
											'nik_tl',
											'nama_tl',
											'need_validate',
											'need_approve',
											'performansi'


							// array(
							// 	'header'=>'View BA',
							// 	'type'=>'raw',
							// 	'value'=>
							// 		'TPemakaian::model()->viewMaterialApproved($data->no_wo)'
							// ),

							),
						));
						?>
						<!-- <table class="table table-striped table-bordered">

							<tr class="success">
								<th  ><center style="margin-top:30px">NO</center></th>
								<th  ><center style="margin-top:30px">REGIONAL</center></th>
								<th  ><center style="margin-top:30px">WITEL</center></th>
								<th  ><center style="margin-top:30px">STO</center></th>
								<th  ><center style="margin-top:30px">NIK TL</th>
								<th  ><center style="margin-top:30px">NAMA TL</th>
								<th  ><center style="margin-top:30px">Need Validate</center></th>
								<th  ><center style="margin-top:30px">Approved</th>
								<th  ><center style="margin-top:30px">Performance [100%]</center></th>

							</tr>

							<?php
							$data_reg = $model->dataRekapPerformTlWit($regional,$witel,$sto,$date1,$date2);
							$no = 1;
							foreach ($data_reg as $reg) {

								$need_validate 		= $reg->need_validate;
								$need_approve 		= $reg->need_approve;
								$nik_tl 					= $reg->nik_tl;
								$nama_tl 					= $reg->nama_tl;
								$regional 				= $reg->regional;
								$witel 						= $reg->witel;
								$sto 							= $reg->sto;
								if ($reg->need_approve == 0) {
									$performasi = 0;
								}else {
									$performasi 			= ($reg->need_approve / ($reg->need_validate + $reg->need_approve)) * 100 / 100;
								}

								?>
								<tr class="info">
									<td><?= $no ?></td>
									<td><?= $regional?></td>
									<td><?= $witel?></td>
									<td><?= $sto?></td>
									<td><?= $nik_tl?></td>
									<td><?= $nama_tl?></td>
									<td><?= $need_validate?></td>
									<td><?=$need_approve?></td>
									<td><?=$performasi?></td>
								</tr>
							<?php
								$no++;
					}
							?>
						</table> -->
							<?php
							if(count($data_reg) > 0){
								//	echo '<a class="btn btn-sm btn-success" href="/report_amalia/index.php?r=report/RekapBaNew&excel=1&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'">Export excel</a>  &nbsp;';
								//	echo '<a class="btn btn-sm btn-success" href="/report_amalia/index.php?r=report/RekapBaNew&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'&material=1">Export Material</a>';
							}
							?>
							</div>
							</div>
							</div>
							</div>
							</div>
					<?php
						}else{
							if($regional == "-"){
					?>
					<?php
					$this->widget('zii.widgets.grid.CGridView',
						array(
							'id' => 'master-aset-grid',
							'dataProvider' =>
								$model->dataRekapPerformTlReg($regional,$witel,$sto,$date1,$date2),
										'filter' => $model,
										'columns' => array(
											'regional',
											'witel',
											'sto',
											'nik_tl',
											'nama_tl',
											'need_validate',
											'need_approve',
											'performansi'


							// array(
							// 	'header'=>'View BA',
							// 	'type'=>'raw',
							// 	'value'=>
							// 		'TPemakaian::model()->viewMaterialApproved($data->no_wo)'
							// ),

							),
						));
						?>
								<!-- <table class="table table-striped table-bordered">

									<tr class="success">
										<th  ><center style="margin-top:30px">NO</center></th>
										<th  ><center style="margin-top:30px">REGIONAL</center></th>
										<th  ><center style="margin-top:30px">WITEL</center></th>
										<th  ><center style="margin-top:30px">STO</center></th>
										<th  ><center style="margin-top:30px">NIK TL</th>
										<th  ><center style="margin-top:30px">NAMA TL</th>
										<th  ><center style="margin-top:30px">Need Validate</center></th>
										<th  ><center style="margin-top:30px">Approved</th>
										<th  ><center style="margin-top:30px">Performance [100%]</center></th>

									</tr>

									<?php

									$data_reg = $model->dataRekapPerformTlReg($regional,$witel,$sto,$date1,$date2);
									 $no = 1;

									foreach ($data_reg as $reg) {
										$need_validate 		= $reg->need_validate;
										$need_approve 		= $reg->need_approve;
										$nik_tl 					= $reg->nik_tl;
										$nama_tl 					= $reg->nama_tl;
										$regional 				= $reg->regional;
										$witel 						= $reg->witel;
										$sto 							= $reg->sto;
										if ($reg->need_approve == 0) {
											$performasi = 0;
										}else {
											$performasi 			= ($reg->need_approve / ($reg->need_validate + $reg->need_approve)) * 100 / 100;
										}

										?>
										<tr class="info">
											<td><?= $no ?></td>
											<td><?= $regional?></td>
											<td><?= $witel?></td>
											<td><?= $sto?></td>
											<td><?= $nik_tl?></td>
											<td><?= $nama_tl?></td>
											<td><?= $need_validate?></td>
											<td><?=$need_approve?></td>
											<td><?=$performasi?></td>
										</tr>
									<?php
										 $no++;
									}
									?>
									</table> -->
									<?php
									?>
									</div>
									</div>
									</div>
									</div>
									</div>

					<?php
							}
							//end if regional == "-"
						}
						// end if change mode
					?>

						<!-- end row -->
					</div>
					<!-- end #content -->


<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="modal" role="dialog" >
    <div class="modal-dialog" style="width: 870px">

      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detil Amalia</h4>
          <br/>
          	<hr/>

        	<div align="center" class="col-md-12">
				<!-- <button id="approve_modal" class="btn btn-sm btn-success" data-dismiss="modal">Approve</button>&nbsp;<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#return">Return</button> -->
			</div>

        </div>
        <div class="modal-body">

		<!-- Tab links -->
		<div class="tab" >
		  <button class="tablinks" onclick="openCity(event, 'ba')">BA Digital</button>
		  <button class="tablinks" id="tab_foto" onclick="openCity(event, 'foto')">Foto Evident</button>
		  <button class="tablinks" id="tab_pelanggan" onclick="openCity(event, 'data_pelanggan')">Material Yang Digunakan</button>
		</div>

		<!-- Tab content -->
		<div id="ba" class="tabcontent active">
		  <br/>
		  <iframe
		   id ="content_ba"
		   style="height: 1200px;width: 800px"
		   src=""></iframe>

		</div>

		<input type="text" hidden id="hd_id">

		<div id="foto" class="tabcontent">
		<div align="center">
		  <div id="table_foto"></div>
		</div>
		</div>

		<!-- Tab content -->
		<div id="data_pelanggan" class="tabcontent">
		  	<br/>
			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-inverse">
			            <div class="panel-body">
						<div id="table_material"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="return" role="dialog" >
    <div class="modal-dialog" style="width: 870px">

      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Return Amalia</h4>
        </div>
        <div class="modal-body">

		Return Karena :
		<select class="form-control"  id="keterangan">
			<option value="1" selected>Foto Kurang Lengkap / Tidak sesuai</option>
			<option value="2">BA Kurang Lengkap</option>
			<option value="2">Foto dan BA Tidak Sesuai</option>
		</select>
		<br/>

		Detil Keterangan :
		<textarea class="form-control" id="keterangan_detil" rows="6" id="keterangan_return"></textarea>

		<input type="text" hidden id="hd_id_return">
		<!-- Tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="return_save" class="btn btn-default" data-dismiss="modal">Return Save</button>
        </div>
      </div>

    </div>
  </div>



</div>
