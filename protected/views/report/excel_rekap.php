<table class="table table-striped table-bordered" border="1">
<tr class="active">
  <th>NO </th>
  <th>Regional <?php echo $regional ?></th>
  <th>Witel</th>
  <th>STO</th>
  <th>NIK</th>
  <th>Nama Mitra</th>
  <th>No WO</th>
  <th>No Pelanggan 1</th>
  <th>No Pelanggan 2</th>
  <th>No Inet</th>
  <th>Nama Pelanggan</th>
  <th>Alamat Pelanggan</th>
  <th>Lat Pelanggan</th>
  <th>Long Pelanggan</th>
  <th>Lat ODP</th>
  <th>Long ODP</th>
  <th>Email Pelanggan</th>
  <th>Datek HK MSAN ODC</th>
  <th>Datek ODP</th>
  <th>Layanan</th>
  <th>Tambahan Perangkat</th>
  <th>Test Voice</th>
  <th>Test Internet</th>
  <th>Test Usee TV</th>
  <th>Test Ping</th>
  <th>Test Upload</th>
  <th>Test Download</th>
  <th>Hasil Ukur Redaman</th>
  <th>Modem Pasang</th>
  <th>SN ONT</th>
  <th>SN Modem</th>
  <th>SN PLC</th>
  <th>SN Wifi Extender</th>
  <th>Power</th>
  <th>DSL</th>
  <th>Internet</th>
  <th>Mac Address STB</th>
  <th>Speed</th>
  <th>Kendala</th>
  <th>Register Voice</th>
  <th>Redaman</th>
  <th>Last Channel</th>
  <th>Approved by</th>
  <th>Keterangan Usage</th>
  <th>Created BA</th>
  <th>ID Valins</th>
  <th>Status Order</th>
</tr>
<?php
$no = 1;
$data_reg = $model->dataRekapNewWithStoForExcel($regional,$witel,$sto,$date1,$date2);
foreach ($data_reg as $reg){

    $psb = $reg->psb;
    $migrasi = $reg->migrasi;
    $tambahan =$reg->tambahan;
    $layanan = "";
    $test_voice = "";
    $test_internet = "";
    $test_usetv = "";

    $power = "";
    $dsl = "";
    $internet = $reg->internet;
    $speed = "";


    if($psb > 0){
        if($psb == "3"){
            $layanan = "3P";
        }else if($psb == "4"){
            $layanan = "1P [Voice only]";
        }else if($psb == "5"){
            $layanan = "1P [Internet only]";
        }else if($psb == "6"){
            $layanan = "2P [Internet + Voice]";
        }else if($psb == "7"){
            $layanan = "2P [Internet + UseeTv]";
        }
    }else if($migrasi > 0){
        if($migrasi == "11"){
            $layanan = "1P [1P Voice only]";
        }else if($migrasi == "13"){
            $layanan = "1P - 2P [2P Voice + Internet]";
        }else if($migrasi == "15"){
            $layanan = "2P - 2P [2P Voice + Internet]";
        }else if($migrasi == "8"){
            $layanan = "2P - 3P";
        }else if($migrasi == "12"){
            $layanan = "1P - 1P [Internet only]";
        }else if($migrasi == "14"){
            $layanan = "1P - 2P [2P Internet + UseeTv]";
        }else if($migrasi == "16"){
            $layanan = "2P - 2P [2P Internet + UseeTv]";
        }else if($migrasi == "9"){
            $layanan = "3P - 3P";
        }
    }else{
        $layanan = "";
    }

    if($tambahan > 0){
        if($tambahan == "1"){
            $tambahan = "Change STB";
        }else if($tambahan == "2"){
            $tambahan = "STB Tambahan";
        }else if($tambahan == "3"){
            $tambahan = "PLC";
        }else if($tambahan == "4"){
            $tambahan = "WIFI Extender";
        }else if($tambahan == "5"){
            $tambahan = "Indi Box";
        }
    }else{
        $tambahan = "-";
    }

    $layanan = $reg->jenis_layanan;

    //Internet
    $test_internet = "-";
    if($internet == '1' || $psb == '3' || $psb == '5' || $psb == '6' || $psb == '7' ||  $migrasi == '13' ||  $migrasi == '15' ||  $migrasi == '8' ||  $migrasi == '6' ||  $migrasi == '12' ||  $migrasi == '14' ||  $migrasi == '16' ||  $migrasi == '9' ||  $migrasi == '20' ||  $migrasi == '21' ||  $migrasi == '3' ||  $migrasi == '2'){
      if($reg->test_internet > 0){
        $test_internet = "Baik";
      }else{
        $test_internet = "Buruk";
      }
    }


    //voice
    $test_voice = "-";
    if($psb == '4' || $psb == '6' || $psb == '3' || $migrasi  == '11' || $migrasi  == '13' || $migrasi  == '15' || $migrasi  == '9' ||  $migrasi == '8' ||  $migrasi == '6' ||  $migrasi == '20' ||  $migrasi == '2' ||  $migrasi == '3'){
      if($reg->test_voice > 0){
          $test_voice = "Baik";
      }else{
          $test_voice = "Buruk";
      }
    }


    //TV
    $test_usetv = "-";
    if($psb == '3' || $psb == '7' ||  $migrasi == '14' ||  $migrasi == '16' ||  $migrasi == '9' ||  $migrasi == '21' ||  $migrasi == '3' ||  $migrasi == '2' ||  $migrasi == '8' ||  $migrasi == '6' ){
      if($reg->test_use_tv > 0){
          $test_usetv = "Baik";
      }else{
          $test_usetv = "Buruk";
      }
    }

    // speed
    if($reg->speed < 11){
        $hasil =  $reg->speed * 10;
        $speed = $hasil." Mb";
    }else{
        if($reg->speed == "11"){
            $speed = "200 Mb";
        }else if($reg->speed == "12"){
            $speed = "300 Mb";
        }if($reg->speed == "13"){
            $speed = "1 Gb";
        }
    }

  ?>
  <tr >
    <td><?= $no?></td>
    <td><?= $reg->reg_tactical?></td>
    <td><?= $reg->witel_tactical?></td>
    <td><?= $reg->sto?></td>
    <td><?= $reg->nik?></td>
    <td><?= $reg->id_mitra?></td>
    <td><?= $reg->no_wo?></td>

    <td><?= $reg->no_kontak?></td>
    <td><?= $reg->no_kontak_2?></td>
    <td><?= $reg->no_inet?></td>
    <td><?= $reg->nama_pelanggan?></td>
    <td><?= $reg->alamat_pelanggan?></td>
    <td><?= $reg->lat_pelanggan?></td>
    <td><?= $reg->long_pelanggan?></td>
    <td><?= $reg->lat_odp?></td>
    <td><?= $reg->long_odp?></td>
    <td><?= $reg->email_pelanggan?></td>
    <td><?= $reg->datek_hk_msan_odc?></td>
    <td><?= $reg->datek_dp_odp?></td>
    <td><?= $layanan?></td>
    <td><?= $tambahan ?></td>
    <td><?= $test_voice?></td>
    <td><?= $test_internet?></td>
    <td><?= $test_usetv?></td>
    <td><?= $reg->test_ping?></td>
    <td><?= $reg->test_upload?></td>
    <td><?= $reg->test_download?></td>
    <td><?= $reg->hasil_ukur_pedalaman?></td>
    <td><?= $reg->modem_pasang?></td>
    <td><?= $reg->sn_ont?></td>
    <td><?= $reg->sn_modem?></td>
    <td><?= $reg->sn_plc?></td>
    <td><?= $reg->sn_wifi?></td>
    <td><?= $reg->power?></td>
    <td><?= $reg->dsl?></td>
    <td><?= $reg->internet?></td>
    <td><?= $reg->mac_address_stb?></td>
    <td><?= $speed?></td>
    <td><?= $reg->kendala?></td>
    <td><?= $reg->register_voice?></td>
    <td><?= $reg->redaman?></td>
    <td><?= $reg->last_channel?></td>
    <td><?= $reg->approved_by?></td>
    <td><?= $reg->keterangan_usage?></td>
    <td><?= $reg->create_dtm?></td>
    <td><?= $reg->id_valins?></td>
    <td><?= $reg->status_approve?></td>

  </tr>


<?php

$no++;

} ?>

</table>
