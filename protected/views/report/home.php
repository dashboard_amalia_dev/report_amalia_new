<?php
if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
	$this->redirect("index.php?r=site/login");
	exit();
}

// if (Pegawai::model()->getNamaPegawai(Yii::app()->user->id) == '') {
// 	$nama = Pegawai::model()->getNamaPegawai(Yii::app()->user->id);
// }else {
// 	$nama = Yii::app()->session['nama'];
// }
?>

		<div class="content-wrapper">
			<div class="content-wrapper-before"></div>
			<div class="content-header row">
				<div class="content-header-left col-md-4 col-12 mb-2">
					<h3 class="content-header-title">Home</h3>
				</div>
				<div class="content-header-right col-md-8 col-12">
					<div class="breadcrumbs-top float-md-right">
						<div class="breadcrumb-wrapper mr-1">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.php?r=report/home">Home</a>
								</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="content-body">
				<section class="row">
						<div class="col-sm-12">
								<div id="with-header" class="card">
										<div class="card-header">
												<h4 class="card-title">Home</h4>
												<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
												<div class="heading-elements">
														<ul class="list-inline mb-0">
																<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
																<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
														</ul>
												</div>
										</div>
										<div class="card-content collapse show">
												<div class="card-body border-top-blue-grey border-top-lighten-5 ">
														<h4 class="card-title">Welcome to Report Amalia, <?=Yii::app()->session['nama']?></h4>
														<div class="card-body">
																<p class="card-text">
																			PT. Telkom Akses (PTTA) merupakan anak perusahaan PT Telekomunikasi Indonesia, Tbk (Telkom) yang sahamnya dimiliki sepenuhnya oleh Telkom.
																			<br>
																			PTTA bergerak dalam bisnis penyediaan layanan
																			konstruksi dan pengelolaan infrastruktur jaringan. </p>
																	<!-- <p>Aplikasi WTMTA PT. Telkom Akses ini digunakan untuk menyimpan data working tools perusahaan. -->
																	</p>
														</div>
												</div>

										</div>
								</div>
							</div>
			 </section>
			</div>
		</div>
