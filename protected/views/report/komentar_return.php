<?php

$komentars = Pemakaian::model()->eksistComment($id);

foreach($komentars as $k){

    $komentar =  $k->fix_komentar;
    $detil_return = $k->detil_return;
    $date_finish_update = $k->fix_date;
    $created_date = $k->created_date;
    $id_return1 = $k->id_return1;
    $id_return2 = $k->id_return2;
    $id_return3 = $k->id_return3;

    $error_count = 0;
    $ket1 = "";
    $ket2 = "";
    $ket3 = "";
    if($id_return1 == "1"){
        $ket1 = "+Foto Kurang Lengkap";
        $error_count++;
    }

    if($id_return2 == "1"){
        $ket2 = "+Salah Input Penggunaan Material";
        $error_count++;
    }

    if($id_return2 == "1"){
        $ket3 = "+Hasil Ukur Under spec / Kosong";
        $error_count++;
    }

    $ket_all = $ket1.$ket2.$ket3;

    $keterangan_error = "[".substr($ket_all,1)."]";

}

if(count($komentars) > 0){
?>

Penyebab return :
</br>
<ul>
    <li>
    <?php echo $keterangan_error." : ".$detil_return." / ".$created_date  ?>
    </li>
</ul>
<hr/>


Perbaikan return :
</br>
<ul>
    <li>
    <?php echo $komentar." / ".$date_finish_update  ?>
    </li>
</ul>
<hr/>

<?php
}
?>
