<?php
if(Yii::app()->session['nik'] == ''){
	$this->redirect("index.php?r=site/login");
	exit();
}
?>
<?php $form=$this->beginWidget('CActiveForm', array(
																									'id'=>'ba_regional',
																									// Please note: When you enable ajax validation, make sure the corresponding
																									// controller action is handling ajax validation correctly.
																									// There is a call to performAjaxValidation() commented in generated controller code.
																									// See class documentation of CActiveForm for details on this.
																									'enableAjaxValidation'=>false,
																									'htmlOptions'=>array('class'=>'form-horizontal','method'=>'POST'),
																								)
															);
?>
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->

	<div class="row">


		<!-- end row -->
		<!-- begin row -->
		<!-- begin breadcrumb -->
	<!-- 	<ol class="breadcrumb pull-right">
			<li><a href="javascript:;">Home</a></li>
			<li><a href="javascript:;">Page Options</a></li>
			<li class="active">Blank Page</li>
		</ol> -->
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<!-- 	<h1 class="page-header">Welcome Page</h1> -->
		<!-- end page-header -->

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">SEARCH WO</h4>
					</div>

					<div class="panel-body">
						<div class="table-responsive">
						<form  action="" method="POST">

							<div class="form-group">

			 <div class="col-md-2" >
				 <?php echo $form->textField($model,'wo_number',array('class'=>'form-control','value'=>$wo_number,'placeholder'=>'Isikan No WO','name'=>'wo_number','id'=>'wo_number','method'=>'POST')); ?>
				 <?php echo $form->error($model,'wo_number'); ?>
			 </div>
			 <div class="col-md-2">
				 <!-- <?php
				 if($wo_number==''){?>
					 <script>alert("Silahkan isi No WO");</script>
			   <?php	} ?> -->
				 <button type="submit" id="filter" class="btn btn-sm btn-success">Filter</button>
			 </div>
					</div>

						</form>

						<h1></h1>

						<table class="table table-striped table-bordered">

								<tr class="success">
									<th><center>No</center></th>
									<th><center>Wo Number</center></th>
									<th><center>Nama Barang</center></th>
									<th><center>Jml Pemakaian</center></th>
                  <th><center>Date</center></th>
									<th><center>Nik Pemakai</center></th>
									<th><center>Nama</center></th>
								</tr>
                <?php

                $data_wo = $model->detailWo($wo_number);
                $no = 1;
                foreach ($data_wo as $wo) {
                ?>

                <tr class="info">
                  <td><?= $no++ ?></td>
                  <td><?= $wo->wo_number ?></td>
                  <td><?= $wo->nama_barang ?></td>
                  <td><?= $wo->jml_pemakaian ?></td>
                  <td><?= $wo->pemakaian_date ?></td>
                  <td><?= $wo->nik_pemakai ?></td>
                  <td><?= $wo->name ?></td>
                </tr>

              <?php } ?>
									<!-- </tbody> -->
								</table>

							</div>
						</div>
				  </div>
				</div>
			</div>
      <!-- end row -->
      </div>
      <!-- end #content -->


<?php $this->endWidget(); ?>
