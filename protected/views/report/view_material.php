<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-body">


    <h2>Material Alista</h2>
    <table class="table table-striped table-bordered">

        <tr class="success">
          <th><center>No</center></th>
          <th ><center>No WO</center></th>
          <th ><center>Nama Barang</center></th>
          <th ><center>Designator</center></th>
          <th><center>Jml<br>Pemakaian</center></th>
          <th><center>Jenis Kabel</center></th>
        </tr>
        <?php

        $no=1;
        $data_m = $model->dataMaterials($id);

        foreach ($data_m as $d) {
          $jenis_kabel = "-";
          if (strpos($d->id_barang, 'AC-OF') !== FALSE)
          {
           $jenis_kabel = "Kabel Udara";
          } else if(strpos($d->id_barang, 'DC-OF') !== FALSE){
            $jenis_kabel = "Kabel Tanah";
          }

          if($d->jml_pemakaian > 0){
          ?>
          <tr>
            <td><?= $no ?></td>
            <td><?= $d->wo_number?></td>
            <td><?= $d->nama_barang?></td>
            <td><?= $d->id_barang?></td>
            <td><?= $d->jml_pemakaian?></td>
            <td><?= $jenis_kabel?></td>
          </tr>

        <?php
          }
        $no++;
      } ?>
          <!-- </tbody> -->
    </table>
    <br/>
    <br/>
     <h2>Material Tambahan</h2>
    <table class="table table-striped table-bordered">

        <tr class="success">
          <th ><center>No</center></th>
          <th ><center>No WO</center></th>
          <th ><center>Designator</center></th>
          <th ><center>Satuan</center></th>
          <th ><center>Volume</center></th>
        </tr>

        <?php
          $data_m_tambahan = $material_tambahan->getMaterialTambahan($id);
          $no = 1;
          foreach ($data_m_tambahan as $d) {
            if($d->volume > 0){

          ?>
          <tr >
            <td><?= $no ?></td>
            <td><?= $d->no_wo?></td>
            <td><?= $d->designator?></td>
            <td><?= $d->satuan?></td>
            <td><?= $d->volume?></td>
          </tr>

        <?php
            }
            $no++;
          } ?>
</table>
</br>
          <h2>Jarak ODP - Rumah Pelanggan</h2>

          <table class="table table-striped table-bordered">

            <tr class="success">
              <th colspan="2"><center>Koordinat ODP</center></th>
              <th colspan="2"><center>Koordinat Rumah Pelanggan</center></th>
              <th rowspan="2"><center>Jarak ODP - Rumah<br> Pelanggan (Meter)</center></th>
            </tr>
            <tr class="active">
            <th >Longitude</th>
            <th >Latitude</th>
            <th >Longitude</th>
            <th >Latitude</th>
            </tr>
              <?php
              $q_longlat = $pemakaian->dataPemakaianRow($id);
              foreach ($q_longlat as $longlat) {
                $jarak = $longlat->jarak;

                if (is_numeric($jarak)) {
                  $summary = $jarak;
                }else{
                $urlapi = Alamatapi::model()->findByAttributes(array('menu'=>'lokasi_odp'));
                $url_api = $urlapi->url_api;
                //$key = '5b3ce3597851110001cf624858fb49c3aa14483f9f68cf0686672a8f';
                $longlat_odp = $longlat->lat_odp.",".$longlat->long_odp;
                $longlat_pel =$longlat->lat_pelanggan .",".$longlat->long_pelanggan;

                if ($longlat_odp != ',' AND $longlat_pel != ',') {
                  $url = $url_api.'&origins='.$longlat_odp.'&destinations='.$longlat_pel;
                  //echo $url;
                  $json   						= file_get_contents($url);

                  if (!$json) {
                    //$error = error_get_last();
                      $summary = 'Lokasi tidak ditemukan';

                      } else {

                        $data    = json_decode($json,true);
                        $summary = $data['rows'][0]['elements'][0]['distance']['value'];

                        if ($summary == '') {
                          $summary = 'Lokasi tidak ditemukan';
                        }

                      }

                }else {
                  $summary = 'Latitude Longitude NULL';
                }
                //$insert = Pemakaian::model()->updateJarak($id,$summary);
                //echo $summary;
                $command = Yii::app()->db->createCommand();
          							$command->update('pemakaian', array(
          									'jarak' => $summary,
          								), 'no_wo=:id', array(':id' => $id));

                        }
              ?>
                <tr>
                  <td><?=$longlat->long_odp?></td>
                  <td><?=$longlat->lat_odp?></td>
                  <td><?=$longlat->long_pelanggan?></td>
                  <td><?=$longlat->lat_pelanggan?></td>
                  <td><?=$summary?></td>
                </tr>
              <?php } ?>

                <!-- </tbody> -->
          </table>
            </div>
        </div>
    </div>
</div>
