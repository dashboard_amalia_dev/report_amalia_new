<?php
if(Yii::app()->session['nik'] == ''){
	$this->redirect("index.php?r=site/login");
	exit();
}

// $form=$this->beginWidget('CActiveForm', array(
// 'id'=>'approval_amalia',
// 'enableAjaxValidation'=>false,
// 'htmlOptions'=>array('class'=>'form-horizontal','method'=>'POST'),));
?>

<style type="text/css">

.myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

.myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

.myImg:hover {opacity: 0.7;}

/* The Modal (background) */

/* The Modal (background) */
.background_modal {
  display: none;  /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); Black w/ opacity
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)}
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)}
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}

	/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">

	var klikOn = 0;
	var id = "";
	function openCity(evt, cityName) {
	  var i, tabcontent, tablinks;
	  // Get all elements with class="tabcontent" and hide them
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }

	  // Get all elements with class="tablinks" and remove the class "active"
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }

	  document.getElementById(cityName).style.display = "block";
	  evt.currentTarget.className += " active";
	}

	function bukaPopUp(id){

		tablinks = document.getElementsByClassName("tablinks");

		tablinks[0].className = tablinks[0].className.replace("tablinks", "tablinks active");
		tablinks[1].className = tablinks[1].className.replace("tablinks active", "tablinks");
		tablinks[2].className = tablinks[2].className.replace("tablinks active", "tablinks");
		tablinks[3].className = tablinks[3].className.replace("tablinks active", "tablinks");

		tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }

		setTimeout(function () {
			document.getElementById('ba').style.display = "block";
			document.getElementById("content_ba").src="https://amalia.telkomakses.co.id/pdf/examples/amalia_for_digital_signatur.php?no_wo="+id;
		}, 1000);

		var base = '<?php echo Yii::app()->getBaseUrl(true)."/images/icon_loading.gif" ?>'
		document.getElementById("content_ba").src=base;
		document.getElementById("id_view").value = id;
		document.getElementById("hd_id").value = id;


	}

	$(document).ready(function(){
			$("#button_return").hide()
		  $("#tab_pelanggan").click(function(){
		  	var ini_id = $("#id_view").val();
			$.get("index.php?r=report/viewMaterial&id="+ini_id, function(data, status){
			    $("#table_material").html(data);
			});
		});

		$("#tab_cek_spek").click(function(){
			var loading = '<img src ="<?php echo Yii::app()->getBaseUrl(true)."/images/icon_loading.gif" ?>" style="width:100px"/>'
			$("#content_spek").html(loading);
		  	var ini_id = $("#id_view").val();
			$.get("index.php?r=report/CekSpek&id="+ini_id, function(data, status){
			    $("#content_spek").html(data);
			});
		  });

	   $("#approve_aso").click(function(){
	   		var id = $('#hd_id').val()
	   		var type = $('#type').val()
			$.get("index.php?r=report/ApproveBaUpdateToAso&id="+id+"&type="+type, function(data, status){
			    $("#table_material").html(data);
			});
		});

	  $("#tab_foto").click(function(){
	  	var ini_id = $("#id_view").val();
		$.get("index.php?r=report/dataFoto&id="+ini_id, function(data, status){
		    $("#table_foto").html(data);
		});

	  });

	  $("#tab_return").click(function(){
	  	var ini_id = $("#hd_id").val();
	  	// alert(ini_id)
		$.get("index.php?r=report/dataReturn&id="+ini_id, function(data, status){
		    $("#keterangan_return_content").html(data);
		    $("#perbaikan").prop("href", "index.php?r=report/perbaikanFoto&id="+ini_id)

		});
	  });

	  $("#return_save").click(function(){
		  	var id = $("#hd_id").val();
		  	var karena = $("#keterangan").val();
		  	var karena_detil = $("#keterangan_detil").val();
		  	if(karena_detil == ""){
		  		alert("Isi Terlebih dahulu Detil Keterangan")
		  		return true;
		  	}
				$.get("index.php?r=report/InsertReturn&id="+id+"&karena="+karena+"&karena_detil="+karena_detil, function(data, status){
				    $("#table_foto").html(data);
				});

			});


				  $("#button_return").click(function(){
					  var id = $('#hd_id').val()
						var comment_rev = $('#comment_revisi').val()
						// alert(comment_rev)
						// exit()

						// var test =$('#return_1').val()
						// alert(test)
						// exit()
					  // 	var karena1 = "0";
					  // 	var karena2 = "0";
					  // 	var karena3 = "0";
						if( $("#return_1").is(':checked')){
							return_1 = '1,'
						}else {
								return_1 = ''
								$( ".button_return" ).hide();
						}
						if( $("#return_2").is(':checked')){
							return_2 = '2,'
						}else {
							return_2 = ''
						}
						if( $("#return_3").is(':checked')){
							return_3 = '3,';
						}else {
							return_3 = ''
						}
						if( $("#return_4").is(':checked')){
							return_4 = '4,';
						}else {
							return_4 = ''
						}
						if( $("#return_5").is(':checked')){
							return_5 = '5,';
						}else {
							return_5 = ''
						}
						if( $("#return_6").is(':checked')){
							return_6 = '6,';
						}else {
							return_6 = ''
						}
						if( $("#return_7").is(':checked')){
							return_7 = '7,';
						}else {
							return_7 = ''
						}
						if( $("#return_8").is(':checked')){
							return_8 = '8,';
						}else {
							return_8 = ''
						}
						if( $("#return_9").is(':checked')){
							return_9 = '9,';
						}else {
							return_9 = ''
						}
						if( $("#return_10").is(':checked')){
							return_10 = '10,';
						}else {
							return_10 = ''
						}
						if( $("#return_11").is(':checked')){
							return_11 = '11,';
						}else {
							return_11 = ''
						}
						if( $("#return_12").is(':checked')){
							return_12 = '12,';
						}else {
							return_12 = ''
						}
						return_foto = return_1+return_2+return_3+return_4+return_5+return_6+return_7+return_8+return_9+return_10+return_11+return_12;
						return_poto = return_foto.slice(0,-1);
						// alert("Return to teknisi !")
						//exit()
						alert("Return to teknisi !")
					  // 	var karena_detil = $("#keterangan_detil").val();
					  // 	if(karena_detil == ""){
					  // 		alert("Isi Terlebih dahulu Detil Keterangan")
					  // 		return true;
					  // 	}
							$.get("index.php?r=report/InsertReturnTL&id="+id+"&return_foto="+return_poto+"&com_rev="+comment_rev, function(data, status){
							//$.get("index.php?r=report/InsertReturnTL&id="+id, function(data, status){

							    $("#table_foto").html(data);
							});
						});

	});
</script>

<input type="text" id="id_view" hidden>
<!-- modal -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->

<script src="assets/plugins/jquery/jquery-2.2.4.min.js"></script>
<script>
    $(document).ready(function(){
        $("#viewmaterial").dialog({
            autoOpen:false,
            modal:true,
            width:800,
            position:top,
            title:'Detail Material'
        });
    });

</script>
<script>
    function viewmaterial(id){
        id = id.trim().replace(/ /g, '+');
       $("#viewMaterial").html('<p><center><? echo CHtml::image("images/ajax-loader.gif", 'imageloading', array('height' => 200)) ?><center></p>');
        $("#viewmaterial").load("index.php?r=report/viewMaterial&id="+id).dialog("open");
    }
</script>
<!-- content -->
<div class="content-wrapper">
	<div class="content-wrapper-before"></div>
	<div class="content-header row">
		<div class="content-header-left col-md-4 col-12 mb-2">
			<h3 class="content-header-title">Inbox TL</h3>
		</div>
		<div class="content-header-right col-md-8 col-12">
			<div class="breadcrumbs-top float-md-right">
				<div class="breadcrumb-wrapper mr-1">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php?r=reservation/admin">List Inbox TL</a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="content-body">

		<section class="row">
				<div class="col-sm-12">
						<div id="with-header" class="card">
								<div class="card-header">
										<h4 class="card-title">List Inbox TL<br><br>
											<?php
												if($type == "1"){
													echo "<p style='color:blue'>Need Validate</p>";
												}else{
													echo "<p style='color:red'>Return</p>";
												}
											?>
										</h4>
										<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
										<div class="heading-elements">
												<ul class="list-inline mb-0">
														<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
														<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
												</ul>
										</div>
								</div>
								<div class="card-content collapse show">
										<div class="card-body border-top-blue-grey border-top-lighten-5 ">
												<div class="form-create">
													<?php
													$this->widget('zii.widgets.grid.CGridView',
														array(
															// 'id' => 'master-aset-grid',
															'itemsCssClass' => 'table table-striped table-bordered table-responsive',
															'htmlOptions' => array('class' => 'responsive','style'=>'width:100%'),
															'dataProvider' =>
																$model->getDataInboxTl($type),
																		'filter' => $model,
																		'columns' => array(
																			'regional',
																			'witel',
																			'sto',
																			'no_wo',
																			'nama_pelanggan',
																			'no_kontak',
																			'no_kontak_2',
																			'email_pelanggan',
											                				'no_inet',
																							'id_valins',
																							array(
																								'header'=>'Jenis Layanan',
																								'type'=>'raw',
																								'value'=>
																									'$data->jenis_layanan'
																							),
																			'create_dtm',

															array(
																'header'=>'View BA',
																'type'=>'raw',
																'value'=>
																	'TPemakaian::model()->viewMaterial($data->no_wo)'
															),

															),
														));
													?>
												</div>
										</div>
								</div>
						</div>
					</div>
	 </section>
	</div>
</div>
<!-- end content -->

<div id="viewmaterial"></div>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="modal" role="dialog" >
    <div class="modal-dialog" style="width: 870px">
      <!-- Modal content-->
      <div class="modal-content" style="width: 870px">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detail Amalia</h4>
          <br/>
          	<hr/>
          	<?php
			  	if($type == 1){
			  		?>
      <div align="center" class="col-md-12">
				<button id="approve_aso" class="btn btn-sm btn-success" data-dismiss="modal">Move Approval ASO</button>
				<button id="button_return" class="btn btn-sm btn-warning" style="display:none">Return to Teknisi</button>
			</div>
			<?php
			  	}
			?>
        </div>
        <div class="modal-body">
		<!-- Tab links -->
			<div class="tab" >
		  	<button class="tablinks" onclick="openCity(event, 'ba')">BA Digital</button>
		  	<button class="tablinks" id="tab_foto" onclick="openCity(event, 'foto')">Foto Evident</button>
		  	<button class="tablinks" id="tab_pelanggan" onclick="openCity(event, 'data_pelanggan')">Material Yang di gunakan</button>
		  	<button class="tablinks" id="tab_cek_spek" onclick="openCity(event, 'data_cek_spek')">Cek Spek</button>
		  <?php
		  	if($type != 1){
		  ?>
		  	<button style="background: #e00202;color: #FFFFFF;" class="tablinks" id="tab_return" onclick="openCity(event, 'keterangan_return')">Keterangan Return</button>
		  <?php
		  	}
		  ?>
			</div>

		<!-- Tab content -->
			<div id="ba" class="tabcontent active">
		  <br/>
		  <iframe
		   id ="content_ba"
		   style="height: 1200px;width: 800px"
		   src=""></iframe>
			</div>

		<input type="text" hidden id="hd_id">
		<input type="text" hidden id="type" value="<?php echo $type ?>">

		<div id="foto" class="tabcontent">
		<div align="center">
		  <div id="table_foto"></div>
		</div>
		</div>

		<!-- Tab content -->
		<div id="data_pelanggan" class="tabcontent">
		  	<br/>
			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-inverse">
			            <div class="panel-body">
						<div id="table_material"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

		<div id="data_cek_spek" class="tabcontent">
		  	<br/>
			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-inverse">
			            <div class="panel-body">
						<div id="content_spek" align="center"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

		<!-- Tab content -->
		<div id="keterangan_return" class="tabcontent">
		  	<br/>
			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-inverse">
			            <div class="panel-body">
						<div id="keterangan_return_content"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- zoom -->
<div class="container_zoom" data-dismiss="modal">
  <!-- Modal -->
  <div class="modal background_modal" id="zoom" role="dialog" >
    <div class="modal-dialog" align="center" >
      <!-- Modal content-->
      <!-- <div class="modal-content" > -->
			<img id="zoom_id" src="" style="height: 600px;width:600px"/>
      <!-- </div> -->
    </div>
  </div>
