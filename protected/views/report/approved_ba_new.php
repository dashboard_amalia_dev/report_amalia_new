<?php
if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
	$this->redirect("index.php?r=site/login");
	exit();
}

$nik	 = Yii::app()->session['nik'];
$user	 = new User();
$aso 	 = $user->IsAso($nik);

$nak = $master_sto->getReg($regional,$changed);
$wit = $master_sto->getWitelNew($regional,$witel,$changed);
$st  = $master_sto->getDataSto($regional,$witel,$sto,$changed);

// if($regional != "-" && $witel != "-" && $sto != "-"){
if($changed == "-"){
	// $regional = $nak[0]->reg;
	// $witel = $wit[0]->witel_versi_tactical;
	// $sto = $st[0]->sto;
}

?>

<style type="text/css">

	/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

/* Modal Content (image) */
 .modal-content {
  margin: auto;
  display: block;
}

/* Add Animation */
.modal-content, #caption {
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">

	var klikOn = 0;
	var id		 = "";
	function openCity(evt, cityName) {
	  var i, tabcontent, tablinks;
	  // Get all elements with class="tabcontent" and hide them
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }
	  // Get all elements with class="tablinks" and remove the class "active"
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(cityName).style.display = "block";
	  evt.currentTarget.className += " active";
	}

	function bukaPopUp(id){
		var base = '<?php echo Yii::app()->getBaseUrl(true)."/images/icon_loading.gif" ?>'
		document.getElementById("content_ba").src=base;

		tablinks = document.getElementsByClassName("tablinks");

		tablinks[0].className = tablinks[0].className.replace("tablinks", "tablinks active");
		tablinks[1].className = tablinks[1].className.replace("tablinks active", "tablinks");
		tablinks[2].className = tablinks[2].className.replace("tablinks active", "tablinks");

		tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }

		  document.getElementById('ba').style.display = "block";

		setTimeout(function () {

		document.getElementById("content_ba").src="https://amalia.telkomakses.co.id/pdf/examples/amalia_for_digital_signatur.php?no_wo="+id;

		}, 1000);

		document.getElementById("id_view").value = id;
		document.getElementById("hd_id").value = id;

		// tablinks = document.getElementsByClassName("tablinks");
		// tablinks[0].className = tablinks[0].className.replace("tablinks", "tablinks active");
		// document.getElementById('ba').style.display = "block";
		// document.getElementById("content_ba").src="http://alista.telkomakses.co.id/amalia/pdf/examples/isi_ba_v3_dev.php?no_wo="+id;
		}

	$(document).ready(function(){

		$("#tab_pelanggan").click(function(){
		  	var ini_id = $("#id_view").val();
			$.get("index.php?r=report/viewMaterial&id="+ini_id, function(data, status){
			    $("#table_material").html(data);
			});
		});

	   $("#approve_modal").click(function(){
	   		var id = $('#hd_id').val()
			$.get("index.php?r=report/ApproveBaUpdate&id="+id, function(data, status){
			    $("#table_material").html(data);
			});
		});

	  $("#tab_foto").click(function(){
	  	var ini_id = $("#id_view").val();
		$.get("index.php?r=report/dataFoto&id="+ini_id, function(data, status){
		    $("#table_foto").html(data);
		});
	   });

	  $("#regional").change(function(){
	  		var regional = $('#regional').val()
	  		var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
			window.location.replace("index.php?r=report/RekapBaNew&regional="+regional+"&date1="+date1+"&date2="+date2+"&changed=regional");
	   });

	  $("#witel").change(function(){
			var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
	  		var regional = $('#regional').val()
	  		var witel = $('#witel').val()
			window.location.replace("index.php?r=report/RekapBaNew&regional="+regional+"&witel="+witel+"&date1="+date1+"&date2="+date2+"&changed=witel");
	   });

	  $("#sto").change(function(){
			var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
	  		var regional = $('#regional').val()
	  		var witel = $('#witel').val()
	  		var sto = $('#sto').val()
			window.location.replace("index.php?r=report/RekapBaNew&regional="+regional+"&witel="+witel+"&sto="+sto+"&date1="+date1+"&date2="+date2+"&changed=sto");
	   });

	  $("#date2").change(function(){
				var date1 = $('#date1').val()
	  		var date2 = $('#date2').val()
	  		var regional = $('#regional').val()
	  		var witel = $('#witel').val()
	  		var sto = $('#sto').val()


				//alert(date1 + " test " + date2)
			window.location.assign("index.php?r=report/RekapBaNew&regional="+regional+"&witel="+witel+"&sto="+sto+"&date1="+date1+"&date2="+date2+"&changed=date");
	   });

     var date2 = $('#date2').val()
		 if (date2 != '') {
			 $("#date1").change(function(){
					 var date1 = $('#date1').val()
					 var date2 = $('#date2').val()
					 var regional = $('#regional').val()
					 var witel = $('#witel').val()
					 var sto = $('#sto').val()


					 //alert(date1 + " test " + date2)
				 window.location.assign("index.php?r=report/RekapBaNew&regional="+regional+"&witel="+witel+"&sto="+sto+"&date1="+date1+"&date2="+date2+"&changed=date");
				});
		 }


// 		$("#date2").datepicker({
//     onSelect: function(dateText) {
//         alert("Selected date: " + dateText + "; input's current value: " + this.value);
//     }
// });

	  $("#return_save").click(function(){
		  	var id = $("#hd_id").val();
		  	var karena = $("#keterangan").val();
		  	var karena_detil = $("#keterangan_detil").val();
		  	if(karena_detil == ""){
		  		alert("Isi Terlebih dahulu Detil Keterangan")
		  		return true;
		  	}
				$.get("index.php?r=report/InsertReturn&id="+id+"&karena="+karena+"&karena_detil="+karena_detil, function(data, status){
				    $("#table_foto").html(data);
				});

			});
	});

</script>

<input type="text" id="id_view" hidden>
<div class="content-wrapper">
	<div class="content-wrapper-before"></div>
	<div class="content-header row">
		<div class="content-header-left col-md-4 col-12 mb-2">
			<h3 class="content-header-title">Rekap Approval</h3>
		</div>
		<div class="content-header-right col-md-8 col-12">
			<div class="breadcrumbs-top float-md-right">
				<div class="breadcrumb-wrapper mr-1">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">REKAP BA INSTALASI</a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="content-body">

		<section class="row">
				<div class="col-sm-12">
						<div id="with-header" class="card">
								<div class="card-header">
										<h4 class="card-title">LIST REKAP BA INSTALASI</h4>
										<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
										<div class="heading-elements">
												<ul class="list-inline mb-0">
														<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
														<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
												</ul>
										</div>
								</div>
								<div class="card-content collapse show">
										<div class="card-body border-top-blue-grey border-top-lighten-5 ">
												<div class="form-create">
													<form  action="" method="POST">
														<div class="form-group">
															<?php
															// hari ini kembali
															echo"	<table align='center' style='margin-left:-5px'><tr>
																	<td width='100px'><div>Regional</div></td>
																		<td> :&nbsp</td>
																	<td width='150px'>";
																	echo '<select class="form-control" name="regional" id="regional" method="POST">';

																	if($level == "1"){
																		echo '<option value="All">All</option>';
																	}

																	$nak = $master_sto->getReg($regional,$level);
																	foreach($nak as $s){

																		if($s->reg == $regional){
																			echo '<option selected value="'.$s->reg.'">'.$s->reg.'</option>';
																		}else{
																			echo '<option value="'.$s->reg.'">'.$s->reg.'</option>';
																		}
																	}
																	echo'</select>';
																	echo "</td>";
																	echo "</tr>";
																	//echo "</table>";
																	echo "<tr>
																		<td colspan='3'><br/></td>
																	</tr>
																	<tr>";
															 ?>

																 <?php
															 		if($changed == "-" && ($level == 2 || $level == 3 || $level == 4) ){
																		$nak = $master_sto->getReg($regional,$level);
																		$regional = $nak[0]->reg;

																	}

																	// hari ini kembali
																	echo"<tr>
																		<td width='100px'><div>Witel</div></td>
																			<td> :&nbsp</td>
																		<td width='150px'>";
																	echo '<select class="form-control" name="witel" id="witel" method="POST">';


																	if(count($master_sto->checkWitelFromRegional($regional)) == count( $master_sto->getWitelNew($regional,$witel,$changed))){
																	// if($level == "1" || $level == "2"){
																		echo '<option value="All">All</option>';
																	}



																	$wit = $master_sto->getWitelNew($regional,$witel,$changed);
																	foreach($wit as $s){
																		if($s->witel_versi_tactical == $witel){
																			echo '<option selected value="'.$s->witel_versi_tactical.'">'.$s->witel_versi_tactical.'</option>';
																		}else{
																			echo '<option value="'.$s->witel_versi_tactical.'">'.$s->witel_versi_tactical.'</option>';
																		}
																	}
																	echo'</select>';
																	echo "</td>";
																	echo "</tr>";
																	//echo "</table>";
																	echo "<tr>
																		<td colspan='3'><br/></td>
																	</tr>
																	<tr>";

																	// untuk mendapatkan index dari filter witel
																	if($witel == "-"){
																		if($changed == "-" || ( $level == 3 || $level == 4)){
																			$wit = $master_sto->getWitelNew($regional,$witel,$changed);
																			$witel = $wit[0]->witel_versi_tactical;
																		}else if($changed != "-" && count($master_sto->checkWitelFromRegional($regional)) != count( $master_sto->getWitelNew($regional,$witel,$changed))){
																			$wit = $master_sto->getWitelNew($regional,$witel,$changed);
																			$witel = $wit[0]->witel_versi_tactical;
																		}
																	}
															 ?>

															 <?php
																	// hari ini kembali
																	echo"<tr>
																		<td width='100px'><div>STO</div></td>
																			<td> :&nbsp</td>
																		<td width='150px'>";
																	echo '<select class="form-control" name="sto" id="sto" method="POST">';
																	if(count($master_sto->checkStoFromWitel($witel)) == count( $master_sto->getDataSto($regional,$witel,$sto,$changed))){
																		echo '<option value="All">All</option>';
																	}

																	$st = $master_sto->getDataSto($regional,$witel,$sto,$changed);
																	foreach($st as $s){
																		if($s->sto == $sto){
																			echo '<option selected value="'.$s->sto.'">'.$s->sto.'</option>';
																		}else{
																			echo '<option value="'.$s->sto.'">'.$s->sto.'</option>';
																		}
																	}
																	echo'</select>';
																	echo "</td>";
																	echo "</tr>";
																	//echo "</table>";
																	echo "<tr>
																		<td colspan='3'><br/></td>
																	</tr>
																	<tr>";

																	if($sto == "-"){
																		if($changed == "-" && $level == 4){
																			$st = $master_sto->getDataSto($regional,$witel,$sto,$changed);
																			$sto = $st[0]->sto;
																		}else if($changed != "-" && count($master_sto->checkStoFromWitel($witel)) != count( $master_sto->getDataSto($regional,$witel,$sto,$changed))){
																			$st = $master_sto->getDataSto($regional,$witel,$sto,$changed);
																			$sto = $st[0]->sto;
																		}

																	}

															 ?>

																<tr>
																	<td ><label style="width:100px">Start Date</label></td>
																	<td>: &nbsp;</td>
																	<td><input  id="date1" name="date1" value="<?php if($date1 !="-") {echo $date1;} ?>"  type="date" class="form-control" style="width:200px" placeholder="date" /></td>
																</tr>
																<tr>
																	<td colspan="3"><br/></td>
																</tr>
																<tr>
																	<td ><label style="width:100px">End Date</label></td>
																	<td>: &nbsp;</td>
																	<td><input id="date2" name="date2" value="<?php if($date2 !="-") {echo $date2;} ?>" type="date" class="form-control" style="width:200px" /></td>
																</tr>
																<tr>
																	<tr>
																		<td colspan='3'><br/></td>
																	</tr>
																	<tr>
																	<td colspan="2">
																		<!-- <button type="submit" class="btn btn-primary btn-sm" class="btn-submit-approval" id="search_date">Submit</button> -->

																	</td>
																</tr>
															</table>

															<br/>
															<!-- <button type="submit" class="btn btn-primary btn-sm" class="btn-submit-approval" id="search_date">Submit</button>
															<br/><br/> -->
														 </div>

														<div class="col-md-2">

													 </div>
													 <div class="col-md-2" >

													 </div>
													 <div class="col-md-2">

														 <!-- <button type="submit" id="filter" class="btn btn-sm btn-success">Filter</button> -->
													 </div>
															</div>

																</form>

													<?php
													// widget

													?>
													<?php
														if($regional != "-" && $witel != "-" && $sto != "-"){
													?>
														<?php
														$this->widget('zii.widgets.grid.CGridView',
															array(
																// 'id' => 'master-aset-grid',
																'itemsCssClass' => 'table table-striped table-bordered table-responsive',
																'htmlOptions' => array('class' => 'responsive','style'=>'width:100%'),
																'dataProvider' =>
																	$model->dataRekapNewWithSto($regional,$witel,$sto,$date1,$date2),
																			'filter' => $model,
																			'columns' => array(
																				'regional',
																				'witel',
																				'sto',
																				'no_wo',
																								'no_inet',
																								'id_valins',
																								'nama_pelanggan',
																								array(
																									'header'=>'Janis Layanan',
																									'type'=>'raw',
																									'value'=>
																										'$data->jenis_layanan'
																								),
																				'ukur_usage',
																				array(
																					'header'=>'Status Order',
																					'type'=>'raw',
																					'value'=>
																						'$data->status_approve'
																				),

																array(
																	'header'=>'View BA',
																	'type'=>'raw',
																	'value'=>
																		'TPemakaian::model()->viewMaterialApproved($data->no_wo)'
																),

																),
															));
															?>
															<div class="col-md-3">
															<?php
															$data_reg = $model->dataRekapNewSto($regional,$witel,$sto,$date1,$date2);
															if(count($data_reg) > 0){
																if( $witel != ""){
																	echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&excel=1&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'">Export excel</a>  &nbsp;';

																	echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'&material=1">Export Material</a>';

																}
															}
															?>
															</div>
															</div>
														</div>
														</div>
														</div>
														</div>
													<?php
														}else if($regional != "-" && $regional != "" && $witel !="-" && $witel !=""){
													?>
														<table class="table table-bordered table-striped table-responsive"  width="2000px">

															<tr class="success">
																<th rowspan="2" width="50px"><center style="margin-top:30px">No</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">Regional</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">Witel</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">STO</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">Need Validate</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">Need Approve</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">Approved</center></th>
																<th colspan="3" width="585px"><center>Not Aproved</center></th>
																<th rowspan="2" width="195px"><center style="margin-top:30px">Total</center></th>

															</tr>
															<tr class="active">
															<th width="195px">Foto</th>
															<th width="195px">BA</th>
															<th width="195px">Unspek</th>

															</tr>

															<?php
															$total_need_validate 	= 0;
															$total_need_approve 	= 0;
															$total_approved 			= 0;

															$total_kurang_foto	= 0;
															$total_kurang_ba		= 0;
															$total_unspek				= 0;
															$total_jumlah_ba		= 0;

															$data_reg = $model->dataRekapNewSto($regional,$witel,$sto,$date1,$date2);
															$no 			= 1;
															foreach ($data_reg as $reg) {

																$need_validate 	= $reg->need_validate;
																$need_approve 	= $reg->need_approve;
																$approved 			= $reg->approved;
																$kurang_foto	  = $reg->kurang_foto;
																$kurang_ba 			= $reg->kurang_ba;
																$unspek 				= $reg->unspek;

																$jumlah_ba = intval($need_validate) + intval($need_approve) + intval($approved) + intval($kurang_foto) + intval($kurang_ba) + intval($unspek)  ;
																// $total = intval($reg->jml_ps);

																?>
																<tr class="info">
																	<td><?= $no ?></td>
																	<td><?= $reg->regional?></td>
																	<td><?= $reg->witel?></td>
																	<td><?= $reg->sto?></td>
																	<td><?= $reg->need_validate?></td>
																	<td>
																	<?php
																	if($need_approve > 0 && count($aso) > 0){ echo CHtml::link($need_approve ,array('report/showApprovalBa&regional='.$regional.'&changed=regional')); }else{ echo $need_approve; }
																	?>
																	</td>
																	<td><?php
																		if($approved > 0){ echo CHtml::link($approved ,array('report/approvedBa&regional='.$regional.'&changed=regional')); }else{ echo $approved; }

																		?>
																	</td>
																	<td>
																	<?php
																	 echo CHtml::link($kurang_foto ,array('report/ShowReturn&status=1&regional='.$regional.'&witel='.$reg->witel.'&sto='.$reg->sto.'&date1='.$date1.'&date2='.$date2));
																	?>
																	</td>
																	<td>
																	<?php
																	 echo CHtml::link($kurang_ba ,array('report/ShowReturn&status=2&regional='.$regional.'&witel='.$reg->witel.'&sto='.$reg->sto.'&date1='.$date1.'&date2='.$date2));
																	?>
																	</td>
																	<td>
																	<?php
																	 echo CHtml::link($unspek ,array('report/ShowReturn&status=3&regional='.$regional.'&witel='.$reg->witel.'&sto='.$reg->sto.'&date1='.$date1.'&date2='.$date2));
																	?>
																	</td>

																	<td><?= $jumlah_ba?></td>
																</tr>
															<?php
																$no++;

																$total_need_validate = $total_need_validate + intval($need_validate);
																$total_need_approve  = $total_need_approve + intval($need_approve);
																$total_approved 		 = $total_approved + intval($approved);

																$total_kurang_foto	= intval($kurang_foto) + $total_kurang_foto;
																$total_kurang_ba		= intval($kurang_ba) + $total_kurang_ba;
																$total_unspek				= intval($unspek) + $total_unspek;
															}
															?>
																<tr>
																		<td colspan="4">
																			<b>Grand Total</b>
																		</td>
																		<td >
																			<b><?= $total_need_validate ?></b>
																		</td>
																		<td >
																			<b><?= $total_need_approve ?></b>
																		</td>
																		<td>
																			<b><?= $total_approved ?></b>
																		</td>
																		<td>
																			<b><?= $total_kurang_foto ?></b>
																		</td>
																		<td>
																			<b><?= $total_kurang_ba ?></b>
																		</td>
																		<td>
																			<b><?= $total_unspek ?></b>
																		</td>

																	</tr>
															</table>
															<?php
															if(count($data_reg) > 0){
																if( $witel != ""){
																	echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&excel=1&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'">Export excel</a>  &nbsp;';

																	echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'&material=1">Export Material</a>';

																}
															}
															?>
															</div>
															</div>
															</div>
															</div>
															</div>

													<?php
														}else if($regional != "-"){
													?>
														<table class="table table-bordered table-striped table-responsive"  width="2000px">

															<tr class="success">
																<th rowspan="2" width="200px"><center style="margin-top:30px">No</center></th>
																<th rowspan="2" width="200px"><center style="margin-top:30px">Regional</center></th>
																<th rowspan="2" width="200px"><center style="margin-top:30px">Witel</center></th>
																<th rowspan="2" width="200px"><center style="margin-top:30px">Need Validate</center></th>
																<th rowspan="2" width="200px"><center style="margin-top:30px">Need Approve</center></th>
																<th rowspan="2" width="200px"><center style="margin-top:30px">Approved</center></th>
																<th colspan="3" width="600px"><center>Not Aproved</center></th>
																<th rowspan="2" width="200px"><center style="margin-top:30px">Total</center></th>

															</tr>
															<tr class="active">
															<th width="200px">Foto</th>
															<th width="200px">BA</th>
															<th width="200px">Unspek</th>

															</tr>
															<?php
																// witel
																$total_need_validate 	= 0;
																$total_need_approve 	= 0;
																$total_approved 			= 0;

																$total_kurang_foto	= 0;
																$total_kurang_ba		= 0;
																$total_unspek				= 0;

															$data_reg = $model->dataRekapNewWitel($regional,$witel,$sto,$date1,$date2);
															$no 			= 1;
															foreach ($data_reg as $reg) {
																$need_validate 	= $reg->need_validate;
																$need_approve 	= $reg->need_approve;
																$approved 			= $reg->approved;
																$kurang_foto 		= $reg->kurang_foto;
																$kurang_ba 			= $reg->kurang_ba;
																$unspek 				= $reg->unspek;

																$jumlah_ps =  intval($need_validate) + intval($need_approve) + intval($approved) + intval($kurang_foto) + intval($kurang_ba) + intval($unspek) ;
																// $total = intval($reg->jml_ps);
																?>
																<tr class="info">
																	<td><?= $no ?></td>
																	<td><?= $reg->regional ?></td>
																	<td><?= $reg->witel ?></td>
																	<td><?= $reg->need_validate ?></td>
																	<td><?php
																	if($need_approve > 0 && count($aso) > 0){ echo CHtml::link($need_approve ,array('report/showApprovalBa&regional='.$regional.'&changed=regional')); }else{ echo $need_approve; }
																			?>
																	</td>
																	<td><?php
																		if($approved > 0){ echo CHtml::link($approved ,array('report/approvedBa&regional='.$regional.'&changed=regional')); }else{ echo $approved; }
																			?>
																	</td>
																	<td>
																	<?php
																	 echo CHtml::link($kurang_foto ,array('report/ShowReturn&status=1&regional='.$regional.'&witel='.$reg->witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2));
																	?>
																	</td>
																	<td>
																	<?php
																	 echo CHtml::link($kurang_ba ,array('report/ShowReturn&status=2&regional='.$regional.'&witel='.$reg->witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2));
																	?>
																	</td>
																	<td>
																	<?php
																	 echo CHtml::link($unspek ,array('report/ShowReturn&status=3&regional='.$regional.'&witel='.$reg->witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2));
																	?>
																	</td>

																	<td><?=$jumlah_ps?></td>
																</tr>
															<?php
																$no++;

																$total_need_validate = $total_need_validate + intval($need_validate);
																$total_need_approve	 = $total_need_approve + intval($need_approve);
																$total_approved 		 = $total_approved + intval($approved);

																$total_kurang_foto	= intval($kurang_foto) + $total_kurang_foto;
																$total_kurang_ba		= intval($kurang_ba) + $total_kurang_ba;
																$total_unspek				= intval($unspek) + $total_unspek;

																$total_jumlah_ba		= intval($jumlah_ps) + $total_jumlah_ba;
															}
															?>
																	<tr>
																		<td colspan="3">
																			<b>Grand Total</b>
																		</td>
																		<td >
																			<b><?= $total_need_validate ?></b>
																		</td>
																		<td >
																			<b><?= $total_need_approve ?></b>
																		</td>
																		<td>
																			<b><?= $total_approved ?></b>
																		</td>
																		<td>
																			<b><?= $total_kurang_foto ?></b>
																		</td>
																		<td>
																			<b><?= $total_kurang_ba ?></b>
																		</td>
																		<td>
																			<b><?= $total_unspek ?></b>
																		</td>
																		<td>
																			<b><?= $total_jumlah_ba ?></b>
																		</td>
																	</tr>
															</table>
															<?php
															if(count($data_reg) > 0){
																	echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&excel=1&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'">Export excel</a>  &nbsp;';
																	echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&regional='.$regional.'&witel='.$witel.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2.'&material=1">Export Material</a>';
															}
															?>
															</div>
															</div>
															</div>
															</div>
															</div>
													<?php
														}else{
															if($regional == "-"){
													 ?>
																<table class="table table-bordered table-striped table-responsive"  width="2000px">
																	<tr class="success">
																		<th rowspan="2" width="80px"><center style="margin-top:30px">No</center></th>
																		<th rowspan="2" width="240px"><center style="margin-top:30px">Regional</center></th>
																		<th rowspan="2" width="240px"><center style="margin-top:30px">Need Validate</center></th>
																		<th rowspan="2" width="240px"><center style="margin-top:30px">Need Approve</center></th>
																		<th rowspan="2" width="240px"><center style="margin-top:30px">Approved</center></th>
																		<th colspan="3" width="1080px"><center style="margin-top:30px">Not Aproved</center></th>
																		<th rowspan="2" width="240px"><center style="margin-top:30px">Total</center></th>
																	</tr>
																	<tr class="active">
																		<th width="240px">Foto</th>
																		<th width="240px">BA</th>
																		<th width="240px">Unspek</th>
																	</tr>
																	<?php

																	$data_regg 			 = $model->dataRekapNewRegional($regional,$witel,$sto,$date1,$date2);
																	$no 						 = 1;
																	// $regional_origin = $regional;
																	// $witel_origin 	 = $witel;
																	// $sto_origin 		 = $sto;

																	$total_need_validate 	= 0;
																	$total_need_approve 	= 0;
																	$total_approved 			= 0;

																	$total_kurang_foto		= 0;
																	$total_kurang_ba			= 0;
																	$total_unspek					= 0;
																	$total_jumlah_ba			= 0;

																	foreach ($data_regg as $reg) {
																		$need_validate 		= $reg->need_validate;
																		$need_approve 		= $reg->need_approve;
																		$approved 				= $reg->approved;
																		$kurang_foto 			= $reg->kurang_foto;
																		$kurang_ba 				= $reg->kurang_ba;
																		$unspek 					= $reg->unspek;
																		$regional 				= $reg->regional;

																		$jumlah_ba = intval($need_validate) + intval($need_approve) + intval($approved) + intval($kurang_foto) + intval($kurang_ba) + intval($unspek) ;
																		// $total = intval($reg['jml_ps']);
																		?>
																		<tr class="info">
																			<td><?= $no ?></td>
																			<td><?= $regional?></td>
																			<td><?= $need_validate?></td>
																			<td>
																			<?php
																				if($need_approve > 0 && count($aso) > 0){ echo CHtml::link($need_approve ,array('report/showApprovalBa&regional='.$regional.'&changed=regional')); }else{ echo $need_approve; }
																			?>
																			</td>
																			<td>
																		  <?php
																				if($approved > 0){ echo CHtml::link($approved ,array('report/approvedBa&regional='.$regional.'&changed=regional')); }else{ echo $approved; }
																			?>
																			</td>
																			<td>
																			<?php
																			 echo CHtml::link($kurang_foto ,array('report/ShowReturn&status=1&regional='.$regional.'&witel='.$witel2.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2));
																			?>
																			</td>
																			<td>
																			<?php
																			 echo CHtml::link($kurang_ba ,array('report/ShowReturn&status=2&regional='.$regional.'&witel='.$witel2.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2));
																			?>
																			</td>
																			<td>
																			<?php
																			 echo CHtml::link($unspek ,array('report/ShowReturn&status=3&regional='.$regional.'&witel='.$witel2.'&sto='.$sto.'&date1='.$date1.'&date2='.$date2));
																			?>
																	  	</td>
																			<td><?echo CHtml::link($jumlah_ba ,array('report/ShowAllRekap&regional='.$regional.'&changed=regional'));?></td>
																		</tr>

																	<?php
																		$no++;
																		$total_need_validate 	= $total_need_validate + intval($need_validate);
																		$total_need_approve 	= $total_need_approve + intval($need_approve);
																		$total_approved 			= $total_approved + intval($approved);

																		$total_kurang_foto		= intval($kurang_foto) + $total_kurang_foto;
																		$total_kurang_ba			= intval($kurang_ba) + $total_kurang_ba;
																		$total_unspek					= intval($unspek) + $total_unspek;

																		$total_jumlah_ba			= intval($jumlah_ba) + $total_jumlah_ba;
																	}
																	?>
																	<tr>
																		<td colspan="2">
																			<b>Grand Total</b>
																		</td>
																		<td >
																			<b><?= $total_need_validate ?></b>
																		</td>
																		<td >
																			<b><?= $total_need_approve ?></b>
																		</td>
																		<td>
																			<b><?= $total_approved ?></b>
																		</td>
																		<td>
																			<b><?= $total_kurang_foto ?></b>
																		</td>
																		<td>
																			<b><?= $total_kurang_ba ?></b>
																		</td>
																		<td>
																			<b><?= $total_unspek ?></b>
																		</td>
																		<td>
																			<b><?= $total_jumlah_ba ?></b>
																		</td>

																	</tr>
																	</table>
																	<?php
																	// if(count($data_reg) > 0){
																	// 	if( $witel_origin != ""){
																	// 		echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&excel=1&regional='.$regional_origin.'&witel='.$witel_origin.'&sto='.$sto_origin.'&date1='.$date1.'&date2='.$date2.'">Export excel</a>  &nbsp;';
																	// 		echo '<a class="btn btn-sm btn-success" href="/report_amalia_new/index.php?r=report/RekapBaNew&regional='.$regional_origin.'&witel='.$witel_origin.'&sto='.$sto_origin.'&date1='.$date1.'&date2='.$date2.'&=1">Export Material</a>';
																	// 	}
																	// }
																	?>
																	</div>
																	</div>
																	</div>
																	</div>
																	</div>
													<?php
															}
															//end if regional == "-"
														}
														// end if change mode
												 	?>
												</div>
										</div>
								</div>
						</div>
					</div>
	 		</section>
	</div>
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="modal" role="dialog" >
    <div class="modal-dialog" style="width: 870px">

      <!-- Modal content-->
      <div class="modal-content" style="width:900px">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Detail BA Instalasi</h4>
          <br/>
          	<hr/>

        	<div align="center" class="col-md-12">
				<!-- <button id="approve_modal" class="btn btn-sm btn-success" data-dismiss="modal">Approve</button>&nbsp;<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#return">Return</button> -->
			</div>

        </div>
        <div class="modal-body">

		<!-- Tab links -->
		<div class="tab" >
		  <button class="tablinks" onclick="openCity(event, 'ba')">BA Digital</button>
		  <button class="tablinks" id="tab_foto" onclick="openCity(event, 'foto')">Foto Evident</button>
		  <button class="tablinks" id="tab_pelanggan" onclick="openCity(event, 'data_pelanggan')">Material Yang Digunakan</button>
		</div>

		<!-- Tab content -->
		<div id="ba" class="tabcontent active">
		  <br/>
		  <iframe
		   id ="content_ba"
		   style="height: 1200px;width: 800px"
		   src=""></iframe>

		</div>

		<input type="text" hidden id="hd_id">

		<div id="foto" class="tabcontent">
		<div align="center">
		  <div id="table_foto"></div>
		</div>
		</div>

		<!-- Tab content -->
		<div id="data_pelanggan" class="tabcontent">
		  	<br/>
			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-inverse">
			            <div class="panel-body">
						<div id="table_material"></div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
</div>
  <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="return" role="dialog" >
    <div class="modal-dialog" style="width: 870px">

      <!-- Modal content-->
      <div class="modal-content" style="width:870px">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Return Amalia</h4>
        </div>
        <div class="modal-body">

		Return Karena :
		<select class="form-control"  id="keterangan">
			<option value="1" selected>Foto Kurang Lengkap / Tidak sesuai</option>
			<option value="2">BA Kurang Lengkap</option>
			<option value="2">Foto dan BA Tidak Sesuai</option>
		</select>
		<br/>

		Detil Keterangan :
		<textarea class="form-control" id="keterangan_detil" rows="6" id="keterangan_return"></textarea>

		<input type="text" hidden id="hd_id_return">
		<!-- Tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="return_save" class="btn btn-default" data-dismiss="modal">Return Save</button>
        </div>
      </div>

    </div>
  </div>

</div>


<!-- <div class="modal fade text-left" id="myModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content" style="width:850px">
			<div class="modal-header bg-info white">
				<h4 class="modal-title white" id="title-modal"><i class="la la-tree"></i></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

			</div>
		</div>
	</div>
</div> -->
