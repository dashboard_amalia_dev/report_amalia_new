<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	 public function actionIndex() {
			 if (Yii::app()->user->isGuest) {
					 $this->redirect(array('site/login'));
			 }
			 // else {
				// 	 if (isset($_GET['Gudang'])) {
				// 			 $model->attributes = $_GET['Gudang'];
				// 	 }
			 // }
			 $this->render('index', array('model' => $model));
//		 renders the view file 'protected/views/site/index.php'
//		 using the default layout 'protected/views/layouts/main.php'
	 }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	 public function actionLogin() {

			 $this->layout 		 = '//layout/nolayout';
			 $modelmantainance = Mantainance::model()->findByPk(1);
			 $model 		   = new LoginForm;
			 $model_user 	   = new User;
			 if (!empty($modelmantainance)) {
					 if($modelmantainance->status == 0){
						 $this->renderPartial('undercontruction');
						 exit();
					 }
			 }

			 $model = new LoginForm;

			if (isset($_GET['code'])) {
				$enc = $_GET['code'];
		
				$now = date("Ymd");
				$value = md5($now);
				$md3 = substr($value,0,3);
				$md_last = substr($value,3);
				$length_value = strlen($value)+2;
				$two_front = substr($enc,3,2);
				$null = substr($two_front,0,2) == '00' ? '00' : '';
				$back = substr($enc,$length_value);
				$hasil_hitung = $two_front.$back;
				$decrypted_ = ($hasil_hitung/3)-3;
				$decrypted = $null.$decrypted_;
				$nik = $decrypted;
		
		
				$model->username = $nik;
				$model->password = 'amalia#2020';
				//$userResult = Authassignment::model()->findByAttributes(array('userid'=>$model->username, 'status'=>'Y'));
				$userResult =  UserBa::model()->findByAttributes(array(
																																		'nik' => $model->username,
																																		'status'=>'Y'
																																		),
																																		array( 'limit' => 1,)
																															);
				if(empty($userResult)){
					$this->layout = '//layout/nolayout';
							$this->render('userNotFound');
							exit();
				}
				// validate user input and redirect to the previous page if valid
				if ($model->validate() && $model->login()) {
						//$cekuser = Authassignment::model()->findByAttributes(array('userid'=>$model->username, 'status'=>'Y'));
						$cekuser = UserBa::model()->findByAttributes(array(
																																				'nik' => $model->username,
																																				'status'=>'Y'
																																				),
																																				array( 'limit' => 1,)
																																	);
						if(!empty($cekuser)){
								$getnamauser = Pegawai::model()->getInitiateName($nik);
								$cekuser->employee_name = $getnamauser;
								$cekuser->save();
								$connection = Yii::app()->db;
								$tglsekarang = date("Y-m-d");
								$sql = "delete from user_log where login_time < '" . $tglsekarang . "' ";
								$command = $connection->createCommand($sql);
								$command->execute();
		
								$modeluserlog = new Userlog ();
								$modeluserlog->nik = Yii::app()->user->id;
								$modeluserlog->ip_address = $_SERVER["REMOTE_ADDR"];
								$modeluserlog->login_time = date("Y-m-d H:i:s");
								$modeluserlog->save();
								$this->redirect(Yii::app()->user->returnUrl);
						}else{
							$this->layout = '//layout/nolayout';
							$this->render('forbiddenLogin');
							exit();
						}
				}
		 }else{

			 // if it is ajax validation request
			 if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
					 echo CActiveForm::validate($model);
					 Yii::app()->end();
			 }

			 // collect user input data
			 if (isset($_POST['LoginForm'])) {
					 $model->attributes = $_POST['LoginForm'];
					 $nik = $_POST['LoginForm']['username'];

					 $cekaso = UserBa::model()->findByAttributes(array(
						'nik' => $model->username,
						'jabatan'=>'aso'
						),
							 array('limit' => 1));

					 //kesel aing ga lolos security cek
					 $expr = '/^[1-9][0-9]*$/';
					 	if (preg_match($expr,$nik)) {
						}else {
					$this->redirect(array('site/login'));
					exit();
					}

					if(!empty($cekaso)){
					
					}else{
						$this->layout = '//layout/nolayout';
						$this->render('forbiddenLogin');
						exit();
					}

					 $data = $model_user->getLokasi($nik);
						 $kosong = 0;
						 foreach ($data as $k) {

							 if($k->lokasi == ""){
								 $kosong++;
							 }
						}
					 // validate user input and redirect to the previous page if valid
					 if ($model->validate() && $model->login()) {
							 //$cekuser = Authassignment::model()->findByAttributes(array('userid'=>$model->username, 'status'=>'Y'));
							 $cekuser = UserBa::model()->findByAttributes(array(
																				'nik' => $model->username,
																				'status'=>'Y'
																				),
																					 array('limit' => 1));
							 if(!empty ($cekuser)){
									 $getnamauser = Pegawai::model()->getInitiateName($model->username);
									 $cekuser->employee_name = $getnamauser;
									 $cekuser->save();
									 $connection = Yii::app()->db;
									 $tglsekarang = date("Y-m-d");
									
									 $modeluserlog = new Userlog ();
									 $modeluserlog->nik = Yii::app()->user->id;
									 $modeluserlog->ip_address = $_SERVER["REMOTE_ADDR"];
									 $modeluserlog->login_time = date("Y-m-d H:i:s");
									 $modeluserlog->save();
									 
									 $this->redirect(array('report/home'));
								
							 }else{
									 $this->redirect(array('site/login'));
							 }
					 }else if($kosong > 0){

							echo "<script type='text/javascript'>
		  							alert('Maaf Lokasi Anda Kosong !')
		  							</script>";

		  			}else{

						$m_tl  			= new Tl();
		 				$m_result 		= $m_tl->isTl($nik);
		 				$sto 			= $m_result[0]->WORKZONE;
		 				$nama 			= $m_result[0]->nama_tl;
		 				$m_res_user 	= $model_user->checkUser($nik);

		 				if(count($m_res_user) < 1 && count($m_result) > 0){
		 					$model_user->nik = $nik;
		 					$model_user->nama = $nama;
		 					$model_user->jabatan = 'Tl';
		 					$model_user->lokasi = $sto;
		 					$model_user->save();
		 				}
		  			}
			 }
			 // display the login form
			 $this->render('newlogin', array('model' => $model));
			}
	 }

	 public function actionUpdatedata(){
			 $this->layout = '//layout/nolayout';
			 $niklama = Yii::app()->session['niklama'];
			 if(isset($_POST['nik'])){
					 $nik = $_POST['nik'];
					 //cek di table user_ba
					 $authassignment = UserBa::model()->findByAttributes(array(
																																			 'nik' => $niklama,
																																			 ),
																																			 array( 'limit' => 1,)
																																 );
					 if(!empty($authassignment) && $authassignment->status == "Y"){
							 //user tercatat di aplikasi, kemudian update data nik lama
							 $listtabel = UpdateNik::model()->findAll();
							 if(!empty($listtabel)){
									 $allquery = Array();
									 foreach ($listtabel as $value) {
											 $detail = UpdateNikDetail::model()->findAllByAttributes(array('id_tabel'=>$value->id_tabel));
											 if(!empty($detail)){
													 foreach ($detail as $valdetail) {
															 $namaatribut = $valdetail->nama_atribut;
															 $myUpdate = "UPDATE " . $value->nama_tabel . " SET " . $namaatribut . " = '" . $nik . "' WHERE " . $namaatribut . " = '" . $niklama . "'";
															 $allquery[] = $myUpdate;
													 }
											 }
									 }
							 }

							 //try catch
							 $connection = Yii::app()->db;
							 $jml = count($allquery);
							 $transaction = $connection->beginTransaction();
							 try{
									 for($i = 0 ; $i < $jml ; $i++){
											$connection->createCommand($allquery[$i])->execute();
									 }
									 $transaction->commit();
									 if(true){
											 //absensi insert
											 $namaaplikasi = 'report_amalia';
											 $optsDataHrmista = array('http' =>
													 array(
															 'method' => 'POST',
															 'header' => 'Content-type: application/x-www-form-urlencoded',
															 'content' => 'nik=' . $nik . '&aplikasi=' . $namaaplikasi
													 )
											 );

											 $contextDataHrmista = stream_context_create($optsDataHrmista);
											 $urlapi = Alamatapi::model()->findByAttributes(array('menu'=>'inserthrmista'));
											 $json = file_get_contents($urlapi->url_api, false, $contextDataHrmista);
											 $this->redirect(array('site/login'));
											 //end

									 }
							 }
							 catch(Exception $e){
									 $transaction->rollback();
									 echo '<center><h4><b>Update Gagal!<b></h4></center>';
									 echo '<center><h4>Silakan Coba Update Kembali</h4></center>';
							}
					 }
			 }
			 $this->render('updatedata', array('nik'=>$nik));
	 }

	 public function actionCekabsensi(){
		 $check = Pegawai::model()->checkAbsensi('865809');
		 if($check == 'n'){
			 echo "tidak absen";
		 }else{
			 echo "absen";
		 }
		 // echo $check;
	 }

	 public function actionGetstockteknisi(){
		 $idbarang = $_POST['id_barang'];
		 $nik = $_POST['nik'];

		 $getdatas = Reservation::model()->getStokUsagePemakai($idbarang, $nik);
		 echo 'Stock :'.$getdatas[0].'</br>';
		 echo 'Harga : '.$getdatas[2]. '</br>';
		 echo 'ON REQUEST: '.$getdatas[4].'</br>';
		 echo 'GI Alista Lama: '.$getdatas[9].'</br>';
		 echo 'RETURN GI ALISTA LAMA: '.$getdatas[10].'</br>';
		 echo 'GI SAP: '.$getdatas[5].'</br>';
		 echo 'RETURN GI SAP: '.$getdatas[12].'</br>';
		 echo 'PENGEMBALIAN IN PROGRESS: '.$getdatas[11].'</br>';
		 echo 'TRANSFER: '.$getdatas[8].'</br>';
		 echo 'PEMAKAIAN: '.$getdatas[7].'</br>';
	 }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('site/login'));
	}
}
