<?php
class ReportController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
				),
			);
	}

	public function accessRules()
{
    return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions

                'actions' => array('BaRegional'),

                'users' => array('@'),

            ),

			array('deny',  // deny all users
						'users'=>array('*'),
					),

    );
}

public function actionCheckReg(){
	$model = new MasterSto;
	print_r( $model->getWitelNew("-","-","-"));
}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	// public function actionIndex()
	// {
	// 	// renders the view file 'protected/views/site/index.php'
	// 	// using the default layout 'protected/views/layouts/main.php'
	// 	$this->render('index');
	// }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionInsertReturn($id,$karena1,$karena2,$karena3,$karena_detil){
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
		$nik = Yii::app()->session['nik'];

		$status_approve = "";

		if($karena1 == "1" && $karena2 == "1" && $karena3 == "1"){
			$status_approve = "-7";
		} else if($karena3 == "1" && $karena2 == "1" ){
			$status_approve = "-6";
		} else if($karena3 == "1" && $karena1 == "1"){
			$status_approve = "-5";
		}else if($karena2 == "1" && $karena1 == "1"){
			$status_approve = "-4";
		} else if($karena3 == "1" ){
			$status_approve = "-3";
		} else if($karena2 == "1"){
			$status_approve = "-2";
		}else if($karena1 == "1"){
			$status_approve = "-1";
		}

		$return = new AmaliaReturn();
		$return->id_return1 = $karena1;
		$return->id_return2 = $karena2;
		$return->id_return3 = $karena3;
		$return->detil_return = $karena_detil;
		$return->returned_by = $nik;
		$return->no_wo =$id;
		$return->created_date =date('Y-m-d H:i:s');
		$return->save();

		$command = Yii::app()->db->createCommand($id);

        $command->update('pemakaian', array(
            'status_approve' => $status_approve,
                ), 'no_wo=:id', array(':id' => $id));

		// echo "<script type='text/javascript'>alert('Date Berhasil di Return')
		// window.location.href = 'index.php?r=report/showApprovalBa';
		// </script>";
		echo "<script type='text/javascript'>alert('Data Berhasil di Return');
		</script>";
	}

	public function actionInsertReturnAsoFoto($id,$return_foto,$com_rev){
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
		$nik = Yii::app()->session['nik'];
		//status 1 karena dikembalikan ke TL bukan teknisi
		$status_approve = "-1";

		$return = new AmaliaReturn();
		$return->id_return1 = '1';
		// $return->id_return2 = $karena2;
		// $return->id_return3 = $karena3;
		$return->detil_return = $com_rev;
		$return->returned_by = $nik;
		$return->no_wo =$id;
		$return->created_date =date('Y-m-d H:i:s');
		$return->foto_return = $return_foto;
	  $return->status = '1';
		$return->save();

		$command = Yii::app()->db->createCommand($id);

				$command->update('pemakaian', array(
						'status_approve' => $status_approve,
								), 'no_wo=:id', array(':id' => $id));

		echo "<script type='text/javascript'>alert('Foto berhasil di return ke TL');
		window.location.href = 'index.php?r=report/showApprovalBa';
		</script>";
	}

	public function actionInsertReturnTL($id,$return_foto,$com_rev){
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
		$nik = Yii::app()->session['nik'];


		$status_approve = "-10";

		$return = new AmaliaReturn();
		$return->id_return1 = '1';
		// $return->id_return2 = $karena2;
		// $return->id_return3 = $karena3;
		$return->detil_return = $com_rev;
		$return->returned_by = $nik;
		$return->no_wo =$id;
		$return->created_date =date('Y-m-d H:i:s');
		$return->foto_return = $return_foto;
		$return->save();

		$command = Yii::app()->db->createCommand($id);

				$command->update('pemakaian', array(
						'status_approve' => $status_approve,
								), 'no_wo=:id', array(':id' => $id));

		echo "<script type='text/javascript'>alert('Data Berhasil di Return');
		</script>";
	}

	public function actionInsertReturnTeknisi($id){
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
		$nik = Yii::app()->session['nik'];

		$status_approve = "-10";

		$command = Yii::app()->db->createCommand($id);
		$command->update('pemakaian', array(
						'status_approve' => $status_approve,
								), 'no_wo=:id', array(':id' => $id));

		echo "<script type='text/javascript'>alert('Data Berhasil di Return');
		</script>";
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
				"Reply-To: {$model->email}\r\n".
				"MIME-Version: 1.0\r\n".
				"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}


	/**
	* halaman utama aplikasi dashboard amalia
	**/

	// kendali
	public function actionRekapBaNew()
	{

		$model 									= new Pemakaian();
		$model2 								= new TPemakaian();
		$model_mapping				 	= new MappingApproval();
		$naker 									= new Naker();
		$master_sto   					= new MasterSto();
		$user   								= new User();
		$material_tambahan   		= new MaterialTambahan();

		$level 	= "";
		$witel 	= "";
		$lokasi = "";
		$date1 	= "";
		$date2 	= "";

		$row = $model_mapping->getData();

		if(isset($_GET['changed'])){
			$changed = $_GET['changed'];
		}else{
			$changed = "-";
		}

		if(isset($_GET['regional']) && $_GET['regional'] !="All"){
			$regional = $_GET['regional'];
		}else{
			$regional = "-";
		}

		if(isset($_GET['date1']) && $_GET['date1'] !=""){
			$date1 = $_GET['date1'];
		}else{
			$date1 = "-";
		}

		if(isset($_GET['date2']) && $_GET['date2'] !=""){
			$date2 = $_GET['date2'];
		}else{
			$date2 = "-";
		}

		if(isset($_GET['witel']) && $_GET['witel'] !="All"){
			$witel = $_GET['witel'];
		}else{
			$witel = "-";
		}

		if(isset($_GET['sto']) && $_GET['sto'] !="All"){
			$sto = $_GET['sto'];
		}else{
			$sto = "-";
		}

		// ganti
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
		$nik = Yii::app()->session['nik'];

		$s = $user->getLokasi($nik);
		foreach ($s as $a) {
			$lokasi = $a->lokasi;
		}

		// excel wo
		if (isset($_GET['excel']))
		{
			$content =$this->renderPartial("excel_rekap",
											array("model"			=> $model,
														"regional"	=> $regional,
														"witel"			=> $witel,
														"sto"				=> $sto,
														"date1"			=> $date1,
														"date2"			=> $date2),
											true);
			Yii::app()->request->sendFile("rekapBaDigital.xls",$content);
		}

		//excel material
		if (isset($_GET['material']))
		{

		$content = $this->renderPartial('view_material_new',array('model'			=> $model2,
																															'model2'		=> $material_tambahan,
																															"regional"	=> $regional,
																															"witel"			=> $witel,
																															"sto"				=> $sto,
																															"date1"			=> $date1,
																															"date2"			=> $date2),true);

			Yii::app()->request->sendFile("MaterialAlistaBaDigital.xls",$content);
		}

		// level 1 bisa all regional all witel all sto
		// level 2 bisa all witel all sto  - no regional
		// level 3 bisa all sto - no regional - no witel
		// level 4 - no sto - no witel - no regional

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 						= $_GET['Pemakaian']['sto'];
			 $model->witel 					= $_GET['Pemakaian']['witel'];
			 $model->regional 			= $_GET['Pemakaian']['regional'];
			 $model->nama_pelanggan = $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 			= $_GET['Pemakaian']['no_kontak'];
			 $model->no_wo 					= $_GET['Pemakaian']['no_wo'];
			 $model->jenis_layanan 	= $_GET['Pemakaian']['jenis_layanan'];
			 $model->approved_by 		= $_GET['Pemakaian']['approved_by'];
			 $model->date_approved 	= $_GET['Pemakaian']['date_approved'];
		}


		$level = $user->getLevel($nik);

		$tgl_m 				= date("-m-");
		$tgl_mulai 		= "2018".$tgl_m."01";
		$tgl_selesai 	= date("Y-m-d");
		$tipe_teknisi = "all_teknisi";

		$this->render('approved_ba_new',
						array(
								'model'			=> $model,
								'changed'		=> $changed,
								'sto'				=> $sto,
								'level'			=> $level,
								'naker'			=> $naker,
								'master_sto'=> $master_sto,
								'regional'	=> $regional,
								'witel'			=> $witel,
								'witel2' 		=> $witel,
								'date1'			=> $date1,
								'date2'			=> $date2,
						)
					);
	}

	// kendali
	public function actionPerformTl()
	{

		$model 									= new Pemakaian();
		$model2 								= new TPemakaian();
		$model_mapping 					= new MappingApproval();
		$naker 									= new Naker();
		$master_sto   					= new MasterSto();
		$user   								= new User();
		$material_tambahan   		= new MaterialTambahan();

		$level ="";
		$witel = "";
		$lokasi = "";
		$date1 = "";
		$date2 = "";

		$row = $model_mapping->getData();

		if(isset($_GET['changed'])){
			$changed = $_GET['changed'];
		}else{
			$changed = "-";
		}

		if(isset($_GET['regional']) && $_GET['regional'] !="All"){
			$regional = $_GET['regional'];
		}else{
			$regional = "-";
		}

		if(isset($_GET['date1']) && $_GET['date1'] !=""){
			$date1 = $_GET['date1'];
		}else{
			$date1 = "-";
		}

		if(isset($_GET['date2']) && $_GET['date2'] !=""){
			$date2 = $_GET['date2'];
		}else{
			$date2 = "-";
		}

		if(isset($_GET['witel']) && $_GET['witel'] !="All"){
			$witel = $_GET['witel'];
		}else{
			$witel = "-";
		}


		if(isset($_GET['sto']) && $_GET['sto'] !="All"){
			$sto = $_GET['sto'];
		}else{
			$sto = "-";
		}

		// ganti
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
		$nik = Yii::app()->session['nik'];

		$s = $user->getLokasi($nik);
		foreach ($s as $a) {
			$lokasi = $a->lokasi;
		}

		// level 1 bisa all regional all witel all sto
		// level 2 bisa all witel all sto  - no regional
		// level 3 bisa all sto - no regional - no witel
		// level 4 - no sto - no witel - no regional

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 				= $_GET['Pemakaian']['sto'];
			 $model->witel 			= $_GET['Pemakaian']['witel'];
			 $model->regional 	= $_GET['Pemakaian']['regional'];
			 $model->nik_tl 		= $_GET['Pemakaian']['nik_tl'];
			 $model->nama_tl		= $_GET['Pemakaian']['nama_tl'];
		}


		$level = $user->getLevel($nik);

		$tgl_m 					= date("-m-");
		$tgl_mulai 			= "2018".$tgl_m."01";
		$tgl_selesai 		= date("Y-m-d");
		$tipe_teknisi   = "all_teknisi";

		$this->render('performansi_tl',
						array(
								'model'			=>$model,
								'changed'		=>$changed,
								'sto'				=>$sto,
								'level'			=>$level,
								'naker'			=>$naker,
								'master_sto'=>$master_sto,
								'regional'	=>$regional,
								'witel'			=>$witel,
								'date1'			=>$date1,
								'date2'			=>$date2,
						)
					);
		// $this->render('approved_ba_new',array('model'=>$model,"tgl_mulai"=>$tgl_mulai,"tgl_selesai"=>$tgl_selesai,"tipe_teknisi"=>$tipe_teknisi));
	}


	public function actionDoUpdate(){
		  $no 			= Yii::app()->getRequest()->getPost("no");
		  $nama_tl 	= Yii::app()->getRequest()->getPost("nama_tl");
		  $nik_tl 	= Yii::app()->getRequest()->getPost("nik_tl");

		  $command = Yii::app()->db_api->createCommand($no);

        $command->update('master_tl', array(
            'nik_tl' 	=> $nik_tl,
            'nama_tl' => $nama_tl,
                ), 'no=:no', array(':no' => $no));

        echo "<script type='text/javascript'>alert('Data berhasil Terupdate')
				window.location.href = 'index.php?r=report/DaftarTl';
				</script>";
		exit();
	}

	public function actionDaftarTl(){
		$tl   		= new Tl();

		if(isset($_GET['Tl']))
		{
			 $tl->regional		= $_GET['Tl']['regional'];
			 $tl->witel 			= $_GET['Tl']['witel'];
			 $tl->WORKZONE 		= $_GET['Tl']['WORKZONE'];
			 $tl->nik_tl 			= $_GET['Tl']['nik_tl'];
			 $tl->nama_tl 		= $_GET['Tl']['nama_tl'];

		}

		$this->render('daftar_tl',
						array(
								'model'			=>$tl,
								'sto'				=>$sto,
								'level'			=>$level,
								'naker'			=>$naker,
								'changed'		=>$changed,
								'master_sto'=>$master_sto,
								'regional'	=>$regional,
								'witel'			=>$witel,
						)
					);
	}

	public function actionUpdateTl($no){
		$tl   		= new Tl();

		$this->render('update_tl',
						array(
								'model' => $tl,
								'no'    => $no
						)
					);
				}

	public function actionApprovedBa(){

		$model 					= new Pemakaian();
		$model2 				= new TPemakaian();
		$model_mapping 	= new MappingApproval();
		$naker 					= new Naker();
		$master_sto   	= new MasterSto();
		$user   				= new User();

		$level 	= "";
		$witel 	= "";
		$lokasi = "";

		if(isset($_GET['changed'])){
			$changed = $_GET['changed'];
		}else{
			$changed = "-";
		}

		if(isset($_GET['regional']) && $_GET['regional'] !="All"){
			$regional = $_GET['regional'];
		}else{
			$regional = "-";
		}

		if(isset($_GET['witel']) && $_GET['witel'] !="All"){
			$witel = $_GET['witel'];
		}else{
			$witel = "-";
		}

		if(isset($_GET['sto']) && $_GET['sto'] !="All"){
			$sto = $_GET['sto'];
		}else{
			$sto = "-";
		}

		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}

		$nik = Yii::app()->session['nik'];

		$s = $user->getLokasi($nik);
		foreach ($s as $a) {
			$lokasi = $a->lokasi;
		}

		// level 1 bisa all regional all witel all sto
		// level 2 bisa all witel all sto  - no regional
		// level 3 bisa all sto - no regional - no witel
		// level 4 - no sto - no witel - no regional

		$level = $user->getLevel($nik);

		$row = $model_mapping->getData();
		if (isset($_GET['excel']))
		{
			$content =$this->renderPartial("excel_sto",array(
				'model'			=>$model,
				'sto'				=>$sto,
				'level'			=>$level,
				'naker'			=>$naker,
				'changed'		=>$changed,
				'master_sto'=>$master_sto,
				'regional'	=>$regional,
				'witel'			=>$witel,
				'date1' 		=> 'Y'
			),true);
			Yii::app()->request->sendFile("dataExcel.xls",$content);
		}

		if (isset($_GET['excel_material']))
		{
			$content_material =$this->renderPartial("excel_material_sto",array("model2"=>$model2),true);
			Yii::app()->request->sendFile("dataExcelMaterial.xls",$content_material);
		}

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 							= $_GET['Pemakaian']['sto'];
			 $model->witel 						= $_GET['Pemakaian']['witel'];
			 $model->regional 				= $_GET['Pemakaian']['regional'];
			 $model->no_wo 						= $_GET['Pemakaian']['no_wo'];
			 $model->nama_pelanggan 	= $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 				= $_GET['Pemakaian']['no_kontak'];
			 $model->no_kontak_2 			= $_GET['Pemakaian']['no_kontak_2'];
			 $model->email_pelanggan 	= $_GET['Pemakaian']['email_pelanggan'];
			 $model->no_inet 					= $_GET['Pemakaian']['no_inet'];
			 $model->jenis_layanan 		= $_GET['Pemakaian']['jenis_layanan'];
			 $model->create_dtm		 		= $_GET['Pemakaian']['create_dtm'];
			 $model->approved_by 			= $_GET['Pemakaian']['approved_by'];
			 $model->date_approved 		= $_GET['Pemakaian']['date_approved'];
		}

		$this->render('approved_ba',
						array(
								'model'			=>$model,
								'sto'				=>$sto,
								'level'			=>$level,
								'naker'			=>$naker,
								'changed'		=>$changed,
								'master_sto'=>$master_sto,
								'regional'	=>$regional,
								'witel'			=>$witel,
						)
					);
	}

	public function actionInboxTl(){

		$model 			= new Pemakaian();
		$model2 		= new TPemakaian();
		$model_mapping 	= new MappingApproval();
		$level = "";

		$row = $model_mapping->getData();
		foreach ($row as $s) {
		   $i =($s->getAttributes());
		   $level =  $i['level'];
		}

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 				= $_GET['Pemakaian']['sto'];
			 $model->witel 				= $_GET['Pemakaian']['witel'];
			 $model->regional 			= $_GET['Pemakaian']['regional'];
			 $model->no_wo 				= $_GET['Pemakaian']['no_wo'];
			 $model->nama_pelanggan 	= $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 			= $_GET['Pemakaian']['no_kontak'];
			 $model->email_pelanggan 	= $_GET['Pemakaian']['email_pelanggan'];
			 $model->no_inet 			= $_GET['Pemakaian']['no_inet'];
		}


		$this->render('inbox_tl', array('model'=>$model,"type"=>2));
	}

	public function ActionInboxTlNeedValidate(){

		$model 					= new Pemakaian();
		$model2 				= new TPemakaian();
		$model_mapping 	= new MappingApproval();
		$level 					= "";

		$row = $model_mapping->getData();
		foreach ($row as $s) {
		   $i =($s->getAttributes());
		   $level =  $i['level'];
		}

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 							= $_GET['Pemakaian']['sto'];
			 $model->witel 						= $_GET['Pemakaian']['witel'];
			 $model->regional 				= $_GET['Pemakaian']['regional'];
			 $model->no_wo 						= $_GET['Pemakaian']['no_wo'];
			 $model->nama_pelanggan 	= $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 				= $_GET['Pemakaian']['no_kontak'];
			 $model->email_pelanggan 	= $_GET['Pemakaian']['email_pelanggan'];
			 $model->no_inet 					= $_GET['Pemakaian']['no_inet'];
		}

			$this->render('inbox_tl', array('model'=>$model,"type"=>1));
	}

	public function actionSwhoRegNew2(){
		$model 	= new Naker();
		$a = $model->getDataRegional();
		foreach ($a as $s) {
		   $i =($s->reg);
		   echo $i;
		}
		print_r($a);
		exit();
	}

	public function actionShowApprovalBa(){

		$model 					= new Pemakaian();
		$model2 				= new TPemakaian();
		$model_mapping 	= new MappingApproval();
		$naker 					= new Naker();
		$master_sto   	= new MasterSto();
		$user   				= new User();

		$level 	= "";
		$witel 	= "";
		$lokasi = "";

		if(isset($_GET['regional']) && $_GET['regional'] !="All"){
			$regional = $_GET['regional'];
		}else{
			$regional = "-";
		}

		if(isset($_GET['witel']) && $_GET['witel'] !="All"){
			$witel = $_GET['witel'];
		}else{
			$witel = "-";
		}

		if(isset($_GET['sto']) && $_GET['sto'] !="All"){
			$sto = $_GET['sto'];
		}else{
			$sto = "-";
		}

		if(isset($_GET['changed'])){
			$changed = $_GET['changed'];
		}else{
			$changed = "-";
		}

		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}

		$nik = Yii::app()->session['nik'];
		$s = $user->getLokasi($nik);
		foreach ($s as $a) {
			$lokasi = $a->lokasi;
		}

		// level 1 bisa all regional all witel all sto
		// level 2 bisa all witel all sto  - no regional
		// level 3 bisa all sto - no regional - no witel
		// level 4 - no sto - no witel - no regional

		$level = $user->getLevel($nik);

		$row = $model_mapping->getData();
		if (isset($_GET['excel']))
		{
			$content =$this->renderPartial("excel_sto",array("model"=>$model),true);
			Yii::app()->request->sendFile("dataExcel.xls",$content);
		}

		if (isset($_GET['excel_material']))
		{
			$content_material =$this->renderPartial("excel_material_sto",array("model2"=>$model2),true);
			Yii::app()->request->sendFile("dataExcelMaterial.xls",$content_material);
		}

		// filter

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 							= $_GET['Pemakaian']['sto'];
			 $model->witel 						= $_GET['Pemakaian']['witel'];
			 $model->regional 				= $_GET['Pemakaian']['regional'];
			 $model->nama_pelanggan 	= $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 				= $_GET['Pemakaian']['no_kontak'];
			 $model->no_wo 						= $_GET['Pemakaian']['no_wo'];
			 $model->email_pelanggan 	= $_GET['Pemakaian']['email_pelanggan'];
			//  $model->no_inet 			= $_GET['Pemakaian']['no_inet'];
			 // $model->jenis_layanan 		= $_GET['Pemakaian']['jenis_layanan'];
			 $model->create_dtm 		= $_GET['Pemakaian']['create_dtm'];
			 $model->date_approved 		= $_GET['Pemakaian']['date_approved'];
		}

		$this->render('approval_amalia',
						array(
								'model'=>$model,
								'sto'=>$sto,
								'level'=>$level,
								'naker'=>$naker,
								'master_sto'=>$master_sto,
								'regional'=>$regional,
								'witel'=>$witel,
								'changed'=>$changed,
						)
					);
	}

	public function actionShowReturn(){

		$model 					= new Pemakaian();
		$model2 				= new TPemakaian();
		$model_mapping 	= new MappingApproval();
		$naker 					= new Naker();
		$master_sto   	= new MasterSto();
		$user   				= new User();

		$level 	= "";
		$witel 	= "";
		$lokasi = "";

		if(isset($_GET['status'])){
			$status = $_GET['status'];
		}else{
			$status = "undefined";
		}

		if(isset($_GET['regional']) && $_GET['regional'] !="All"){
			$regional = $_GET['regional'];
		}else{
			$regional = "-";
		}

		if(isset($_GET['witel']) && $_GET['witel'] !="All"){
			$witel = $_GET['witel'];
		}else{
			$witel = "-";
		}


		if(isset($_GET['sto']) && $_GET['sto'] !="All"){
			$sto = $_GET['sto'];
		}else{
			$sto = "-";
		}

		if(isset($_GET['changed'])){
			$changed = $_GET['changed'];
		}else{
			$changed = "-";
		}

		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}

		$nik = Yii::app()->session['nik'];
		$s = $user->getLokasi($nik);
		foreach ($s as $a) {
			$lokasi = $a->lokasi;
		}

		// level 1 bisa all regional all witel all sto
		// level 2 bisa all witel all sto  - no regional
		// level 3 bisa all sto - no regional - no witel
		// level 4 - no sto - no witel - no regional

		$level = $user->getLevel($nik);

		$row = $model_mapping->getData();

		if (isset($_GET['excel']))
		{
			$content =$this->renderPartial("excel_return",array(
			'model'			=>$model,
			'sto'				=>$sto,
			'level'			=>$level,
			'naker'			=>$naker,
			'master_sto'=>$master_sto,
			'regional'	=>$regional,
			'witel'			=>$witel,
			'changed'		=>$changed,
			'status'=>$status),true);
			Yii::app()->request->sendFile("dataReturn.xls",$content);
		}

		// if (isset($_GET['excel_material']))
		// {
		// 	$content_material =$this->renderPartial("excel_material_sto",array("model2"=>$model2),true);
		// 	Yii::app()->request->sendFile("dataExcelMaterial.xls",$content_material);
		// }

		// filter

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 							= $_GET['Pemakaian']['sto'];
			 $model->witel 						= $_GET['Pemakaian']['witel'];
			 $model->regional 				= $_GET['Pemakaian']['regional'];
			 $model->nama_pelanggan 	= $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 				= $_GET['Pemakaian']['no_kontak'];
			 $model->no_wo 						= $_GET['Pemakaian']['no_wo'];
			 $model->email_pelanggan 	= $_GET['Pemakaian']['email_pelanggan'];
			 $model->no_inet 					= $_GET['Pemakaian']['no_inet'];
			 $model->jenis_layanan 		= $_GET['Pemakaian']['jenis_layanan'];
			 $model->create_dtm 			= $_GET['Pemakaian']['create_dtm'];
			 $model->date_approved 		= $_GET['Pemakaian']['date_approved'];
		}

		$this->render('listreturn',
						array(
								'model'			=>$model,
								'sto'				=>$sto,
								'level'			=>$level,
								'naker'			=>$naker,
								'master_sto'=>$master_sto,
								'regional'	=>$regional,
								'witel'			=>$witel,
								'changed'		=>$changed,
								'status'=>$status
						)
					);
	}

	public function actionShowAllRekap(){

		$model 					= new Pemakaian();
		$model2 				= new TPemakaian();
		$model_mapping 	= new MappingApproval();
		$naker 					= new Naker();
		$master_sto   	= new MasterSto();
		$user   				= new User();

		$level 	= "";
		$witel 	= "";
		$lokasi = "";

		if(isset($_GET['regional']) && $_GET['regional'] !="All"){
			$regional = $_GET['regional'];
		}else{
			$regional = "-";
		}

		if(isset($_GET['witel']) && $_GET['witel'] !="All"){
			$witel = $_GET['witel'];
		}else{
			$witel = "-";
		}


		if(isset($_GET['sto']) && $_GET['sto'] !="All"){
			$sto = $_GET['sto'];
		}else{
			$sto = "-";
		}

		if(isset($_GET['changed'])){
			$changed = $_GET['changed'];
		}else{
			$changed = "-";
		}

		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}

		$nik = Yii::app()->session['nik'];
		$s = $user->getLokasi($nik);
		foreach ($s as $a) {
			$lokasi = $a->lokasi;
		}

		// level 1 bisa all regional all witel all sto
		// level 2 bisa all witel all sto  - no regional
		// level 3 bisa all sto - no regional - no witel
		// level 4 - no sto - no witel - no regional

		$level = $user->getLevel($nik);

		$row = $model_mapping->getData();
		if (isset($_GET['excel']))
		{
			$content =$this->renderPartial("excel_sto",array("model"=>$model),true);
			Yii::app()->request->sendFile("dataExcel.xls",$content);
		}

		if (isset($_GET['excel_material']))
		{
			$content_material =$this->renderPartial("excel_material_sto",array("model2"=>$model2),true);
			Yii::app()->request->sendFile("dataExcelMaterial.xls",$content_material);
		}

		// filter

		if(isset($_GET['Pemakaian']))
		{
			 $model->sto 							= $_GET['Pemakaian']['sto'];
			 $model->witel 						= $_GET['Pemakaian']['witel'];
			 $model->regional 				= $_GET['Pemakaian']['regional'];
			 $model->nama_pelanggan 	= $_GET['Pemakaian']['nama_pelanggan'];
			 $model->no_kontak 				= $_GET['Pemakaian']['no_kontak'];
			 $model->no_wo 						= $_GET['Pemakaian']['no_wo'];
			 $model->email_pelanggan 	= $_GET['Pemakaian']['email_pelanggan'];
			//  $model->no_inet 			= $_GET['Pemakaian']['no_inet'];
			 $model->jenis_layanan 		= $_GET['Pemakaian']['jenis_layanan'];
			//  $model->create_dtm 		= $_GET['Pemakaian']['create_dtm'];
			 $model->date_approved 		= $_GET['Pemakaian']['date_approved'];
		}

		$this->render('listallba',
						array(
								'model'=>$model,
								'sto'=>$sto,
								'level'=>$level,
								'naker'=>$naker,
								'master_sto'=>$master_sto,
								'regional'=>$regional,
								'witel'=>$witel,
								'changed'=>$changed,
						)
					);
	}

	public function actionApproveBaUpdateToAso($id,$type){

		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}

		$nik =Yii::app()->session['nik'];
        $command = Yii::app()->db->createCommand($id);

        $command->update('pemakaian', array(
            'status_approve' => '1',
            'approved_by' => $nik,
						'approved_tl_by' => $nik,
                ), 'no_wo=:id', array(':id' => $id));

        // echo "<script type='text/javascript'>alert('Data berhasil masuk ke approval ASO')
				// window.location.href = 'index.php?r=report/inboxTlNeedValidate';
				// </script>";
				echo "<script type='text/javascript'>alert('Data berhasil masuk ke approval ASO');
				</script>";

	}

	public function actionApproveBaUpdate($id,$comment_aso,$check_aso){
		if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
			$this->redirect("index.php?r=site/login");
		}
			$nik =Yii::app()->session['nik'];

		if (isset($check_aso)) {
			if ($comment_aso == '') {
				$comment_aso = "ok";
			}

			if ($check_aso == '') {
				$check_aso= "N";
			}

			$command = Yii::app()->db->createCommand($id);
				$tgl 	 = date("Y-m-d h:i:s");

						$command->update('pemakaian', array(
								'status_approve'		 => '2',
								'date_approved'			 => $tgl,
								'approved_by'				 => $nik,
								'is_check_aso'			 => $check_aso,
								'comment_check_aso'	 => $comment_aso
										), 'no_wo=:id', array(':id' => $id));
		}else{
			$command = Yii::app()->db->createCommand($id);
			$tgl 	 	 = date("Y-m-d h:i:s");

					$command->update('pemakaian', array(
								'status_approve' 		 => '2',
								'date_approved' 		 => $tgl,
								'approved_by'				 => $nik,
								'is_check_aso'			 => $check_aso,
								'comment_check_aso'	 => $comment_aso
									), 'no_wo=:id', array(':id' => $id));

			// 		echo "<script type='text/javascript'>alert('Data berhasil Approve')
			// 						window.location.href = 'index.php?r=report/showApprovalBa';
			// </script>";
		}

	}


	public function actionPerubahanPerbaikan($id,$status){

        $command = Yii::app()->db->createCommand($id);

        $command->update('pemakaian', array(
            'status_approve' => $status,
                ), 'no_wo=:id', array(':id' => $id));

        if($status < 0){
        	$notif = "Terhapus";
        }else{
        	$notif = "Di ajukan untuk approve";
        }

        echo "<script type='text/javascript'>alert('Data berhasil ".$notif."')
		window.location.href = 'index.php?r=report/InboxTl';
		</script>";
	}


	public function actionDownloadfile($no_wo)
	{
    $this->renderPartial('downloadfile',array('no_wo'=>$no_wo));
  }

	public function actionViewmaterial($id)
	{
		$model 						 = new TPemakaian;
		$material_tambahan = new MaterialTambahan;
		$pemakaian = new Pemakaian;

		$this->renderPartial('view_material',
			array('model'=>$model,
				'material_tambahan' => $material_tambahan,
				"id"=>$id,"pemakaian"=>$pemakaian));
	}

	public function actionDatafoto($id)
	{
		$model 					 = new TPemakaian;
		$model_pemakaian = new Pemakaian;
		$layanan 				 = new LayananAmalia;
		$this->renderPartial('content_foto_attachment',
															array('pemakaian'	=> $model_pemakaian,
																		'model'			=> $model,
																		"id"				=> $id,
																		"layanan"		=> $layanan));
	}

	public function actionPerbaikanFoto($id){

		$model 				= new TPemakaian;
		$m_pemakaian 	= new Pemakaian;
		$this->render('perbaikan_foto',
												array(
													'm_pemakaian'	=> $m_pemakaian,
													'model'				=> $model,
													"id"					=> $id));

	}

	public function actionDataReturn($id){
		$model = new TPemakaian;
		$this->renderPartial('content_return',
															array('model'	=> $model,
																		"id"		=> $id));
	}


	public function actionFotoAttachment($id)
	{
		$model = new TPemakaian;
		$this->renderPartial('view_material',
													array('model'	=> $model,
																"id"		=> $id));
	}

	public function actionTableViewMaterial($id)
	{
		$model = new TPemakaian;
		$this->renderPartial('view_material',
													array('model'	=> $model,
																"id"		=> $id));
	}

	public function actionSearchWo(){

			$model 		 = new TPemakaian();
			$wo_number ='Isikan No WO';

			if(isset($_POST['wo_number']))
			{
				$wo_number = $_POST['wo_number'];
			}

			$this->render('search_wo',array('model'=>$model,"wo_number"=>$wo_number));
		}


	public function actionUkur($id){
		$optsDataUser = "";
		// echo $id;

		$optsDataUser = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => 'no_wo='.$id
				)
			);
		$contextDataUser    = stream_context_create($optsDataUser);
		$json   						= file_get_contents('http://localhost/ibooster/ibooster.php', false, $contextDataUser);
		$data       				= json_decode($json);
		// print_r($data);
		return $data;
	}

	public function actionCekSpek($id){
		$model 					= new Pemakaian();
		$layanan_amalia = new LayananAmalia();

		$data_on_off = $model->dataUkurApprove($id);
		$data_on_off = "1";

		$this->renderPartial('cek_spek',array('model'=>$model,'layanan_amalia'=>$layanan_amalia,"id"=>$id,"data_on_off"=>$data_on_off));
	}

	public function actionHome(){
		$this->render('home');
	}

	public function actionRekapTl(){

			$model 									= new Pemakaian();
			$model2 								= new TPemakaian();
			$model_mapping	 				= new MappingApproval();
			$naker 									= new Naker();
			$master_sto 	 				 	= new MasterSto();
			$user   								= new User();
			$material_tambahan   		= new MaterialTambahan();

			$level 	= "";
			$witel 	= "";
			$lokasi = "";
			$date1 	= "";
			$date2 	= "";

			$row = $model_mapping->getData();

			if(isset($_GET['changed'])){
				$changed = $_GET['changed'];
			}else{
				$changed = "-";
			}

			if(isset($_GET['regional']) && $_GET['regional'] !="All"){
				$regional = $_GET['regional'];
			}else{
				$regional = "-";
			}

			if(isset($_GET['date1']) && $_GET['date1'] !=""){
				$date1 = $_GET['date1'];
			}else{
				$date1 = "-";
			}

			if(isset($_GET['date2']) && $_GET['date2'] !=""){
				$date2 = $_GET['date2'];
			}else{
				$date2 = "-";
			}

			if(isset($_GET['witel']) && $_GET['witel'] !="All"){
				$witel = $_GET['witel'];
			}else{
				$witel = "-";
			}

			if(isset($_GET['sto']) && $_GET['sto'] !="All"){
				$sto = $_GET['sto'];
			}else{
				$sto = "-";
			}

			// ganti
			if(Yii::app()->session['nik'] == '' || Yii::app()->session['nik'] == null){
				$this->redirect("index.php?r=site/login");
			}

			$nik = Yii::app()->session['nik'];

			$s = $user->getLokasi($nik);
			foreach ($s as $a) {
				$lokasi = $a->lokasi;
			}

			$level = $user->getLevel($nik);

			$tgl_m 					= date("-m-");
			$tgl_mulai 			= "2018".$tgl_m."01";
			$tgl_selesai 		= date("Y-m-d");
			$tipe_teknisi   = "all_teknisi";

			$this->render('rekap_approve_by_tl',
							array(
									'model' 		=>$model,
									'changed'		=>$changed,
									'sto'				=>$sto,
									'level'			=>$level,
									'naker'			=>$naker,
									'master_sto'=>$master_sto,
									'regional'	=>$regional,
									'witel'			=>$witel,
									'date1'			=>$date1,
									'date2'			=>$date2,
							)
						);
		}

	public function actionCheckIsVoice(){
		$model = new LayananAmalia();
		if($model->isVoice("39")){
			echo "voice ada";
		}else{
			echo "voice ga ada";
		}
	}

	public function actionKomentarRetrun($id){
		$model = new Pemakaian();
		$this->renderPartial('komentar_return',array('model'=>$model,"id"=>$id));
	}

	public function actionIsTl(){
		$nik = "17770046";
		$tl = new Tl();
		 $f = $tl->isTl($nik);
		echo count($f);
	}

	public function actionDataUkurApprove($no_wo){
		$model_pemakaian 		= new Pemakaian();
		$layanan_amalia 		= new LayananAmalia();
		$internet_spek 			= true;
		$tv_spek 						= true;
		$voice_spek 				= true;
		$data 							= $model_pemakaian->dataUkurApprove($no_wo);
		$last_chanel 				= $model_pemakaian->dataUkurChanel($no_wo);
		$register 					= $model_pemakaian->dataUkurRegister($no_wo);
		$pemakaian 					= $model_pemakaian->dataPemakaianRow($no_wo);
		// $l_voice = $layanan_amalia->layanan("3");

		foreach($pemakaian as $p){
		    $psb 								= $p->psb;
		    $redaman 						= $p->redaman;
		    $redaman_ont 				= $p->redaman_ont;
		    $ukur 							= $p->ukur;
		    $keterangan_usage 	= $p->keterangan_usage;
		    $last_channel 			= $p->last_channel;
		    $registred 					= $p->register_voice;
		    $sn_ont							= $p->sn_ont;
		    $dsl 								= $p->dsl;
		    $internet 					= $p->internet;
		    $last_update_usage 	= $p->last_update_usage;
				$migrasi 						= $p->migrasi;
		}

		if($psb > 0){

		    $l_layanan = $layanan_amalia->layananPsb($psb);
		    if(count($l_layanan) > 0){
		        foreach($l_layanan as $l){
		            $n_psb = $l['is_psb'];
		            $n_voice= $l['is_voice'];
		            $n_internet= $l['is_internet'];
		            $n_useetv= $l['is_tv'];

		            $n_layanan = $l['deskripsi'];

		            if($n_psb == 1){
		                $layanan =  $n_layanan;
		                if($n_voice == "1"){
		                    $is_voice = true;
		                }

		                if($n_internet == "1"){
		                    $is_internet = true;
		                }

		                if($n_useetv == "1"){
		                    $is_useetv = true;
		                }

		            }
		        }
		    }else{

		        if($psb == 1){
		            if($internet == 1){
		                $is_internet = true;
		            }

		            if($dsl == 1){
		                $is_useetv = true;
		            }

		            if($sn_ont != ""){
		                $is_voice = true;
		            }

		            $layanan = "1P";
		        }else{

		        }
		    }

		}else{
		    //if($layanan_amalia->layananMigrasi($migrasi)){
				$l_layanan = $layanan_amalia->layananMigrasi($migrasi);
		        foreach($l_layanan as $l){
		            $n_migrasi = $l['is_migrasi'];
		            $n_voice= $l['is_voice'];
		            $n_internet= $l['is_internet'];
		            $n_useetv= $l['is_tv'];

		            $n_layanan = $l['deskripsi'];

		            if($n_migrasi == 1){
		                $layanan =  $n_layanan;
		                if($n_voice == "1"){
		                    $is_voice = true;
		                }

		                if($n_internet == "1"){
		                    $is_internet = true;
		                }

		                if($n_useetv == "1"){
		                    $is_useetv = true;
		                }

		            }
		        }
		    //}
		}

		if($ukur == "1"){
			//skip
		}else if($ukur == "2"){
		    $internet_spek = false;
		}else{
		    $internet_spek = false;
		    }

		if($is_voice){
				if($registred == "NOT REGISTERED"){
						$voice_spek = false;
				    }else{
							//skip
				    }
				}

 		if($is_useetv){
				if($last_channel == "-"){
				     $tv_spek = false;
				    }else{
							//skip
				    }
				}

		echo json_encode(array("ukur"						=> $data,
													 "last_channel" 	=> $last_chanel,
													 "register"				=> $register,
													 "internet_spek" 	=> $internet_spek,
													 "tv_spek" 				=> $tv_spek,
													 "voice_spek" 		=> $voice_spek
												 		));
		//echo json_encode(array("ukur"=>$data));
	}


	public function actionDataInboxTl($nik){
		$model_pemakaian =  new Pemakaian();
		$data 					 = $model_pemakaian->dataInboxTl($nik);
		echo $data;
	}

	public function actiondataInboxAso($nik){
		$model_pemakaian =  new Pemakaian();
		$data 				   = $model_pemakaian->dataInboxAso($nik);
		echo $data;
	}


	/**
	* page average rating
	**/

	public function actionRating(){
		$m_pemakaian = new pemakaian();
		$m_master_employee = new master_employee();
		//return $this->render('average_rating',['m_pemakaian'=>$m_pemakaian,"master_employee"=>$m_master_employee]);
		return $this->render('average_rating',array('m_pemakaian'=>$m_pemakaian,"master_employee"=>$m_master_employee));
	}

	public function actionRevisiSpek($id){
		$status_approve = '1';
		$command = Yii::app()->db->createCommand($id);

				$command->update('pemakaian', array(
						'status_approve' => $status_approve,
								), 'no_wo=:id', array(':id' => $id));

				// $command->update('amalia_return', array(
				// 						'status' => '2',
				// 						//'fix_by' =>
				// 								), 'no_wo=:id', array(':id' => $id));

		echo "<script type='text/javascript'>alert('Data Berhasil di kembalikan ke ASO');
		window.location.href = 'index.php?r=report/inboxTl';
		</script>";

	}

}
