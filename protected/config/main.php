<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Reservation',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1234',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		// 'db'=>array(
		// 	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		// ),
		// uncomment the following to use a MySQL database
		'db' => array(
				'connectionString' => 'mysql:host=10.204.200.8;dbname=alista_v3',
				'emulatePrepare' => true,
				'username' => 'QAAlista',
				'password' => 'AlistaXlimitation2008%7',
				'charset' => 'utf8',
				'class' => 'CDbConnection'
		),
		'db_naker'=>array(
			'connectionString' => 'mysql:host=10.204.200.8;dbname=naker',
			'emulatePrepare' => true,
			'username' => 'QAAlista',
			'password' => 'AlistaXlimitation2008%7',
			'charset' => 'utf8',
			 'class' => 'CDbConnection',
		),
		'db_api'=>array(
			'connectionString' => 'mysql:host=10.204.200.8;dbname=api',
			'emulatePrepare' => true,
			'username' => 'QA_api',
			'password' => 'Xchallenge911*',
			'charset' => 'utf8',
			'class' => 'CDbConnection',
		),
		// 'dbproject' => array(
    //     'connectionString' => 'mysql:host=10.204.200.8;dbname=proactive',
    //     'emulatePrepare' => true,
		// 		'username' => 'QAAlista',
		// 		'password' => 'AlistaXlimitation2008%7',
    //     'charset' => 'utf8',
    //     'class' => 'CDbConnection'
    // ),
		// 'dbnewproactive' => array(
    //     'connectionString' => 'mysql:host=10.204.200.8;dbname=proactive2',
    //     'emulatePrepare' => true,
		// 		'username' => 'QAAlista',
		// 		'password' => 'AlistaXlimitation2008%7',
    //     'charset' => 'utf8',
    //     'class' => 'CDbConnection'
    // ),
		// 'dbvendor' => array(
    //     'connectionString' => 'mysql:host=10.204.200.8;dbname=myta',
    //     'emulatePrepare' => true,
		// 		'username' => 'QAAlista',
		// 		'password' => 'AlistaXlimitation2008%7',
    //     'charset' => 'utf8',
    //     'class' => 'CDbConnection'
    // ),
		// 'dbvendor' => array(
    //     'connectionString' => 'mysql:host=10.204.200.8;dbname=myta',
    //     'emulatePrepare' => true,
		// 		'username' => 'QAAlista',
		// 		'password' => 'AlistaXlimitation2008%7',
    //     'charset' => 'utf8',
    //     'class' => 'CDbConnection'
    // ),
		// 'dbtraining' => array(
    //     'connectionString' => 'mysql:host=10.204.200.8;dbname=training_prod',
    //     'emulatePrepare' => true,
		// 		'username' => 'cort',
		// 		'password' => 'P4yungt3duh%',
    //     'charset' => 'utf8',
    //     'class' => 'CDbConnection'
    // ),
		//koneksi production

		// 'db' => array(
				// 'connectionString' => 'mysql:host=10.204.200.2;dbname=alista_v3',
				// 'emulatePrepare' => true,
				// 'username' => 'AppAlista',
				// 'password' => 'Appalista2016%1',
				// 'charset' => 'utf8',
				// 'class' => 'CDbConnection'
		// ),
		// 'dbnewproactive' => array(
        // 'connectionString' => 'mysql:host=10.204.200.2;dbname=proactive2',
        // 'emulatePrepare' => true,
				// 'username' => 'AppAlista',
				// 'password' => 'Appalista2016%1',
        // 'charset' => 'utf8',
        // 'class' => 'CDbConnection'
    // ),
		// 'dbproject' => array(
        // 'connectionString' => 'mysql:host=10.204.200.2;dbname=proactive',
        // 'emulatePrepare' => true,
				// 'username' => 'AppAlista',
				// 'password' => 'Appalista2016%1',
        // 'charset' => 'utf8',
        // 'class' => 'CDbConnection'
    // ),
		// 'dbvendor' => array(
        // 'connectionString' => 'mysql:host=10.204.200.2;dbname=myta',
        // 'emulatePrepare' => true,
				// 'username' => 'AppAlista',
				// 'password' => 'Appalista2016%1',
        // 'charset' => 'utf8',
        // 'class' => 'CDbConnection'
    // ),

		//end

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'authManager' => array(
				'class' => 'CDbAuthManager',
				'connectionID' => 'db',
				'defaultRoles' => array('authenticated', 'guest'),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
