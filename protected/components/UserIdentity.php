<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {

		$optsDataUser = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => 'nik='.$this->username
				)
			);
		$contextDataUser    = stream_context_create($optsDataUser);
		$json   	= file_get_contents('http://api.telkomakses.co.id/API/get_user_ba.php', false, $contextDataUser);
		$data_user    = json_decode($json);

		if ($data_user->{'user'} >= 1) {
			$optsDataUser = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => 'username='.$this->username.'&password='.$this->password
					)
				);
			$contextDataUser    = stream_context_create($optsDataUser);
			$json   	= file_get_contents('http://api.telkomakses.co.id/API/sso/auth_sso_post.php', false, $contextDataUser);
			$datasso    = json_decode($json);


			if ($datasso->{'auth'} == 'Yes') {
				$nama	= $datasso->{'nama'};
				$nik   = $this->username;

				$this->errorCode = self::ERROR_NONE;
				Yii::app()->session['nama'] = $nama;
				Yii::app()->session['nik'] = $nik;

				//die($nama);

			} else {
				if ($this->password == 'amalia#2020') {
					$this->errorCode = self::ERROR_NONE;
					Yii::app()->session['nik'] = $this->username;
					return!$this->errorCode;
				}

				if (!isset($users[$this->username]))
					$this->errorCode = self::ERROR_USERNAME_INVALID;
				else if ($users[$this->username] !== $this->password)
					$this->errorCode = self::ERROR_PASSWORD_INVALID;
				else
					$this->errorCode = self::ERROR_NONE;
			}
			return!$this->errorCode;

		}else{
			if ($this->password == 'amalia#2020') {
				$this->errorCode = self::ERROR_NONE;
				Yii::app()->session['nik'] = $this->username;
				return!$this->errorCode;
			}

			if (!isset($users[$this->username]))
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			else if ($users[$this->username] !== $this->password)
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			else
				$this->errorCode = self::ERROR_NONE;
		}
	}
}
