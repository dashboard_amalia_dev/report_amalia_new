<?php

/**
 * This is the model class for table "user_ba".
 *
 * The followings are the available columns in table 'user_ba':
 * @property integer $no
 * @property integer $nik
 * @property string $nama
 * @property string $jabatan
 * @property integer $type_user
 * @property string $lokasi
 */
class UserBa extends CActiveRecord
{
	  public $nama, $password, $status, $fileuar, $employee_name;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_ba';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no', 'required'),
			array('no, nik, type_user', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>250),
			array('jabatan, lokasi', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no, nik, nama, jabatan, type_user, lokasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no' => 'No',
			'nik' => 'Nik',
			'nama' => 'Nama',
			'jabatan' => 'Jabatan',
			'type_user' => 'Type User',
			'lokasi' => 'Lokasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no',$this->no);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('type_user',$this->type_user);
		$criteria->compare('lokasi',$this->lokasi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_api;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserBa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
