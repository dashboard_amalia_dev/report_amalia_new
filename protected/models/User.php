<?php

/**
 * This is the model class for table "master_sto".
 *
 * The followings are the available columns in table 'master_sto':
 * @property integer $nik
 * @property string $reg
 * @property string $witel
 * @property string $lokasi
 * @property integer $level
 */
class User extends CActiveRecord
{
    // public $lokasi;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_ba';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik' => 'Nik',
			'reg' => 'Regional',
			'witel' => 'Witel',
			'sto' => 'Sto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('reg',$this->reg,true);
		$criteria->compare('witel',$this->witel,true);
		$criteria->compare('sto',$this->sto,true);
		$criteria->compare('level',$this->level,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));	
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_api;
	}

	public function getLokasi($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik;
		$data = $this->findAll($criteria);
		return $data;
	}

	public function IsAso($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik.' and t.jabatan = "aso"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function checkUser($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik ='.$nik.' and t.jabatan = "Tl"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function UserTa($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik;
		$data = $this->findAll($criteria);
		return $data;
	}

	public function IsHo($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik.' and t.lokasi = "ho"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function IsAll($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik.' and t.jabatan = "all" ';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getDataUserOneRow($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik;
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getLevel($nik){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.nik = '.$nik;
		$data = $this->findAll($criteria);
		$level_out = 5;
		$model = new MasterSto;
		foreach ($data as $d) {
			// regional
			$lokasi = $d->lokasi;
			if($lokasi != "all"){
				//regional
				$reg = $model->getDataRegionalOneRow($lokasi);
				$witel = $model->getDataWitelOneRow($lokasi);
				$sto = $model->getDataStoOneRow($lokasi);
				if(count($reg) > 0){
					$level = 2;
				}else if(count($witel) > 0){
					$level = 3;
				}else{
					$level = 4;
				}
			}else{
				$level = 1;
			}

			if($level < $level_out){
				$level_out = $level;
			}
		}
		return $level_out;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterSto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
