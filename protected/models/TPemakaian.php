<?php

/**
 * This is the model class for table "t_pemakaian".
 *
 * The followings are the available columns in table 't_pemakaian':
 * @property integer $pemakaian_id
 * @property string $nik_pemakai
 * @property string $pemakaian_date
 * @property string $wo_number
 * @property string $file_ba_instalasi
 * @property integer $regional
 * @property string $witel
 * @property string $jns
 * @property string $isactive
 */
class TPemakaian extends CActiveRecord
{
	public $nama_barang,$jml_pemakaian,$no_wo,$nik,$id_barang,$name,$reg_tactical,$witel_tactical,$sto;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_pemakaian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik_pemakai, pemakaian_date, wo_number', 'required'),
			array('regional', 'numerical', 'integerOnly'=>true),
			array('nik_pemakai, wo_number', 'length', 'max'=>100),
			array('file_ba_instalasi', 'length', 'max'=>200),
			array('witel, jns', 'length', 'max'=>45),
			array('isactive', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pemakaian_id, nik_pemakai, pemakaian_date, wo_number, file_ba_instalasi, regional, witel, jns, isactive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pemakaian_id' => 'Pemakaian',
			'nik_pemakai' => 'Nik Pemakai',
			'pemakaian_date' => 'Pemakaian Date',
			'wo_number' => 'Wo Number',
			'file_ba_instalasi' => 'File Ba Instalasi',
			'regional' => 'Regional',
			'witel' => 'Witel',
			'jns' => 'Jns',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pemakaian_id',$this->pemakaian_id);
		$criteria->compare('nik_pemakai',$this->nik_pemakai,true);
		$criteria->compare('pemakaian_date',$this->pemakaian_date,true);
		$criteria->compare('wo_number',$this->wo_number,true);
		$criteria->compare('file_ba_instalasi',$this->file_ba_instalasi,true);
		$criteria->compare('regional',$this->regional);
		$criteria->compare('witel',$this->witel,true);
		$criteria->compare('jns',$this->jns,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TPemakaian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function viewMaterial($id){
		$id = "'$id'";
		$link1 = '<center>'.CHtml::image('images/search.png', 'material', array('onclick'=>'bukaPopUp('.$id.')','class'=>'modal_new','name'=>'material','width' => 30, 'style'=>'cursor:pointer','data-toggle'=>'modal','data-target'=>'#modal')).'</center>';
		//$link1 = '<small><center><button onclick="bukaPopUp('.$id.',)" type="button" name="material"   class="btn btn-warning btn-xs modal_new" data-toggle="modal" data-target="#modal"><i class="fa fa-pencil"></i></button></center></small>';
		return $link1;

	}

	public function viewMaterialApproved($id){
		$id = "'$id'";
		$link1 = '<center>'.CHtml::image('images/search.png', 'material', array('onclick'=>'bukaPopUp('.$id.')','name'=>'material','width' => 30, 'style'=>'cursor:pointer','data-toggle'=>'modal','data-target'=>'#modal')).'</center>';
		//$link1 = '<small><center><button onclick="bukaPopUp('.$id.')" type="button" name="material" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal"><i class="fonticon-classname"></i></button></center></small>';
		return $link1;

	}

	public function dataMaterials($id)
	{
			$criteria = new CDbCriteria();

			$criteria->select 	 = 't.pemakaian_id,t.wo_number,b.id_barang,b.nama_barang,d.jml_pemakaian';
			$criteria->join 		 = 'INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang';
			$criteria->condition ='t.wo_number ="'.$id.'" and t.isactive = "Y" ';

			$data = $this->findAll($criteria);
			return $data;

	}

	public function dataFoto($id)
	{
			// $pemakaian = Yii::app()->db->createCommand()
		 //    ->select('*')
		 //    ->from('pemakaian')
		 //    ->where('no_wo=:no_wo', array(':no_wo'=>$id))
		 //    ->queryRow();
			// return $pemakaian;

			$pemakaian = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('detil_meta_data_amalia')
		    ->where('no_wo=:no_wo', array(':no_wo'=>$id))
		    ->queryRow();
			return $pemakaian;
	}

	public function dataReturn($id)
	{
			$pemakaian = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('amalia_return')
			->where('no_wo=:no_wo and status = 1', array(':no_wo'=>$id))
		    ->queryRow();
			return $pemakaian;
	}



	public function excel_material_reg($reg,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{
		$criteria = new CDbCriteria();

		$criteria->select = 'p.no_wo,
												 p.nik,
												 b.nama_barang,
												 d.jml_pemakaian,b.id_barang';

		$criteria->join 	= 'INNER JOIN pemakaian p ON t.wo_number = p.no_wo
												 LEFT JOIN naker.area_ta ta4 ON p.regional = ta4.id
												 LEFT JOIN naker.area_ta ta3 ON p.fiberzone = ta3.id
												 LEFT JOIN naker.area_ta ta2 ON p.witel = ta2.id
												 INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id
												 INNER JOIN t_master_barang b on d.id_barang = b.id_barang ';

		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND ta4.id = "'.$reg.'"  and p.id_mitra != "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND ta4.id = "'.$reg.'" and p.id_mitra = "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		}else
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND ta4.id = "'.$reg.'" and t.isactive = "Y" and p.isactive = "Y"';
		}

		$criteria->order ='p.no_wo';

		$data = $this->findAll($criteria);
		return $data;
	}

	public function excel_material_fz($fz,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 'p.no_wo,
												 b.nama_barang,
												 p.nik,
												 d.jml_pemakaian,b.id_barang';

		$criteria->join 	= 'INNER JOIN pemakaian p ON t.wo_number = p.no_wo
												 LEFT JOIN naker.area_ta ta4 ON p.regional = ta4.id
												 LEFT JOIN naker.area_ta ta3 ON p.fiberzone = ta3.id
												 LEFT JOIN naker.area_ta ta2 ON p.witel = ta2.id
												 INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id
												 INNER JOIN t_master_barang b on d.id_barang = b.id_barang ';

	if($tipe_teknisi == 'mitra')
	{
		$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra != "Telkom Akses" AND p.fiberzone = "'.$fz.'" and t.isactive = "Y" and p.isactive = "Y"';
	}
	elseif($tipe_teknisi == 'telkom_akses')
	{
		$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra = "Telkom Akses" AND p.fiberzone = "'.$fz.'" and t.isactive = "Y" and p.isactive = "Y"';
	}
	else
	{
		$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'"  AND p.fiberzone = "'.$fz.'" and t.isactive = "Y" and p.isactive = "Y"';
	}

		$criteria->order ='p.no_wo';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function excel_material_witel($witel,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 'p.no_wo,
												 b.nama_barang,
												 p.nik,
												 d.jml_pemakaian,b.id_barang';

		$criteria->join 	= 'INNER JOIN pemakaian p ON t.wo_number = p.no_wo
												 LEFT JOIN naker.area_ta ta4 ON p.regional = ta4.id
												 LEFT JOIN naker.area_ta ta3 ON p.fiberzone = ta3.id
												 LEFT JOIN naker.area_ta ta2 ON p.witel = ta2.id
												 INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id
												 INNER JOIN t_master_barang b on d.id_barang = b.id_barang ';

		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra != "Telkom Akses"  AND p.witel ="'.$witel.'" and t.isactive = "Y" and p.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra = "Telkom Akses" AND p.witel ="'.$witel.'" and t.isactive = "Y" and p.isactive = "Y"';
		}
		else
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND p.witel ="'.$witel.'" and t.isactive = "Y" and p.isactive = "Y"';
		}

		$criteria->order ='p.no_wo';

		$data = $this->findAll($criteria);
		return $data;
	}

	public function excel_material_new($regional,$witel,$sto,$date1,$date2){
		$criteria = new CDbCriteria();
		$criteria->select = 'p.no_wo,
												 b.nama_barang,
												 p.nik,
												 p.reg_tactical,
												 p.witel_tactical,
												 p.sto,
												 d.jml_pemakaian,b.id_barang';

		$criteria->join 	= 'INNER JOIN pemakaian p ON t.wo_number = p.no_wo
							 INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id
							 INNER JOIN t_master_barang b on d.id_barang = b.id_barang ';

		$criteria->condition = ' t.isactive = "Y" and p.isactive = "Y" ';

		if($date1 != "-" && $date2 != "-" && $date1 != "" && $date2 != ""){
			$criteria->condition .= 'and date_format(p.create_dtm, "%Y-%m-%d") between "'.$date1.'" and "'.$date2.'"';
		}

		if($regional != "-" && $witel !="-" && $sto != "-" ){
			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.witel_tactical = "'.$witel.'" and p.sto = "'.$sto.'"';
		}else if($regional != "-" && $witel !="-"  ){
			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.witel_tactical = "'.$witel.'"';
		}else if($regional != "-"){
			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'"';
		}

		// $criteria->limit = 10;
		// $criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra != "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		$data = $this->findAll($criteria);
		return $data;

	}

	public function excel_material_sto($sto,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 'p.no_wo,
												 b.nama_barang,
												 p.nik,
												 d.jml_pemakaian,b.id_barang';

		$criteria->join 	= 'INNER JOIN pemakaian p ON t.wo_number = p.no_wo
												 LEFT JOIN naker.area_ta ta4 ON p.regional = ta4.id
												 LEFT JOIN naker.area_ta ta3 ON p.fiberzone = ta3.id
												 LEFT JOIN naker.area_ta ta2 ON p.witel = ta2.id
												 INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id
												 INNER JOIN t_master_barang b on d.id_barang = b.id_barang ';

		if($tipe_teknisi == 'mitra' && $sto ='all_sto')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra != "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'p.sto = "'.$sto.'" AND  date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra != "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		}

		if($tipe_teknisi == 'telkom_akses' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra = "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'p.sto = "'.$sto.'"  AND  date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and p.id_mitra = "Telkom Akses" and t.isactive = "Y" and p.isactive = "Y"';
		}

		if($tipe_teknisi == 'all_teknisi' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" and p.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'all_teknisi')
		{
			$criteria->condition = 'p.sto = "'.$sto.'" AND  date_format(p.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" and p.isactive = "Y"';
		}

		$criteria->order ='p.no_wo';
		$data = $this->findAll($criteria);
		return $data;

	}

	public function detailWo($wo_number){
			$criteria = new CDbCriteria();

			$criteria->select 	 = 't.pemakaian_id,t.wo_number,b.id_barang,b.nama_barang,d.jml_pemakaian,t.pemakaian_date,t.nik_pemakai,coalesce(me.name,mi.name)as name';
			$criteria->join 		 = 'INNER JOIN t_pemakaian_detail d on t.pemakaian_id = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang
													 		LEFT JOIN naker.master_employee me on t.nik_pemakai = me.nik
													 		LEFT JOIN naker.master_employee_mitra mi on t.nik_pemakai = mi.nik ';
			$criteria->condition ='t.wo_number ="'.$wo_number.'" and t.isactive = "Y" ';

			$data = $this->findAll($criteria);
			return $data;

	}


}
