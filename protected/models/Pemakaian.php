<?php


/**
 * This is the model class for table "pemakaian".
 *
 * The followings are the available columns in table 'pemakaian':
 * @property integer $no
 * @property string $id_mitra
 * @property string $no_wo
 * @property string $no_permintaan
 * @property string $no_telp
 * @property string $no_ine
 * @property string $start_date
 * @property string $end_date
 * @property string $nama_pelanggan
 * @property string $datek_hk_msan_odc
 * @property string $datek_dp_odp
 * @property string $datek_klem_primer_feeder
 * @property string $klem_sec_distribusi
 * @property string $new_material
 * @property integer $test_voice
 * @property integer $test_internet
 * @property integer $test_use_tv
 * @property integer $test_ping
 * @property integer $test_upload
 * @property integer $test_download
 * @property integer $hasil_ukur_pedalaman
 * @property integer $catatan_khusus
 * @property string $other_catatan_khusus
 * @property string $modem_pasang
 * @property string $sn_ont
 * @property string $sn_modem
 * @property integer $power
 * @property integer $dsl
 * @property integer $internet
 * @property string $mac_address_stb
 * @property string $nama
 * @property string $notel_teknisi
 * @property integer $psb
 * @property integer $migrasi
 * @property integer $speed
 * @property string $other_speed
 * @property string $kendala
 * @property string $alasan_decline
 * @property string $other_alasan_decline_1
 * @property string $other_alasan_decline_2
 * @property string $url_ttd_pelanggan
 * @property string $url_ttd_mitra
 * @property string $jumlah_klem_ring
 * @property string $panjang_pvc
 * @property string $jumlah_breket
 * @property string $panjang_drop_core
 * @property string $drop_core
 * @property string $meter_awal
 * @property string $meter_akhir
 * @property string $panjang_tray_cable
 * @property string $jumlah_tiang_tlpn
 * @property string $create_dtm
 * @property string $panjang_utp
 * @property string $nik
 * @property string $witel
 * @property string $fiberzone
 * @property string $regional
 * @property string $witel_tactical
 * @property string $reg_tactical
 * @property string $level
 * @property string $jenis_layanan
 * @property string $approved_by
 * @property string $is_check_aso
 * @property string $comment_check_aso
 * @property string $filter_reg
 * @property string $approved_tl_by
 * @property string $id_valins
 */
class Pemakaian extends CActiveRecord
{

	public $nama_regional,
	$jml_ps,
	$satu_p,
	$dua_p,
	$tiga_p,
	$stb_tambahan,
	$change_stb,
	$infrastruktur,
	$migrasi_satu,
	$migrasi_dua,
	$migrasi_tiga,
	$migrasi_empat,
	$migrasi_lima,
	$migrasi_enam,
	$migrasi_tujuh,
	$migrasi_delapan,
	$migrasi_sembilan,
	$nama_fiberzone,
	$nama_witel,
	$jenis_layanan,
	$ukur,
	$ukur_usage,
	$tgl_mulai,
	$tgl_selesai,
	$tipe_teknisi,
	$fzt,
	$witell,
	$stoo,
	$nama_barang,
	$jml_pemakaian,
	$id_barang,
	$sn_ont,
	$dsl,
	$approved_by,
	$approved_tl_by,
	$date_approved,

	$need_approve,
	$approved,
	$kurang_foto,
	$kurang_ba,
	$unspek,
	$foto_ba,
	$foto_unspek,
	$ba_unspek,
	$foto_ba_unspek,

	$anomali1,
	$anomali2,
	$reg_tactical,
	$reg,
	$witel_versi_tactical,
	$witel_tactical,
	$detil_return,
	$komentar,
	$id_return1,
	$id_return2,
	$id_return3,
	$date_finish_update,
	$created_date,
	$need_validate,
	$return_inbox,
	$fix_komentar,
	$fix_date,
	$internet,
	$alamat_pelanggan,
	$email_pelanggan,
	$datek_hk_msan_odc,
	$datek_dp_odp,
	$status_approve,
	$sn_modem,
	$kendala,
	$sn_plc,
	$stos,
	$sn_wifi,
	$keterangan_usage,
	$average,
	$name,
	$nik,
	$level,
	$is_check_aso,
	$comment_check_aso,

	$nik_tl,
	$nama_tl,
	$performansi,
	$jarak
	;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pemakaian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tgl_mulai,tgl_selesai', 'required'),
			array('test_voice, test_internet, test_use_tv, test_ping, test_upload, test_download, hasil_ukur_pedalaman, catatan_khusus, power, dsl, internet, psb, migrasi, speed', 'numerical', 'integerOnly'=>true),
			array('id_valins,id_mitra, no_wo, sto, no_permintaan, nama_pelanggan, datek_hk_msan_odc, datek_dp_odp, datek_klem_primer_feeder, klem_sec_distribusi, other_catatan_khusus, modem_pasang, sn_ont, sn_modem, mac_address_stb, nama, notel_teknisi, other_speed, kendala, alasan_decline, other_alasan_decline_1, other_alasan_decline_2, url_ttd_pelanggan, url_ttd_mitra, jumlah_klem_ring, panjang_pvc, jumlah_breket, panjang_drop_core, drop_core, meter_awal, meter_akhir, panjang_tray_cable, jumlah_tiang_tlpn, panjang_utp, nik', 'length', 'max'=>100),
			array('no_telp, no_inet', 'length', 'max'=>15),
			array('witel', 'length', 'max'=>20),
			array('fiberzone, regional,jarak', 'length', 'max'=>50),
			array('start_date, end_date, new_material, create_dtm', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no, id_mitra, no_wo, sto, no_permintaan, no_telp, no_inet, start_date, end_date, nama_pelanggan, datek_hk_msan_odc, datek_dp_odp, datek_klem_primer_feeder, klem_sec_distribusi, new_material, test_voice, test_internet, test_use_tv, test_ping, test_upload, test_download, hasil_ukur_pedalaman, catatan_khusus, other_catatan_khusus, modem_pasang, sn_ont, sn_modem, power, dsl, internet, mac_address_stb, nama, notel_teknisi, psb, migrasi, speed, other_speed, kendala, alasan_decline, other_alasan_decline_1, other_alasan_decline_2, url_ttd_pelanggan, url_ttd_mitra, jumlah_klem_ring, panjang_pvc, jumlah_breket, panjang_drop_core, drop_core, meter_awal, meter_akhir, panjang_tray_cable, jumlah_tiang_tlpn, create_dtm, panjang_utp, nik, witel, fiberzone, regional, id_valins', 'safe', 'on'=>'search'),
			);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no' => 'No',
			'id_mitra' => 'Nama Mitra',
			'no_wo' => 'No Wo',
			'sto' => 'STO',
			'no_permintaan' => 'No Permintaan',
			'no_telp' => 'No Telfon Layanan',
			'no_inet' => 'No Inet',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'nama_pelanggan' => 'Nama Pelanggan',
			'datek_hk_msan_odc' => 'Datek Hk Msan Odc',
			'datek_dp_odp' => 'Datek Dp Odp',
			'datek_klem_primer_feeder' => 'Datek Klem Primer Feeder',
			'klem_sec_distribusi' => 'Klem Sec Distribusi',
			'new_material' => 'New Material',
			'test_voice' => 'Test Voice',
			'test_internet' => 'Test Internet',
			'test_use_tv' => 'Test Use Tv',
			'test_ping' => 'Test Ping',
			'test_upload' => 'Test Upload',
			'test_download' => 'Test Download',
			'hasil_ukur_pedalaman' => 'Hasil Ukur Pedalaman',
			'catatan_khusus' => 'Catatan Khusus',
			'other_catatan_khusus' => 'Other Catatan Khusus',
			'modem_pasang' => 'Modem Pasang',
			'sn_ont' => 'Sn Ont',
			'sn_modem' => 'Sn Modem',
			'power' => 'Power',
			'dsl' => 'Dsl',
			'internet' => 'Internet',
			'mac_address_stb' => 'Mac Address Stb',
			'nama' => 'Nama',
			'notel_teknisi' => 'Notel Teknisi',
			'psb' => 'Psb',
			'migrasi' => 'Migrasi',
			'speed' => 'Speed',
			'other_speed' => 'Other Speed',
			'kendala' => 'Kendala',
			'alasan_decline' => 'Alasan Decline',
			'other_alasan_decline_1' => 'Other Alasan Decline 1',
			'other_alasan_decline_2' => 'Other Alasan Decline 2',
			'url_ttd_pelanggan' => 'Url Ttd Pelanggan',
			'url_ttd_mitra' => 'Url Ttd Mitra',
			'jumlah_klem_ring' => 'Jumlah Klem Ring',
			'panjang_pvc' => 'Panjang Pvc',
			'jumlah_breket' => 'Jumlah Breket',
			'panjang_drop_core' => 'Panjang Drop Core',
			'drop_core' => 'Drop Core',
			'meter_awal' => 'Meter Awal',
			'meter_akhir' => 'Meter Akhir',
			'panjang_tray_cable' => 'Panjang Tray Cable',
			'jumlah_tiang_tlpn' => 'Jumlah Tiang Tlpn',
			'create_dtm' => 'Tgl Create BA',
			'panjang_utp' => 'Panjang Utp',
			'nik' => 'Nik Teknisi',
			'witel' => 'Witel',
			'fiberzone' => 'Fiberzone',
			'regional' => 'Regional',
			'no_kontak' => 'No Telp Pelanggan 1',
			'no_kontak_2' => 'No Telp Pelanggan 2',
			'reg_tactical' => 'regional',
			'witel_tactical' => 'witel',
			'need_validate' => 'Need Val',
			'is_check_aso' =>'Checlist ASO',
			'comment_check_aso' => 'Comment ASO',
			'filter_reg'=>'Filter Reg',
			'nik_tl'=> 'NIK TL',
			'nama_tl' => 'Nama TL',
			'approved_tl_by'=> 'Approved TL',
			'performansi'=>'Performansi [%]',
			'id_valins' => 'ID Valins',
			'jarak' => 'Jarak'
			);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no',$this->no);
		$criteria->compare('id_mitra',$this->id_mitra,true);
		$criteria->compare('no_wo',$this->no_wo,true);
		$criteria->compare('sto',$this->sto,true);
		$criteria->compare('no_permintaan',$this->no_permintaan,true);
		$criteria->compare('no_telp',$this->no_telp,true);
		$criteria->compare('no_inet',$this->no_inet,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('datek_hk_msan_odc',$this->datek_hk_msan_odc,true);
		$criteria->compare('datek_dp_odp',$this->datek_dp_odp,true);
		$criteria->compare('datek_klem_primer_feeder',$this->datek_klem_primer_feeder,true);
		$criteria->compare('klem_sec_distribusi',$this->klem_sec_distribusi,true);
		$criteria->compare('new_material',$this->new_material,true);
		$criteria->compare('test_voice',$this->test_voice);
		$criteria->compare('test_internet',$this->test_internet);
		$criteria->compare('test_use_tv',$this->test_use_tv);
		$criteria->compare('test_ping',$this->test_ping);
		$criteria->compare('test_upload',$this->test_upload);
		$criteria->compare('test_download',$this->test_download);
		$criteria->compare('hasil_ukur_pedalaman',$this->hasil_ukur_pedalaman);
		$criteria->compare('catatan_khusus',$this->catatan_khusus);
		$criteria->compare('other_catatan_khusus',$this->other_catatan_khusus,true);
		$criteria->compare('modem_pasang',$this->modem_pasang,true);
		$criteria->compare('sn_ont',$this->sn_ont,true);
		$criteria->compare('sn_modem',$this->sn_modem,true);
		$criteria->compare('power',$this->power);
		$criteria->compare('dsl',$this->dsl);
		$criteria->compare('internet',$this->internet);
		$criteria->compare('mac_address_stb',$this->mac_address_stb,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('notel_teknisi',$this->notel_teknisi,true);
		$criteria->compare('psb',$this->psb);
		$criteria->compare('migrasi',$this->migrasi);
		$criteria->compare('speed',$this->speed);
		$criteria->compare('other_speed',$this->other_speed,true);
		$criteria->compare('kendala',$this->kendala,true);
		$criteria->compare('alasan_decline',$this->alasan_decline,true);
		$criteria->compare('other_alasan_decline_1',$this->other_alasan_decline_1,true);
		$criteria->compare('other_alasan_decline_2',$this->other_alasan_decline_2,true);
		$criteria->compare('url_ttd_pelanggan',$this->url_ttd_pelanggan,true);
		$criteria->compare('url_ttd_mitra',$this->url_ttd_mitra,true);
		$criteria->compare('jumlah_klem_ring',$this->jumlah_klem_ring,true);
		$criteria->compare('panjang_pvc',$this->panjang_pvc,true);
		$criteria->compare('jumlah_breket',$this->jumlah_breket,true);
		$criteria->compare('panjang_drop_core',$this->panjang_drop_core,true);
		$criteria->compare('drop_core',$this->drop_core,true);
		$criteria->compare('meter_awal',$this->meter_awal,true);
		$criteria->compare('meter_akhir',$this->meter_akhir,true);
		$criteria->compare('panjang_tray_cable',$this->panjang_tray_cable,true);
		$criteria->compare('jumlah_tiang_tlpn',$this->jumlah_tiang_tlpn,true);
		$criteria->compare('create_dtm',$this->create_dtm,true);
		$criteria->compare('panjang_utp',$this->panjang_utp,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('witel',$this->witel,true);
		$criteria->compare('fiberzone',$this->fiberzone,true);
		$criteria->compare('regional',$this->regional,true);
		$criteria->compare('is_check_aso',$this->is_check_aso,true);
		$criteria->compare('comment_check_aso',$this->comment_check_aso,true);
		$criteria->compare('filter_reg',$this->filter_reg,true);
		$criteria->compare('approved_tl_by',$this->approved_tl_by,true);
		$criteria->compare('id_valins',$this->id_valins,true);
		$criteria->compare('jarak',$this->jarak,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pemakaian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getData()
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.nik,t.no_wo,t.nama,t.start_date';
		$criteria->condition = 't.id_mitra != "" AND t.nik != ""';
		$criteria->compare('t.nik',$this->nik,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.nama',$this->nama,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));
	}

	public function getRating(){
		$criteria = new CDbCriteria();
		$criteria->select = 't.nik, count(t.nik) , sum(t.rate) ,mid((sum(t.rate)/count(t.nik)),1,3) as average';
		// $criteria->join = '	LEFT JOIN naker.employee_mapping em  on t.nik = em.nik
		// 					LEFT JOIN naker.master_employee me on t.nik = me.nik
		// 					LEFT JOIN naker.master_om mo on em.object_id = mo.object_id
		// 					LEFT JOIN naker.area_ta ta1 on mo.psa = ta1.ta_area
		// 					LEFT JOIN naker.area_ta ta2 on ta1.parent = ta2.ta_area
		// 					LEFT JOIN naker.area_ta ta3 on ta2.parent = ta3.ta_area
		// ';
		$criteria->condition = 't.nik != "" and t.nik != "null" and t.nik is not null and rate is not null ';
		$criteria->group = "t.nik";
		$criteria->limit = "10";
		return $data = $this->findAll($criteria);
		// $criteria->join = 'LEFT JOIN naker.master_employee ta4 ON t.regional = ta4.id';
		// return new CActiveDataProvider($this, array(
		// 	'criteria'=>$criteria,
		// 	));

	}


	public function dataRegional($tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.regional,ta4.ta_area nama_regional,count(t.no_wo) jml_ps,

												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =1,1,0)))AS satu_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =2,1,0)))AS dua_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =3,1,0)))AS tiga_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =4,1,0)))AS stb_tambahan,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =5,1,0)))AS change_stb,

												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =1,1,0)))AS infrastruktur,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =2,1,0)))AS migrasi_satu,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =3,1,0)))AS migrasi_dua,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =4,1,0)))AS migrasi_tiga,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =5,1,0)))AS migrasi_empat,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =6,1,0)))AS migrasi_lima,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =7,1,0)))AS migrasi_enam,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =8,1,0)))AS migrasi_tujuh,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =9,1,0)))AS migrasi_delapan,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =10,1,0)))AS migrasi_sembilan,

												sum(if(t.migrasi > 0 and t.psb > 0,1,0))AS anomali1,
												sum(if(t.migrasi = 0 and t.psb = 0,1,0))AS anomali2';

		$criteria->join = 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id';

		if ($tgl_mulai=='')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "0" and "0" and t.isactive = "Y"';
		}else
		{
			if($tipe_teknisi == 'mitra')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
			}
			elseif($tipe_teknisi == 'telkom_akses')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
			}
			else
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" ';
			}
		}

		$criteria->group = 'ta4.ta_area';
		$criteria->order ='t.regional asc';

		$data = $this->findAll($criteria);

		return $data;
	}

	public function getDataApprovedBa($regional,$witel,$sto_input,$level)
	{
		$criteria = new CDbCriteria();

		// if($level == "3"){
			$criteria->select = 't.sto,
								t.reg_tactical regional,
								t.approved_by,
								t.date_approved,
								t.witel_tactical witel,
								t.fiberzone,
								t.email_pelanggan,
								t.witel_tactical nama_witel,
								t.reg_tactical,
								t.witel_tactical,
								t.email_pelanggan,
								t.nama_pelanggan,
								t.no_wo,
								t.no_telp,
								t.no_kontak,
								t.no_kontak_2,
								t.no_inet,
								t.create_dtm,
								t.nik,t.id_valins,
								case

								when t.psb ="1" then "1P"
								when t.psb ="2" then "2P"
								when t.psb ="3" then "3P"
								when t.psb ="4" then "1P [Voice]"
								when t.psb ="5" then "1P [Internet]"
								when t.psb ="6" then "2p [Voice + Internet]"
								when t.psb ="7" then "2p [Internet + Useetv]"
								when t.migrasi="6" then "1p-3p"
								when t.migrasi="9" then "3p-3p"
								when t.migrasi="11" then "1p-1p [voice]"
								when t.migrasi="12" then "1p-1p [internet]"
								when t.migrasi="13" then "1p-2p [voice+internet]"
								when t.migrasi="14" then "1p-2p [internet+useetv]"
								when t.migrasi="15" then "2p-2p [voice+internet]"
								when t.migrasi="16" then "2p-2p [Internet+useetv]"
								when t.migrasi="8" then "2p-3p"
								when t.migrasi="2" then "1p-3p"
								when t.migrasi="3" then "2p-3p"
								when t.migrasi="20" then "1p-2p [voice+internet]"
								when t.migrasi="21" then "1p-2p [internet+useetv]"
								end as jenis_layanan,
								case
								when t.status_approve ="0" then "Need Validate"
								when t.status_approve ="1" then "Need Approve"
								when t.status_approve ="2" then "Approved"
								when t.status_approve ="-1" then "Problem Foto"
								when t.status_approve ="-10" then "Problem Foto"
								when t.status_approve ="-2" then "Problem BA"
								when t.status_approve ="-3" then "Unspek"
								-- when t.status_approve ="-4" then "Problem Foto & BA"
								-- when t.status_approve ="-5" then "Problem Foto & Unspek"
								-- when t.status_approve ="-6" then "Problem BA & Unspek"
								-- when t.status_approve ="-7" then "Problem Foto & Ba & Unspek"
								end as status_approve';
		// $criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id
		// LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id
		// LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id
		// LEFT JOIN api.master_sto ms ON t.sto = ms.sto
		// ';

		// $criteria->condition = 't.status_approve = "2"';

		if($level == "1"){
			if($regional != "-" && $witel != "-" && $sto_input != "-"){
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.sto = '".$sto_input."' and t.status_approve = 2 and t.isactive = 'Y' ";
			}else if($regional != "-" && $witel != "-" ){
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.status_approve = 2 and t.isactive = 'Y' ";
			}else if($regional != "-"){
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.status_approve = 2 and t.isactive = 'Y' ";
			}else{
				$criteria->condition = "t.status_approve = 2 and t.isactive = 'Y'";

			}
		}else if($level == "2"){

			if($witel != "-" && $sto_input != "-"){
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.sto = '".$sto_input."' and t.status_approve = 2 and t.isactive = 'Y' ";
			}else if($witel != "-"){
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.status_approve = 2 and t.isactive = 'Y'";
			}else{
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.status_approve = 2 and t.isactive = 'Y'";

			}
			// $criteria->condition = "t.reg_tactical. .= '".$regional."'";
			// $criteria->condition = 't.isactive = "Y"';
			// $criteria->condition = 't.status_approve = 2';
		}else if($level == '3'){
			if($sto_input != "-"){
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.sto = '".$sto_input."' and t.status_approve = 2 and t.isactive = 'Y'";
			}else{
				$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.status_approve = 2 and t.isactive = 'Y'";
			}

		}else{
			$criteria->condition = "t.reg_tactical = '".$regional."' and t.witel_tactical = '".$witel."' and t.sto = '".$sto_input."' and t.status_approve = 2 and t.isactive = 'Y'";
		}
		$criteria->condition .= " and t.filter_reg = 'Y' and t.migrasi not in (11,12,14,15,9)";


			 $criteria->compare('t.reg_tactical',$this->regional,true);
			 $criteria->compare('t.witel_tactical',$this->witel,true);
			 $criteria->compare('t.sto',$this->sto,true);
			 $criteria->compare('t.no_wo',$this->no_wo,true);
			 $criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
			 $criteria->compare('t.no_kontak',$this->no_kontak,true);
			 $criteria->compare('t.no_kontak_2',$this->no_kontak_2,true);
			 $criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
			 $criteria->compare('t.no_inet',$this->no_inet,true);
			 $criteria->compare('t.psb',$this->jenis_layanan,true);
			 $criteria->compare('t.create_dtm',$this->create_dtm,true);
			 $criteria->compare('t.approved_by',$this->approved_by,true);
			 $criteria->compare('t.date_approved',$this->date_approved,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));
	}	//	$row = $model_mapping->getData();


	public function getDataInboxTl($type)
	{
		$criteria = new CDbCriteria();

		$tl  = new Tl();
		$nik = Yii::app()->session['nik'];

		$no = 0;
		$in = "";
		foreach($tl->isTl($nik) as $t){
			if($no == 0){
				$in .="'".$t->WORKZONE."'";
			}else{
				$in .=",'".$t->WORKZONE."'";
			}
			$no++;
		}

		$criteria->select = 't.id_valins,
									 t.sto,
									 t.reg_tactical regional,
									 t.witel_tactical witel,
									 t.email_pelanggan,
									 t.nama_pelanggan,
									 t.no_wo,
									 t.no_telp,
									 t.no_kontak,
									 t.no_kontak_2,
									 t.no_inet,
									 t.create_dtm,
									 case
									 when t.psb ="1" then "1P"
									when t.psb ="2" then "2P"
									when t.psb ="3" then "3P"
									when t.psb ="4" then "1P [Voice]"
									when t.psb ="5" then "1P [Internet]"
									when t.psb ="6" then "2p [Voice + Internet]"
									when t.psb ="7" then "2p [Internet + Useetv]"
									when t.migrasi="6" then "1p-3p"
									when t.migrasi="9" then "3p-3p"
									when t.migrasi="11" then "1p-1p [voice]"
									when t.migrasi="12" then "1p-1p [internet]"
									when t.migrasi="13" then "1p-2p [voice+internet]"
									when t.migrasi="14" then "1p-2p [internet+useetv]"
									when t.migrasi="15" then "2p-2p [voice+internet]"
									when t.migrasi="16" then "2p-2p [Internet+useetv]"
									when t.migrasi="8" then "2p-3p"
									when t.migrasi="2" then "1p-3p"
									when t.migrasi="3" then "2p-3p"
									when t.migrasi="20" then "1p-2p [voice+internet]"
									when t.migrasi="21" then "1p-2p [internet+useetv]"
									end as jenis_layanan';

		// $criteria->join 	= ' LEFT JOIN naker.area_ta ta4
		// 							ON t.regional = ta4.id
		// 						LEFT JOIN naker.area_ta ta3
		// 							ON t.fiberzone = ta3.id
		// 						LEFT JOIN naker.area_ta ta2
		// 							ON t.witel = ta2.id
		// 					';

		if($type == "1"){
			$criteria->condition = 't.status_approve = 0 and t.isactive = "Y" and t.sto in( '.$in.')';
		}else{
			$criteria->condition = 't.status_approve < 0 and t.isactive = "Y" and t.sto in('.$in.')';
		}

			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9)';

		$criteria->order = "t.create_dtm desc";


		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('regional',$this->regional,true);
		$criteria->compare('witel',$this->witel,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
	 	$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

			));
	}

	public function updateApproval($id){
		Yii::$app->db->createCommand("UPDATE alista_v3.pemakaian SET status_approve= 3 WHERE no_wo='".$id."'")
		 // ->bindValue(':id',$id )
		 ->execute();
	}

	public function updateJarak($id,$summary){
		Yii::$app->db->createCommand("UPDATE alista_v3.pemakaian SET jarak= '".$summary."' WHERE no_wo='".$id."'")
		 // ->bindValue(':id',$id )
		 ->execute();
	}

	public function dataTeritory($tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.fiberzone,ta3.ta_area nama_fiberzone,count(t.no_wo) jml_ps,

												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =1,1,0)))AS satu_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =2,1,0)))AS dua_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =3,1,0)))AS tiga_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =4,1,0)))AS stb_tambahan,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =5,1,0)))AS change_stb,

												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =1,1,0)))AS infrastruktur,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =2,1,0)))AS migrasi_satu,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =3,1,0)))AS migrasi_dua,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =4,1,0)))AS migrasi_tiga,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =5,1,0)))AS migrasi_empat,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =6,1,0)))AS migrasi_lima,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =7,1,0)))AS migrasi_enam,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =8,1,0)))AS migrasi_tujuh,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =9,1,0)))AS migrasi_delapan,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =10,1,0)))AS migrasi_sembilan,

												sum(if(t.migrasi > 0 and t.psb > 0,1,0))AS anomali1,
												sum(if(t.migrasi = 0 and t.psb = 0,1,0))AS anomali2';

		$criteria->join = 'LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id';

		if ($tgl_mulai=='')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "0" and "0" and t.isactive = "Y" ';
		}else
		{
			if($tipe_teknisi == 'mitra')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'"and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
			}
			elseif($tipe_teknisi == 'telkom_akses')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
			}
			else
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" ';
			}
		}

		$criteria->group ='t.fiberzone';
		$criteria->order ='t.fiberzone asc';

		$data = $this->findAll($criteria);
		return $data;
	}

	public function getOneRowById($id){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition = 't.no_wo = "'.$id.'"';
		$data = $this->findAll($criteria);
		return $data;
	}


	public function dataWitel($tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.witel,ta2.ta_area nama_witel,count(t.no_wo) jml_ps,

							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =1,1,0)))AS satu_p,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =2,1,0)))AS dua_p,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =3,1,0)))AS tiga_p,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =4,1,0)))AS stb_tambahan,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =5,1,0)))AS change_stb,

							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =1,1,0)))AS infrastruktur,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =2,1,0)))AS migrasi_satu,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =3,1,0)))AS migrasi_dua,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =4,1,0)))AS migrasi_tiga,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =5,1,0)))AS migrasi_empat,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =6,1,0)))AS migrasi_lima,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =7,1,0)))AS migrasi_enam,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =8,1,0)))AS migrasi_tujuh,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =9,1,0)))AS migrasi_delapan,
							sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =10,1,0)))AS migrasi_sembilan,

							sum(if(t.migrasi > 0 and t.psb > 0,1,0))AS anomali1,
							sum(if(t.migrasi = 0 and t.psb = 0,1,0))AS anomali2';

		$criteria->join = 'LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';

		//$criteria->condition = 't.regional != "" AND ta2.ta_area != "" AND t.nik != "" AND t.no_wo != "" AND  create_dtm like "'.$tgl.'%"  GROUP BY ta2.ta_area';
		if ($tgl_mulai=='')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "0" and "0" and t.isactive = "Y"';
		}else
		{
			if($tipe_teknisi == 'mitra')
			{
				$criteria->condition = ' date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
			}
			elseif($tipe_teknisi == 'telkom_akses')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
			}
			else
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" ';
			}
		}

		$criteria->group ='t.witel';
		$criteria->order ='t.witel asc';

		$data = $this->findAll($criteria);

		return $data;
	}

	public function dataLevel(){
		$criteria = new CDbCriteria();
		$criteria->select = '';

		$data = $this->findAll($criteria);
		return $data;

	}

	public function dataRekapNewSto($regional,$witel,$sto,$date1,$date2){


		$criteria = new CDbCriteria();

		//status_approve = 1 blm update foto
		//status_approve = 2 need approve
		//status_approve = 3 approved
		//status_approve = -1 kurang foto
		//status_approve = -2 kurang ba
		//status_approve = -2 kurang foto & ba

			$criteria->select = 't.reg_tactical regional,
								t.witel_tactical witel,
								t.sto,count(t.no_wo) jml_ps,
								sum(if(t.status_approve = 0,1,0))AS need_validate,
								sum(if(t.status_approve = 1,1,0))AS need_approve,
			 					sum(if(t.status_approve = 2,1,0))AS approved,
			 					sum(if(t.status_approve = -1 or t.status_approve = -10,1,0))AS kurang_foto,
			 					sum(if(t.status_approve = -2,1,0))AS kurang_ba,
			 				    sum(if(t.status_approve = -3,1,0))AS unspek,
			 				    sum(if(t.status_approve = -4,1,0))AS foto_ba,
			 				    sum(if(t.status_approve = -5,1,0))AS foto_unspek,
			 				    sum(if(t.status_approve = -6,1,0))AS ba_unspek,
			 				    sum(if(t.status_approve = -7,1,0))AS foto_ba_unspek
								';
			// $criteria->join    = 'right join api.master_sto ms on t.sto = ms.sto';

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") && $sto != "-" && trim($sto != "")){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" and t.filter_reg = "Y"';
				}

				$criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.filter_reg = "Y"';
				}

				$criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';


			}else if($regional != "-" && trim($regional != "")){
				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.filter_reg = "Y"';
				}
					$criteria->group = 't.reg_tactical,t.witel_tactical';
			}else{
				$criteria->condition = 't.isactive = "Y" and t.filter_reg = "Y"';
				$criteria->group = 't.reg_tactical';
			}

			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9)';
			$criteria->order ='t.reg_tactical asc';

			$datas = $this->findAll($criteria);
			return $datas;

			// $reg = "-";
			// foreach($datas as $detil){
			// 	$reg = $detil->regional;
			// 	$witel_detil = $detil->witel;
			// 	$sto_detil = $detil->sto;

			// 	if($reg != "-"){
			// 		$data_result[] = array(
			// 			"reg"=>$reg,
			// 			"witel"=>$witel_detil,
			// 			"sto"=>$sto_detil,
			// 			"need_validate"=>$detil->need_validate,
			// 			"need_approve"=>$detil->need_approve,
			// 			"approved"=>$detil->approved,
			// 			"kurang_foto"=>$detil->kurang_foto,
			// 			"kurang_ba"=>$detil->kurang_ba,
			// 			"unspek"=>$detil->unspek,
			// 			"foto_ba"=>$detil->foto_ba,
			// 			"foto_unspek"=>$detil->foto_unspek,
			// 			"ba_unspek"=>$detil->ba_unspek,
			// 			"foto_ba_unspek"=>$detil->foto_ba_unspek
			// 		);
			// 	}

			// }

			// if($reg == "-"){
			// 	$data_result[] = array(
			// 		"reg"=>$regional,
			// 		"witel"=>$witel,
			// 		"sto"=>$sto,
			// 		"need_validate"=>0,
			// 		"need_approve"=>0,
			// 		"approved"=>0,
			// 		"kurang_foto"=>0,
			// 		"kurang_ba"=>0,
			// 		"unspek"=>0,
			// 		"foto_ba"=>0,
			// 		"foto_unspek"=>0,
			// 		"ba_unspek"=>0,
			// 		"foto_ba_unspek"=>0

			// 	);
			// }



		// }

		// return $data_result;


	}

	public function dataRekapPerformTlSto($regional,$witel,$sto,$date1,$date2){


		$criteria = new CDbCriteria();


			$criteria->select = '	t.sto,
														t.reg_tactical regional,
														t.witel_tactical witel,
														t.no_wo,mt.nik_tl,mt.nama_tl,t.approved_tl_by,t.status_approve,
														sum(if(t.status_approve="0",1,0)) as need_validate,
														sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) as need_approve,
														if(round(sum(if(approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) / (sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) + sum(if(t.status_approve="0",1,0))) * 100 / 100,2) IS NUll,
														0,round(sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) / (sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) + sum(if(t.status_approve="0",1,0))) * 100 / 100,2)) as performansi';
				$criteria->join    = '	join api.master_sto ms on t.sto = ms.sto
										 			 			join api.master_tl mt on ms.id = mt.id_sto';

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") ){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.filter_reg = "Y"';
				}

				$criteria->group = 'mt.nik_tl';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.filter_reg = "Y"';
				}

				$criteria->group = 'mt.nik_tl';


			}else if($regional != "-" && trim($regional != "")){
				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.filter_reg = "Y"';
				}
					$criteria->group = 'mt.nik_tl';
			}else{
				$criteria->condition = 't.isactive = "Y" and t.filter_reg = "Y"';
				$criteria->group = 'mt.nik_tl';
			}

			$criteria->order ='t.reg_tactical asc';


			// $datas = $this->findAll($criteria);
			// return $datas;
			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('t.witel_tactical',$this->witel_tactical,true);
			$criteria->compare('t.reg_tactical',$this->reg_tactical,true);
			$criteria->compare('mt.nik_tl',$this->nik_tl,true);
			$criteria->compare('mt.nama_tl',$this->nama_tl,true);
			//$criteria->compare('',$this->,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				));

	}


	public function dataRekapNewWitel($regional,$witel,$sto,$date1,$date2){


		$criteria = new CDbCriteria();

		//status_approve = 1 blm update foto
		//status_approve = 2 need approve
		//status_approve = 3 approved
		//status_approve = -1 kurang foto
		//status_approve = -2 kurang ba
		//status_approve = -2 kurang foto & ba

			$criteria->select = 't.reg_tactical regional,
								t.witel_tactical witel,
								count(t.no_wo) jml_ps,
								sum(if(t.status_approve = 0,1,0))AS need_validate,
								sum(if(t.status_approve = 1,1,0))AS need_approve,
			 					sum(if(t.status_approve = 2,1,0))AS approved,
			 					sum(if(t.status_approve = -1 or t.status_approve = -10,1,0))AS kurang_foto,
			 					sum(if(t.status_approve = -2,1,0))AS kurang_ba,
			 				    sum(if(t.status_approve = -3,1,0))AS unspek,
			 				    sum(if(t.status_approve = -4,1,0))AS foto_ba,
			 				    sum(if(t.status_approve = -5,1,0))AS foto_unspek,
			 				    sum(if(t.status_approve = -6,1,0))AS ba_unspek,
			 				    sum(if(t.status_approve = -7,1,0))AS foto_ba_unspek';

			// $criteria->join    = 'right join api.master_sto ms on t.sto = ms.sto';

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") && $sto != "-" && trim($sto != "")){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" and t.filter_reg = "Y" ';
				}
				$criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.filter_reg = "Y" ';
				}

				$criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "")){
				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.filter_reg = "Y"';
				}
					$criteria->group = 't.reg_tactical,t.witel_tactical';
			}else{
				$criteria->condition = 't.isactive = "Y" and t.filter_reg = "Y"';
				$criteria->group = 't.reg_tactical';
			}

			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9)';
			$criteria->order ='t.reg_tactical asc';
			$datas = $this->findAll($criteria);
			return $datas;

	}

	public function dataRekapPerformTlWit($regional,$witel,$sto,$date1,$date2){

		$criteria = new CDbCriteria();

		$criteria->select = '	t.sto,
													t.reg_tactical regional,
													t.witel_tactical witel,
													t.no_wo,mt.nik_tl,mt.nama_tl,t.approved_tl_by,t.status_approve,
													sum(if(t.status_approve="0",1,0)) as need_validate,
													sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) as need_approve,
													if(round(sum(if(approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) / (sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) + sum(if(t.status_approve="0",1,0))) * 100 / 100,2) IS NUll,
													0,round(sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) / (sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) + sum(if(t.status_approve="0",1,0))) * 100 / 100,2)) as performansi';

			 $criteria->join    = '	join api.master_sto ms on t.sto = ms.sto
			 												join api.master_tl mt on ms.id = mt.id_sto';

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") ){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'"  and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'"  and t.filter_reg = "Y" ';
				}

				$criteria->group = 'mt.nik_tl';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.filter_reg = "Y" ';
				}

				$criteria->group = 'mt.nik_tl';


			}else if($regional != "-" && trim($regional != "")){
				if($date1 !="-" && $date1 !="" && $date2 !="-" && $date2 !=""){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'" and t.filter_reg = "Y"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.filter_reg = "Y"';
				}
					$criteria->group = 'mt.nik_tl';
			}else{
				$criteria->condition = 't.isactive = "Y" and t.filter_reg = "Y"';
				$criteria->group = 'mt.nik_tl';
			}


			$criteria->order ='t.reg_tactical asc';

			// $datas = $this->findAll($criteria);
			// return $datas;
			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('t.witel_tactical',$this->witel_tactical,true);
			$criteria->compare('t.reg_tactical',$this->reg_tactical,true);
			$criteria->compare('mt.nik_tl',$this->nik_tl,true);
			$criteria->compare('mt.nama_tl',$this->nama_tl,true);
			//$criteria->compare('',$this->,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				));

	}


	public function dataRekapTl(){
		$criteria = new CDbCriteria();
		$criteria->select = 't.nik';
		$criteria->condition = 't.isactive = "Y" and t.nik != "" and t.nik != "null" and  t.rate != ""';
		$criteria->group = 't.nik';
		// $criteria->limit = '1';
		$datas = $this->findAll($criteria);
		return $datas;

	}

	public function dataRekapNewRegional($regional,$witel,$sto,$date1,$date2){
		$criteria = new CDbCriteria();

			$criteria->select = 't.reg_tactical regional,
								t.witel_tactical witel,
								count(t.no_wo) jml_ps,
								sum(if(t.status_approve = 0,1,0))AS need_validate,
								sum(if(t.status_approve = 1,1,0))AS need_approve,
			 					sum(if(t.status_approve = 2,1,0))AS approved,
			 					sum(if(t.status_approve = -1 or t.status_approve = -10,1,0))AS kurang_foto,
			 					sum(if(t.status_approve = -2,1,0))AS kurang_ba,
			 				    sum(if(t.status_approve = -3,1,0))AS unspek,
			 				    sum(if(t.status_approve = -4,1,0))AS foto_ba,
			 				    sum(if(t.status_approve = -5,1,0))AS foto_unspek,
			 				    sum(if(t.status_approve = -6,1,0))AS ba_unspek,
			 				    sum(if(t.status_approve = -7,1,0))AS foto_ba_unspek
								';

			$criteria->condition = ' t.isactive = "Y" and t.reg_tactical != "" and t.filter_reg="Y" ';
			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9) ';
				if($date1 !="-" && $date2 !="-" & $date1 !="" && $date2 !=""){
				$criteria->condition .= ' and t.create_dtm between "'.$date1.'" and "'.$date2.'" ';
			}

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") && $sto != "-" && trim($sto != "")){
				$criteria->condition .= ' and t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
				$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" ';
				$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "")){
				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'"';
				$criteria->group = 't.reg_tactical,t.witel_tactical';

			}else{
				$criteria->group = 't.reg_tactical';
			}

			$datas = $this->findAll($criteria);
			return $datas;


			// filter date
			if($date1 !="-" && $date2 !="-" & $date1 !="" && $date2 !=""){
				$criteria->condition .= ' and t.create_dtm between "'.$date1.'" and "'.$date2.'" ';
			}

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") && $sto != "-" && trim($sto != "")){
				$criteria->condition .= ' and t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
				$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" ';
				$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "")){
				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'"';
				$criteria->group = 't.reg_tactical,t.witel_tactical';

			}else{
				$criteria->group = 't.reg_tactical';
			}

			$datas = $this->findAll($criteria);
			return $datas;

	}

	//1 = foto
	//2 = ba
	//3 = unspek
	public function dataReturnRekap($status,$regional,$witel,$sto,$date1,$date2){
		$criteria = new CDbCriteria();

		$criteria->select = '
												t.sto,
												t.reg_tactical regional,
												t.witel_tactical witel,
												t.email_pelanggan,
												t.nama_pelanggan,
												t.no_wo,
												t.no_telp,
												t.no_kontak,
												t.no_kontak_2,
												t.no_inet,
												t.create_dtm,
												case
												when t.psb ="1" then "1P"
	 											when t.psb ="2" then "2P"
	 											when t.psb ="3" then "3P"
	 											when t.psb ="4" then "1P [Voice]"
	 											when t.psb ="5" then "1P [Internet]"
	 											when t.psb ="6" then "2p [Voice + Internet]"
	 											when t.psb ="7" then "2p [Internet + Useetv]"
	 											when t.migrasi="6" then "1p-3p"
	 											when t.migrasi="9" then "3p-3p"
	 											when t.migrasi="11" then "1p-1p [voice]"
	 											when t.migrasi="12" then "1p-1p [internet]"
	 											when t.migrasi="13" then "1p-2p [voice+internet]"
	 											when t.migrasi="14" then "1p-2p [internet+useetv]"
	 											when t.migrasi="15" then "2p-2p [voice+internet]"
	 											when t.migrasi="16" then "2p-2p [Internet+useetv]"
	 											when t.migrasi="8" then "2p-3p"
	 											when t.migrasi="2" then "1p-3p"
	 											when t.migrasi="3" then "2p-3p"
	 											when t.migrasi="20" then "1p-2p [voice+internet]"
	 											when t.migrasi="21" then "1p-2p [internet+useetv]"
	 											end as jenis_layanan';

				$criteria->join    = 'left join api.master_sto ms on t.sto = ms.sto';

			$criteria->condition = ' t.isactive = "Y" and t.reg_tactical != "" and t.filter_reg="Y" ';
			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9) ';

			if ($status == 1) {
				$criteria->condition .= ' and t.status_approve in ("-1","-10") ';
			}else if ($status == 2) {
				$criteria->condition .= ' and t.status_approve = "-2" ';
			}else if ($status == 3) {
				$criteria->condition .= ' and t.status_approve = "-3" ';
			}else {
				$criteria->condition .= ' and t.status_approve = "undefined" ';
			}

			//filter
			if($date1 !="-" && $date2 !="-" & $date1 !="" && $date2 !=""){
				$criteria->condition .= ' and t.create_dtm between "'.$date1.'" and "'.$date2.'" ';
			}

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") && $sto != "-" && trim($sto != "")){
				$criteria->condition .= ' and t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
				//$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" ';
				//$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "")){
				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'"';
				//$criteria->group = 't.reg_tactical,t.witel_tactical';

			}else{
				//$criteria->group = 't.reg_tactical';
			}

			$criteria->compare('t.reg_tactical',$this->regional,true);
			$criteria->compare('t.witel_tactical',$this->witel,true);
			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('t.no_wo',$this->no_wo,true);
			$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
			$criteria->compare('t.no_kontak',$this->no_kontak,true);
			$criteria->compare('t.no_kontak_2',$this->no_kontak_2,true);
			$criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
			$criteria->compare('t.no_inet',$this->no_inet,true);
			$criteria->compare('t.psb',$this->jenis_layanan,true);
			$criteria->compare('t.create_dtm',$this->create_dtm,true);
			$criteria->compare('t.approved_by',$this->approved_by,true);
			$criteria->compare('t.date_approved',$this->date_approved,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				));

	}

	public function dataReturnRekapExcel($status,$regional,$witel,$sto,$date1,$date2){
		$criteria = new CDbCriteria();

		$criteria->select = '
		t.sto,
t.reg_tactical regional,
t.witel_tactical witel,
t.nama_pelanggan,
t.no_wo,
t.no_kontak,
t.no_kontak_2,
t.no_inet,
t.lat_pelanggan,
t.long_pelanggan,
t.alamat_pelanggan,
t.id_mitra,
t.create_dtm,
t.nik,
t.nik_teknisi_2,
case
when t.psb ="1" then "1P"
when t.psb ="2" then "2P"
when t.psb ="3" then "3P"
when t.psb ="4" then "1P [Voice]"
when t.psb ="5" then "1P [Internet]"
when t.psb ="6" then "2p [Voice + Internet]"
when t.psb ="7" then "2p [Internet + Useetv]"
when t.migrasi="6" then "1p-3p"
when t.migrasi="9" then "3p-3p"
when t.migrasi="11" then "1p-1p [voice]"
when t.migrasi="12" then "1p-1p [internet]"
when t.migrasi="13" then "1p-2p [voice+internet]"
when t.migrasi="14" then "1p-2p [internet+useetv]"
when t.migrasi="15" then "2p-2p [voice+internet]"
when t.migrasi="16" then "2p-2p [Internet+useetv]"
when t.migrasi="8" then "2p-3p"
when t.migrasi="2" then "1p-3p"
when t.migrasi="3" then "2p-3p"
when t.migrasi="20" then "1p-2p [voice+internet]"
when t.migrasi="21" then "1p-2p [internet+useetv]"
end as jenis_layanan,
case
when t.status_approve ="0" then "Need Validate"
when t.status_approve ="1" then "Need Approve"
when t.status_approve ="2" then "Approved"
when t.status_approve ="-1" then "Problem Foto"
when t.status_approve ="-10" then "Problem Foto"
when t.status_approve ="-2" then "Problem BA"
when t.status_approve ="-3" then "Unspek"
-- when t.status_approve ="-4" then "Problem Foto & BA"
-- when t.status_approve ="-5" then "Problem Foto & Unspek"
-- when t.status_approve ="-6" then "Problem BA & Unspek"
-- when t.status_approve ="-7" then "Problem Foto & Ba & Unspek"
end as status_approve';

				$criteria->join    = 'left join api.master_sto ms on t.sto = ms.sto';

			$criteria->condition = ' t.isactive = "Y" and t.reg_tactical != "" and t.filter_reg="Y" ';
			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9) ';

			if ($status == 1) {
				$criteria->condition .= ' and t.status_approve in ("-1","-10") ';
			}else if ($status == 2) {
				$criteria->condition .= ' and t.status_approve = "-2" ';
			}else if ($status == 3) {
				$criteria->condition .= ' and t.status_approve = "-3" ';
			}else {
				$criteria->condition .= ' and t.status_approve = "undefined" ';
			}

			//filter
			if($date1 !="-" && $date2 !="-" & $date1 !="" && $date2 !=""){
				$criteria->condition .= ' and t.create_dtm between "'.$date1.'" and "'.$date2.'" ';
			}

			if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel != "") && $sto != "-" && trim($sto != "")){
				$criteria->condition .= ' and t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
				//$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" ';
				//$criteria->group = 't.reg_tactical,ms.witel_versi_tactical,t.sto';

			}else if($regional != "-" && trim($regional != "")){
				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'"';
				//$criteria->group = 't.reg_tactical,t.witel_tactical';

			}else{
				//$criteria->group = 't.reg_tactical';
			}

			$datas = $this->findAll($criteria);
			return $datas;

	}

	public function dataRekapPerformTlReg($regional,$witel,$date1,$date2){
			$criteria = new CDbCriteria();

			$criteria->select = '	t.sto,
														t.reg_tactical regional,
														t.witel_tactical witel,
														t.no_wo,mt.nik_tl,mt.nama_tl,t.approved_tl_by,t.status_approve,
														sum(if(t.status_approve="0",1,0)) as need_validate,
														sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) as need_approve,
														if(round(sum(if(approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) / (sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) + sum(if(t.status_approve="0",1,0))) * 100 / 100,2) IS NUll,
														0,round(sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) / (sum(if(t.approved_tl_by = mt.nik_tl and t.status_approve > 0,1,0)) + sum(if(t.status_approve="0",1,0))) * 100 / 100,2)) as performansi';

			$criteria->join    = '	join api.master_sto ms on t.sto = ms.sto
															join api.master_tl mt on ms.id = mt.id_sto';

			$criteria->condition = ' t.isactive = "Y" and t.reg_tactical != "" and t.filter_reg="Y" ';
				if($date1 !="-" && $date2 !="-" & $date1 !="" && $date2 !=""){
				$criteria->condition .= ' and t.create_dtm between "'.$date1.'" and "'.$date2.'" ';
			}

		if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" ';
				$criteria->group = 'mt.nik_tl';

			}else if($regional != "-" && trim($regional != "")){
				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'"';
				$criteria->group = 'mt.nik_tl';

			}else{
				$criteria->group = 'mt.nik_tl';
			}

			// $datas = $this->findAll($criteria);
			// return $datas;
			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('t.witel_tactical',$this->witel_tactical,true);
			$criteria->compare('t.reg_tactical',$this->reg_tactical,true);
			$criteria->compare('mt.nik_tl',$this->nik_tl,true);
			$criteria->compare('mt.nama_tl',$this->nama_tl,true);
			//$criteria->compare('',$this->,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				));


			// filter date
			$criteria->condition = ' t.isactive = "Y" and t.reg_tactical != "" and t.filter_reg="Y" ';
				if($date1 !="-" && $date2 !="-" & $date1 !="" && $date2 !=""){
				$criteria->condition .= ' and t.create_dtm between "'.$date1.'" and "'.$date2.'" ';
			}

		if($regional != "-" && trim($regional != "") && $witel != "-" && trim($witel) != ""){

				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'" and  ms.witel_versi_tactical = "'.$witel.'" ';
				$criteria->group = 'mt.nik_tl';

			}else if($regional != "-" && trim($regional != "")){
				$criteria->condition .= ' and  t.reg_tactical = "'.$regional.'"';
				$criteria->group = 'mt.nik_tl';

			}else{
				$criteria->group = 'mt.nik_tl';
			}
			// $datas = $this->findAll($criteria);
			// return $datas;
			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('t.witel_tactical',$this->witel_tactical,true);
			$criteria->compare('t.reg_tactical',$this->reg_tactical,true);
			$criteria->compare('mt.nik_tl',$this->nik_tl,true);
			$criteria->compare('mt.nama_tl',$this->nama_tl,true);
			//$criteria->compare('',$this->,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				));

	}

	public function dataRekapNewWithSto($regional,$witel,$sto,$date1,$date2){
		$criteria = new CDbCriteria();

		//status_approve = 1 blm update foto
		//status_approve = 2 need approve
		//status_approve = 3 approved
		//status_approve = -1 kurang foto
		//status_approve = -2 kurang ba
		//status_approve = -2 kurang foto & ba

		$criteria->select = 't.no_wo,t.id_valins,
							t.sto,
							t.no_inet,
							t.nama_pelanggan,
							t.reg_tactical regional,
							t.witel_tactical witel,
							case
							when t.psb ="3" then "3P"
							when t.psb ="4" then "1P [Voice]"
							when t.psb ="5" then "1P [Internet]"
							when t.psb ="6" then "2P [Voice + Internet]"
							when t.psb ="7" then "2P [Internet + UseeTv]"
							when t.migrasi="1" then "Infrastuktur"
							when t.migrasi="2" then "Layanan 1P-3P"
							when t.migrasi="3" then "Layanan 2P-3P"
							when t.migrasi="10" then "Layanan 1P-2P"
							when t.migrasi="11" then "Layanan 1P-1P [Voice]"
							when t.migrasi="4" then "Infra 1P-1P"
							when t.migrasi="5" then "Infra 1P-2P"
							when t.migrasi="6" then "Infra 1P-3P"
							when t.migrasi="7" then "Infra 2P-2P"
							when t.migrasi="8" then "Infra 2P-3P"
							when t.migrasi="9" then "Infra 3P-3P"
							end as jenis_layanan,
							case
							when t.ukur ="1" then "Spek"
							when t.ukur ="2" then "Under Spek"
							when t.ukur ="3" then "Tidak Terukur"
							ELSE "Belum Terukur"
							end as ukur_usage,
							case
							when t.status_approve ="0" then "Need Validate"
							when t.status_approve ="1" then "Need Approve"
							when t.status_approve ="2" then "Approved"
							when t.status_approve ="-1" then "Problem Foto"
							when t.status_approve ="-2" then "Problem BA"
							when t.status_approve ="-3" then "Unspek"
							when t.status_approve ="-4" then "Problem Foto & BA"
							when t.status_approve ="-5" then "Problem Foto & Unspek"
							when t.status_approve ="-6" then "Problem BA & Unspek"
							when t.status_approve ="-7" then "Problem Foto & Ba & Unspek"
							when t.status_approve ="-10" then "Problem Foto"
							end as status_approve
							';

			// $criteria->join    = 'right join api.master_sto ms on t.sto = ms.sto';

			if($regional != "-" && $witel != "-" && $sto != "-"){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
				}

				// $criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';
			}else if($regional != "-" && $witel != "-"){

				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" ';
				}

				// $criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';

			}else if($regional != "-" ){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'"';
				}
				// $criteria->group = 't.reg_tactical';
			}else{
				$criteria->condition = 't.isactive = "Y"';
			}
			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9)';

		// $criteria->group = 'ms.reg';
		$criteria->order ='t.reg_tactical asc';
		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('t.reg_tactical',$this->reg_tactical,true);
		$criteria->compare('t.witel_tactical',$this->witel_tactical,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
		$criteria->compare('t.id_mitra',$this->id_mitra,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
	 	$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);
		$criteria->compare('t.ukur',$this->ukur,true);
		$criteria->compare('t.status_approve',$this->status_approve,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));


	}

	public function dataRekapNewWithStoForExcel($regional,$witel,$sto,$date1,$date2){
		$criteria = new CDbCriteria();

		//status_approve = 1 blm update foto
		//status_approve = 2 need approve
		//status_approve = 3 approved
		//status_approve = -1 kurang foto
		//status_approve = -2 kurang ba
		//status_approve = -2 kurang foto & ba

		$criteria->select = 't.*,
							case
							when t.psb ="3" then "3P"
							when t.psb ="4" then "1P [Voice]"
							when t.psb ="5" then "1P [Internet]"
							when t.psb ="6" then "2P [Voice + Internet]"
							when t.psb ="7" then "2P [Internet + UseeTv]"
							when t.migrasi="1" then "Infrastuktur"
							when t.migrasi="2" then "Layanan 1P-3P"
							when t.migrasi="3" then "Layanan 2P-3P"
							when t.migrasi="10" then "Layanan 1P-2P"
							when t.migrasi="11" then "Layanan 1P-1P [Voice]"
							when t.migrasi="4" then "Infra 1P-1P"
							when t.migrasi="5" then "Infra 1P-2P"
							when t.migrasi="6" then "Infra 1P-3P"
							when t.migrasi="7" then "Infra 2P-2P"
							when t.migrasi="8" then "Infra 2P-3P"
							when t.migrasi="9" then "Infra 3P-3P"
							end as jenis_layanan,
							case
							when t.ukur ="1" then "Spek"
							when t.ukur ="2" then "Under Spek"
							when t.ukur ="3" then "Tidak Terukur"
							ELSE "Belum Terukur"
							end as ukur_usage,
							case
							when t.status_approve ="0" then "Need Validate"
							when t.status_approve ="1" then "Need Approve"
							when t.status_approve ="2" then "Approved"
							when t.status_approve ="-1" then "Problem Foto"
							when t.status_approve ="-10" then "Problem Foto"
							when t.status_approve ="-2" then "Problem BA"
							when t.status_approve ="-3" then "Unspek"
							-- when t.status_approve ="-4" then "Problem Foto & BA"
							-- when t.status_approve ="-5" then "Problem Foto & Unspek"
							-- when t.status_approve ="-6" then "Problem BA & Unspek"
							-- when t.status_approve ="-7" then "Problem Foto & Ba & Unspek"
							end as status_approve
							';

			$criteria->condition = ' t.isactive = "Y" ';

			// filter by date
			if($date1 !="-" && $date2 !="-"){
				if ($date1 == 'Y') {
					//untuk excel approved karena waktunya dikit jadi numpang dulu di function ini
						$criteria->condition .= ' and t.status_approve = "2" ';
				}else {
					$criteria->condition .=' and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}

			}



			if($regional != "-" && $witel != "-" && $sto != "-"){
					$criteria->condition .= ' and t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
			}else if($regional != "-" && $witel != "-"){
				$criteria->condition .= ' and t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" ';
			}else if($regional != "-" ){
				$criteria->condition .= ' and t.reg_tactical = "'.$regional.'"';
			}
			$criteria->condition .= ' and t.filter_reg = "Y" ';
			$criteria->condition .= ' and t.migrasi not in (11,12,14,15,9)';

			//$criteria->limit = 10;
			$data = $this->findAll($criteria);
			return $data;


	}

	public function dataSto($tgl_mulai,$tgl_selesai,$tipe_teknisi,$sto)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,count(t.no_wo) jml_ps,

												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =1,1,0)))AS satu_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =2,1,0)))AS dua_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =3,1,0)))AS tiga_p,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =4,1,0)))AS stb_tambahan,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.psb =5,1,0)))AS change_stb,

												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =1,1,0)))AS infrastruktur,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =2,1,0)))AS migrasi_satu,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =3,1,0)))AS migrasi_dua,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =4,1,0)))AS migrasi_tiga,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =5,1,0)))AS migrasi_empat,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =6,1,0)))AS migrasi_lima,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =7,1,0)))AS migrasi_enam,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =8,1,0)))AS migrasi_tujuh,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =9,1,0)))AS migrasi_delapan,
												sum(if(t.migrasi > 0 and t.psb > 0,0,if(t.migrasi =10,1,0)))AS migrasi_sembilan,

												sum(if(t.migrasi > 0 and t.psb > 0,1,0))AS anomali1,
												sum(if(t.migrasi = 0 and t.psb = 0,1,0))AS anomali2';

		//$criteria->join = 'LEFT JOIN api.master_sto ta ON t.sto = ta.sto';
		if ($tgl_mulai=='')
		{
			$criteria->condition = ' trim(t.sto) ="'.$sto.'" AND date_format(t.create_dtm, "%Y-%m-%d") between "0" and "0" and t.isactive = "Y" ';
		}else
		{
			if($tipe_teknisi == 'mitra' && $sto ='all_sto')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';

			}elseif($tipe_teknisi == 'mitra')
			{
				$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
			}

			if($tipe_teknisi == 'telkom_akses' && $sto == 'all_sto')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';

			}elseif($tipe_teknisi == 'telkom_akses')
			{
				$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
			}

			if($tipe_teknisi == 'all_teknisi' && $sto == 'all_sto')
			{
				$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" ';

			}elseif($tipe_teknisi == 'all_teknisi')
			{
				$criteria->condition = ' trim(t.sto) ="'.$sto.'" AND date_format(t.create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y" ';
			}
		}

		$criteria->group ='trim(t.sto)';
		$criteria->order ='trim(t.sto) asc';

		$data = $this->findAll($criteria);

		return $data;
	}


	public function excel_reg($reg,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{


		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,
												t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case
												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"

												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';

		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.regional = "'.$reg.'"  and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.regional = "'.$reg.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}else
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.regional = "'.$reg.'" and t.isactive = "Y" ';
		}

		//$criteria->group ='t.no_wo';
		$data = $this->findAll($criteria);

		return $data;
	}

	public function excel_fz($fz,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,
												 t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"

												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';
	//	$criteria->condition = 't.regional != "" AND ta4.ta_area != "" AND t.nik != "" AND t.no_wo != "" AND  create_dtm like "'.$tgl.'%"  AND t.fiberzone = "'.$fz.'"';

	if($tipe_teknisi == 'mitra')
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}
	elseif($tipe_teknisi == 'telkom_akses')
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}
	else
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'"  AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}

		// $criteria->group ='t.no_wo';
		$data = $this->findAll($criteria);

		return $data;
	}

	public function excel_witel($witel,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,
												 t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"

												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';
		//$criteria->condition = 't.regional != "" AND ta4.ta_area != "" AND t.nik != "" AND t.no_wo != "" AND  create_dtm like "'.$tgl.'%"  AND t.witel = "'.$witel.'"';
		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses"  AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}
		elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}
		else
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}

		// $criteria->group ='t.no_wo';
		$data = $this->findAll($criteria);

		return $data;
	}

	public function excel_sto($sto,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,
												 t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"

												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';

		if($tipe_teknisi == 'mitra' && $sto ='all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';

		}elseif($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'telkom_akses' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'"  AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'all_teknisi' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}elseif($tipe_teknisi == 'all_teknisi')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}

		// $criteria->group ='t.no_wo';
		$data = $this->findAll($criteria);
		return $data;

	}


	public function excel_rekap($regional,$witel,$sto,$date1,$date2)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,
												 t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"

												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';

		if($tipe_teknisi == 'mitra' && $sto ='all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';

		}elseif($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'telkom_akses' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'"  AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'all_teknisi' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}elseif($tipe_teknisi == 'all_teknisi')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}

		// $criteria->group ='t.no_wo';
		$data = $this->findAll($criteria);
		return $data;

	}


	public function getDataReg($regional,$witel,$sto,$date1,$date2)
	{
	$criteria = new CDbCriteria();
		//status_approve = 1 blm update foto
		//status_approve = 2 need approve
		//status_approve = 3 approved
		//status_approve = -1 kurang foto
		//status_approve = -2 kurang ba
		//status_approve = -2 kurang foto & ba

		$criteria->select = 't.no_wo,
							t.sto,
							t.no_inet,
							t.nama_pelanggan,
							t.reg_tactical regional,
							t.witel_tactical witel,
							case
							when t.psb ="3" then "3P"
							when t.psb ="4" then "1P [Voice]"
							when t.psb ="5" then "1P [Internet]"
							when t.psb ="6" then "2P [Voice + Internet]"
							when t.psb ="7" then "2P [Internet + UseeTv]"
							when t.migrasi="1" then "Infrastuktur"
							when t.migrasi="2" then "Layanan 1P-3P"
							when t.migrasi="3" then "Layanan 2P-3P"
							when t.migrasi="10" then "Layanan 1P-2P"
							when t.migrasi="11" then "Layanan 1P-1P [Voice]"
							when t.migrasi="4" then "Infra 1P-1P"
							when t.migrasi="5" then "Infra 1P-2P"
							when t.migrasi="6" then "Infra 1P-3P"
							when t.migrasi="7" then "Infra 2P-2P"
							when t.migrasi="8" then "Infra 2P-3P"
							when t.migrasi="9" then "Infra 3P-3P"
							end as jenis_layanan,
							case
							when t.ukur ="1" then "Spek"
							when t.ukur ="2" then "Under Spek"
							when t.ukur ="3" then "Tidak Terukur"
							ELSE "Belum Terukur"
							end as ukur_usage,
							case
							when t.status_approve ="0" then "Need Validate"
							when t.status_approve ="1" then "Need Approve"
							when t.status_approve ="2" then "Approved"
							when t.status_approve ="-1" then "Problem Foto"
							when t.status_approve ="-10" then "Problem Foto"
							when t.status_approve ="-2" then "Problem BA"
							when t.status_approve ="-3" then "Unspek"
							when t.status_approve ="-4" then "Problem Foto & BA"
							when t.status_approve ="-5" then "Problem Foto & Unspek"
							when t.status_approve ="-6" then "Problem BA & Unspek"
							when t.status_approve ="-7" then "Problem Foto & Ba & Unspek"
							end as status_approve
							';

			// $criteria->join    = 'right join api.master_sto ms on t.sto = ms.sto';

			if($regional != "-" && $witel != "-" && $sto != "-"){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.sto = "'.$sto.'" ';
				}

				// $criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';
			}else if($regional != "-" && $witel != "-"){

				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and  t.witel_tactical = "'.$witel.'" ';
				}

				// $criteria->group = 't.reg_tactical,t.witel_tactical,t.sto';

			}else if($regional != "-" ){
				if($date1 !="-" && $date2 !="-"){
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'" and t.create_dtm between "'.$date1.'" and "'.$date2.'"';
				}else{
					$criteria->condition = 't.isactive = "Y" and t.reg_tactical != "" and  t.reg_tactical = "'.$regional.'"';
				}
				// $criteria->group = 't.reg_tactical';
			}else{
				$criteria->condition = 't.isactive = "Y"';
			}


		// $criteria->group = 'ms.reg';
		$criteria->order ='t.reg_tactical asc';
		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('t.reg_tactical',$this->reg_tactical,true);
		$criteria->compare('t.witel_tactical',$this->witel_tactical,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
		$criteria->compare('t.id_mitra',$this->id_mitra,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
		$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);
		$criteria->compare('t.ukur',$this->ukur,true);
		$criteria->compare('t.status_approve',$this->status_approve,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));

	}

	public function getDataRekapNew($reg,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();

		$criteria->select = 't.sto,
												 t.regional,
												 ta4.ta_area nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel,
												 ta2.ta_area nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case
												 when t.psb ="1" then "1P"
												 when t.psb ="2" then "2P"
												 when t.psb ="3" then "3P"
												 when t.psb ="4" then "STB Tambahan"
												 when t.psb ="5" then "Change STB"
												 when t.migrasi="1" then "Infrastuktur"
												 when t.migrasi="2" then "Layanan 1p-3p"
												 when t.migrasi="3" then "Layanan 2p-3p"
												 when t.migrasi="10" then "Layanan 1p-2p"
												 when t.migrasi="4" then "Infra 1p-1p"
												 when t.migrasi="5" then "Infra 1p-2p"
												 when t.migrasi="6" then "Infra 1p-3p"
												 when t.migrasi="7" then "Infra 2p-2p"
												 when t.migrasi="8" then "Infra 2p-3p"
												 when t.migrasi="9" then "Infra 3p-3p"
												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';

		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.regional = "'.$reg.'"  and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.regional = "'.$reg.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}else
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.regional = "'.$reg.'" and t.isactive = "Y" ';
		}

		//$criteria->group ='t.no_wo';
		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('ta4.ta_area',$this->nama_regional,true);
		$criteria->compare('ta3.ta_area',$this->nama_fiberzone,true);
		$criteria->compare('ta2.ta_area',$this->nama_witel,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.id_mitra',$this->id_mitra,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
		$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

			));
	}

	public function getDataFz($fz,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,
												 t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"
												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';
	//	$criteria->condition = 't.regional != "" AND ta4.ta_area != "" AND t.nik != "" AND t.no_wo != "" AND  create_dtm like "'.$tgl.'%"  AND t.fiberzone = "'.$fz.'"';

	if($tipe_teknisi == 'mitra')
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" AND t.fiberzone = "'.$fz.'" and t.isactive = "Y"';
	}
	elseif($tipe_teknisi == 'telkom_akses')
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}
	else
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'"  AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}

		//$criteria->group ='t.no_wo';
		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('ta4.ta_area',$this->nama_regional,true);
		$criteria->compare('ta3.ta_area',$this->nama_fiberzone,true);
		$criteria->compare('ta2.ta_area',$this->nama_witel,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.id_mitra',$this->id_mitra,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
		$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

			));
	}

	public function getDataWitel($witel,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{
		$criteria = new CDbCriteria();

		$criteria->select = 't.sto,
												t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"
												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';
		//$criteria->condition = 't.regional != "" AND ta4.ta_area != "" AND t.nik != "" AND t.no_wo != "" AND  create_dtm like "'.$tgl.'%"  AND t.witel = "'.$witel.'"';
		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses"  AND t.witel ="'.$witel.'" and t.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}
		else
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}

		//$criteria->group ='t.no_wo';
		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('ta4.ta_area',$this->nama_regional,true);
		$criteria->compare('ta3.ta_area',$this->nama_fiberzone,true);
		$criteria->compare('ta2.ta_area',$this->nama_witel,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.id_mitra',$this->id_mitra,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
	 	$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

			));
	}

	public function getDataSto($sto,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{
		$criteria = new CDbCriteria();

		$criteria->select = 't.sto,
												 t.regional,
												 t.reg_tactical nama_regional,
												 t.fiberzone,
												 ta3.ta_area nama_fiberzone,
												 t.witel_tactical witel,
												 t.email_pelanggan,
												 t.witel_tactical nama_witel,
												 t.email_pelanggan,
												 ta2.ta_area nama_witel,
												 t.nama_pelanggan,
												 t.no_wo,
												 t.no_telp,
												 t.no_kontak,
												 t.no_inet,
												 t.id_mitra,
												 t.create_dtm,
												 t.nik,
												 case

												 when t.psb ="1" then "1P"
												when t.psb ="2" then "2P"
												when t.psb ="3" then "3P"
												when t.psb ="4" then "1P [Voice]"
												when t.psb ="5" then "1P [Internet]"
												when t.psb ="6" then "2p [Voice + Internet]"
												when t.psb ="7" then "2p [Internet + Useetv]"
												when t.migrasi="6" then "1p-3p"
												when t.migrasi="9" then "3p-3p"
												when t.migrasi="11" then "1p-1p [voice]"
												when t.migrasi="12" then "1p-1p [internet]"
												when t.migrasi="13" then "1p-2p [voice+internet]"
												when t.migrasi="14" then "1p-2p [internet+useetv]"
												when t.migrasi="15" then "2p-2p [voice+internet]"
												when t.migrasi="16" then "2p-2p [Internet+useetv]"
												when t.migrasi="8" then "2p-3p"
												when t.migrasi="2" then "1p-3p"
												when t.migrasi="3" then "2p-3p"
												when t.migrasi="20" then "1p-2p [voice+internet]"
												when t.migrasi="21" then "1p-2p [internet+useetv]"
												 end as jenis_layanan';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id';

		if($tipe_teknisi == 'mitra' && $sto ='all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';

		}elseif($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'telkom_akses' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'trim(t.sto) = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'all_teknisi' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}elseif($tipe_teknisi == 'all_teknisi')
		{
			$criteria->condition = 'trim(t.sto )= "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}

		$criteria->compare('t.sto',$this->sto,true);
		$criteria->compare('ta4.ta_area',$this->nama_regional,true);
		$criteria->compare('ta3.ta_area',$this->nama_fiberzone,true);
		$criteria->compare('ta2.ta_area',$this->nama_witel,true);
		$criteria->compare('t.create_dtm',$this->create_dtm,true);
		$criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
		$criteria->compare('t.id_mitra',$this->id_mitra,true);
		$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('t.no_wo',$this->no_wo,true);
		$criteria->compare('t.no_telp',$this->no_telp,true);
		$criteria->compare('t.no_kontak',$this->no_kontak,true);
		$criteria->compare('t.no_inet',$this->no_inet,true);
	 	$criteria->compare('t.psb',$this->jenis_layanan,true);
		$criteria->compare('t.nik',$this->nik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,

			));
	}

	public function getDataAll($regional,$witel,$sto,$level)
		{
			$criteria = new CDbCriteria();

				$criteria->select = 't.sto,t.id_valins,
								ms.witel_versi_tactical as witel,
								ms.reg as regional,
								t.fiberzone,
								t.email_pelanggan,
								t.nama_pelanggan,
								t.no_wo,
								t.no_telp,
								t.no_kontak,
								t.no_inet,
								t.id_mitra,
								t.create_dtm,
								t.nik,
								case

								when t.psb ="1" then "1P"
								when t.psb ="2" then "2P"
								when t.psb ="3" then "3P"
								when t.psb ="4" then "1P [Voice]"
								when t.psb ="5" then "1P [Internet]"
								when t.psb ="6" then "2p [Voice + Internet]"
								when t.psb ="7" then "2p [Internet + Useetv]"
								when t.migrasi="6" then "1p-3p"
								when t.migrasi="9" then "3p-3p"
								when t.migrasi="11" then "1p-1p [voice]"
								when t.migrasi="12" then "1p-1p [internet]"
								when t.migrasi="13" then "1p-2p [voice+internet]"
								when t.migrasi="14" then "1p-2p [internet+useetv]"
								when t.migrasi="15" then "2p-2p [voice+internet]"
								when t.migrasi="16" then "2p-2p [Internet+useetv]"
								when t.migrasi="8" then "2p-3p"
								when t.migrasi="2" then "1p-3p"
								when t.migrasi="3" then "2p-3p"
								when t.migrasi="20" then "1p-2p [voice+internet]"
								when t.migrasi="21" then "1p-2p [internet+useetv]"
								end as jenis_layanan,
								case
								when t.status_approve ="0" then "Need Validate"
								when t.status_approve ="1" then "Need Approve"
								when t.status_approve ="2" then "Approved"
								when t.status_approve ="-1" then "Problem Foto"
								when t.status_approve ="-10" then "Problem Foto"
								when t.status_approve ="-2" then "Problem BA"
								when t.status_approve ="-3" then "Unspek"
								-- when t.status_approve ="-4" then "Problem Foto & BA"
								-- when t.status_approve ="-5" then "Problem Foto & Unspek"
								-- when t.status_approve ="-6" then "Problem BA & Unspek"
								-- when t.status_approve ="-7" then "Problem Foto & Ba & Unspek"
								end as status_approve';

			$criteria->join    = 'right join api.master_sto ms on t.sto = ms.sto';

			if($level == "1"){
				if($regional != "-" && $witel != "-" && $sto != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9)";
				}else if($regional != "-" && $witel != "-" ){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."'  and t.isactive = 'Y' and migrasi not in(11,12,14,15,9)  ";
				}else if($regional != "-"){
					$criteria->condition = "ms.reg = '".$regional."'  and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";
				}else{
					$criteria->condition = "t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";

				}
			}else if($level == "2"){

				if($witel != "-" && $sto != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";
				}else if($witel != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";
				}else{
					$criteria->condition = "ms.reg = '".$regional."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";

				}
				// $criteria->condition = "ms.reg. .= '".$regional."'";
				// $criteria->condition = 't.isactive = "Y"';
				 // $criteria->condition .= 't.status_approve in (-7,-6,-5,-4,-3,-2,-1,0,1,2)';
			}else if($level == '3'){
				if($sto != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";
				}else{
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) ";
				}

			}else{
				$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."'  and t.isactive = 'Y' and migrasi not in(11,12,14,15,9)";
			}
			$criteria->condition .= " and t.filter_reg = 'Y' ";
			$criteria->condition .= ' and t.status_approve in (-3,-2,-1,0,1,2)';
			$criteria->order = "t.create_dtm desc";

			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('ms.reg',$this->regional,true);
			// $criteria->compare('ta3.ta_area',$this->nama_fiberzone,true);
			$criteria->compare('ms.witel_versi_tactical',$this->witel,true);
			// $criteria->compare('t.create_dtm',$this->create_dtm,true);
			// $criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
			// $criteria->compare('t.id_mitra',$this->id_mitra,true);
			$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
			$criteria->compare('t.no_wo',$this->no_wo,true);
			// $criteria->compare('t.no_telp',$this->no_telp,true);
			$criteria->compare('t.no_kontak',$this->no_kontak,true);
			// $criteria->compare('t.no_inet',$this->no_inet,true);
			// $criteria->compare('t.psb',$this->jenis_layanan,true);
			// $criteria->compare('t.nik',$this->nik,true);
			// $criteria->compare('ms.reg',$this->reg_tactical,true);
			// $criteria->compare('ms.witel_versi_tactical',$this->witel_tactical,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,

				));
		}


	public function getDataApproval($regional,$witel,$sto,$level)
		{
			$criteria = new CDbCriteria();

				$criteria->select = 't.sto,t.id_valins,
								ms.witel_versi_tactical as witel,
								ms.reg as regional,
								t.fiberzone,
								t.email_pelanggan,
								t.nama_pelanggan,
								t.no_wo,
								t.no_telp,
								t.no_kontak,
								t.no_inet,
								t.id_mitra,
								t.create_dtm,
								t.nik,
								case

								when t.psb ="1" then "1P"
								when t.psb ="2" then "2P"
								when t.psb ="3" then "3P"
								when t.psb ="4" then "1P [Voice]"
								when t.psb ="5" then "1P [Internet]"
								when t.psb ="6" then "2p [Voice + Internet]"
								when t.psb ="7" then "2p [Internet + Useetv]"
								when t.migrasi="6" then "1p-3p"
								when t.migrasi="9" then "3p-3p"
								when t.migrasi="11" then "1p-1p [voice]"
								when t.migrasi="12" then "1p-1p [internet]"
								when t.migrasi="13" then "1p-2p [voice+internet]"
								when t.migrasi="14" then "1p-2p [internet+useetv]"
								when t.migrasi="15" then "2p-2p [voice+internet]"
								when t.migrasi="16" then "2p-2p [Internet+useetv]"
								when t.migrasi="8" then "2p-3p"
								when t.migrasi="2" then "1p-3p"
								when t.migrasi="3" then "2p-3p"
								when t.migrasi="20" then "1p-2p [voice+internet]"
								when t.migrasi="21" then "1p-2p [internet+useetv]"
								end as jenis_layanan';

			$criteria->join    = 'right join api.master_sto ms on t.sto = ms.sto';

			if($level == "1"){
				if($regional != "-" && $witel != "-" && $sto != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1 ";
				}else if($regional != "-" && $witel != "-" ){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."'  and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1 ";
				}else if($regional != "-"){
					$criteria->condition = "ms.reg = '".$regional."'  and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";
				}else{
					$criteria->condition = "t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";

				}
			}else if($level == "2"){

				if($witel != "-" && $sto != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";
				}else if($witel != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";
				}else{
					$criteria->condition = "ms.reg = '".$regional."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";

				}
				// $criteria->condition = "ms.reg. .= '".$regional."'";
				// $criteria->condition = 't.isactive = "Y"';
				// $criteria->condition = 't.status_approve = 2';
			}else if($level == '3'){
				if($sto != "-"){
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";
				}else{
					$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";
				}

			}else{
				$criteria->condition = "ms.reg = '".$regional."' and ms.witel_versi_tactical = '".$witel."' and t.sto = '".$sto."'  and t.isactive = 'Y' and migrasi not in(11,12,14,15,9) and status_approve = 1";
			}
			$criteria->condition .= " and t.filter_reg = 'Y' ";
			$criteria->order = "t.create_dtm desc";

			$criteria->compare('t.sto',$this->sto,true);
			$criteria->compare('ms.reg',$this->regional,true);
			// $criteria->compare('ta3.ta_area',$this->nama_fiberzone,true);
			$criteria->compare('ms.witel_versi_tactical',$this->witel,true);
			$criteria->compare('t.create_dtm',$this->create_dtm,true);
			// $criteria->compare('t.email_pelanggan',$this->email_pelanggan,true);
			// $criteria->compare('t.id_mitra',$this->id_mitra,true);
			$criteria->compare('t.nama_pelanggan',$this->nama_pelanggan,true);
			$criteria->compare('t.no_wo',$this->no_wo,true);
			// $criteria->compare('t.no_telp',$this->no_telp,true);
			$criteria->compare('t.no_kontak',$this->no_kontak,true);
			// $criteria->compare('t.no_inet',$this->no_inet,true);
		 	// $criteria->compare('t.psb',$this->jenis_layanan,true);
			// $criteria->compare('t.nik',$this->nik,true);
			// $criteria->compare('ms.reg',$this->reg_tactical,true);
			// $criteria->compare('ms.witel_versi_tactical',$this->witel_tactical,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,

				));
		}


	public function excel_material_reg($reg,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{
		$criteria = new CDbCriteria();

		$criteria->select = 't.no_wo,
												 b.nama_barang,
												 d.jml_pemakaian';

		$criteria->join 	='LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id
												INNER JOIN t_pemakaian_detail d on t.no = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang';

		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND ta4.id = "'.$reg.'"  and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND ta4.id = "'.$reg.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}else
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND ta4.id = "'.$reg.'" and t.isactive = "Y" ';
		}

		$criteria->order ='t.no_wo';

		$data = $this->findAll($criteria);
		return $data;
	}

	public function excel_material_fz($fz,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{
		$criteria = new CDbCriteria();

		$criteria->select = 't.no_wo,
												 b.nama_barang,
												 d.jml_pemakaian';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id
												INNER JOIN t_pemakaian_detail d on t.no = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang';

	if($tipe_teknisi == 'mitra')
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" AND t.fiberzone = "'.$fz.'" and t.isactive = "Y"';
	}
	elseif($tipe_teknisi == 'telkom_akses')
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}
	else
	{
		$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'"  AND t.fiberzone = "'.$fz.'" and t.isactive = "Y" ';
	}

		$criteria->order ='t.no_wo';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function excel_material_witel($witel,$tgl_mulai,$tgl_selesai,$tipe_teknisi)

	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.no_wo,
												 b.nama_barang,
												 d.jml_pemakaian';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id
												INNER JOIN t_pemakaian_detail d on t.no = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang';

		if($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses"  AND t.witel ="'.$witel.'" and t.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}
		else
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" AND t.witel ="'.$witel.'" and t.isactive = "Y" ';
		}

		$criteria->order ='t.no_wo';

		$data = $this->findAll($criteria);
		return $data;
	}


	public function excel_material_sto($sto,$tgl_mulai,$tgl_selesai,$tipe_teknisi)
	{

		$criteria = new CDbCriteria();
		$criteria->select = 't.no_wop,
												 b.nama_barang,
												 d.jml_pemakaian';

		$criteria->join 	= 'LEFT JOIN naker.area_ta ta4 ON t.regional = ta4.id LEFT JOIN naker.area_ta ta3 ON t.fiberzone = ta3.id LEFT JOIN naker.area_ta ta2 ON t.witel = ta2.id
												INNER JOIN t_pemakaian_detail d on t.no = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang';

		if($tipe_teknisi == 'mitra' && $sto ='all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}
		elseif($tipe_teknisi == 'mitra')
		{
			$criteria->condition = 't.sto = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra != "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'telkom_akses' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}
		elseif($tipe_teknisi == 'telkom_akses')
		{
			$criteria->condition = 't.sto = "'.$sto.'"  AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.id_mitra = "Telkom Akses" and t.isactive = "Y" ';
		}

		if($tipe_teknisi == 'all_teknisi' && $sto == 'all_sto')
		{
			$criteria->condition = 'date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}
		elseif($tipe_teknisi == 'all_teknisi')
		{
			$criteria->condition = 't.sto = "'.$sto.'" AND  date_format(create_dtm, "%Y-%m-%d") between "'.$tgl_mulai.'" and "'.$tgl_selesai.'" and t.isactive = "Y"';
		}

		$criteria->order ='t.no_wo';
		$data = $this->findAll($criteria);
		return $data;

	}

	public function Uploadfile($no_wo)
	{

		$img = CHtml::image('images/download.png', 'Download');
		$link = CHtml::link($img, array('report/downloadfile','no_wo'=>$no_wo), array('target'=>'_blank'));

		return $link;
	}

	public function viewMaterial($id)
	{
		$id = "'$id'";
		$link1 = '<small><center><button type="button" class="btn btn-warning btn-xs" onclick="viewmaterial('.$id.')"><i class="fa fa-pencil"></i></button></center></small>';
		return $link1;

	}

	public function actionInsertReturn(){
		$return = new AmaliaReturn();
		$return->id_return = 1;
		$return->detil_return = "kurang bagus";
		$return->returned_by ="9555139";
		$return->save();
	}

	public function dataMaterials($no_wo){

			$criteria = new CDbCriteria();
			$criteria->select 		= 't.no_wo,b.nama_barang,d.jml_pemakaian,d.id_barang';
			$criteria->join 		= 'INNER JOIN t_pemakaian_detail d on t.no = d.pemakaian_id INNER JOIN t_master_barang b on d.id_barang = b.id_barang';
			$criteria->condition 	= 't.no_wo ="'.$no_wo.'" and t.isactive = "Y"';

			$data = $this->findAll($criteria);
			return $data;
	}


	public function dataPemakaianRow($no_wo){


			$criteria = new CDbCriteria();
			$criteria->select 		= 't.*';
			$criteria->condition 	= 't.no_wo ="'.$no_wo.'" and t.isactive = "Y"';
			$data = $this->findAll($criteria);
			return $data;

	}

	public function eksistComment($no_wo){

			$criteria = new CDbCriteria();
			// $criteria->select 		= 't.no_wo,ar.id_return1,ar.id_return2,ar.id_return3,ar.created_date,ar.detil_return,meta.komentar,meta.date_finish_update,ar.fix_komentar,ar.fix_date';
			$criteria->select 		= 't.no_wo,ar.id_return1,ar.id_return2,ar.id_return3,ar.created_date,ar.detil_return,meta.komentar,ar.fix_komentar,ar.fix_date';
			$criteria->join 		= 'INNER JOIN amalia_return ar on t.no_wo = ar.no_wo INNER JOIN detil_meta_data_amalia meta on t.no_wo = meta.no_wo';
			$criteria->condition 	= 't.no_wo ="'.$no_wo.'" and t.isactive = "Y" and ar.status = 2';

			$data = $this->findAll($criteria);
			return $data;
	}

	public function dataUkurApprove($no_wo){
		$criteria = new CDbCriteria();
		$criteria->select 		= 't.ukur';
		$criteria->condition 	= 't.no_wo ="'.$no_wo.'" and t.isactive = "Y"';
		$data = $this->findAll($criteria);
		$turn_on_off = 0;
		foreach($data as $d){
			$turn_on_off = $d->ukur;
		}
		return $turn_on_off;
	}
	public function dataUkurChanel($no_wo){
		$criteria = new CDbCriteria();
		$criteria->select 		= 't.last_channel';
		$criteria->condition 	= 't.no_wo ="'.$no_wo.'" and t.isactive = "Y"';
		$data = $this->findAll($criteria);
		//$turn_on_off = 0;
		foreach($data as $d){
			$turn_on_off = $d->last_channel;
		}
		return $turn_on_off;
	}

	public function dataUkurRegister($no_wo){
		$criteria = new CDbCriteria();
		$criteria->select 		= 't.register_voice';
		$criteria->condition 	= 't.no_wo ="'.$no_wo.'" and t.isactive = "Y"';
		$data = $this->findAll($criteria);
		//$turn_on_off = 0;
		foreach($data as $d){
			$turn_on_off = $d->register_voice;
		}
		return $turn_on_off;
	}

	public function dataInboxTl($nik){

		$tl  = new Tl();

		$no = 0;
		$in = "";
		foreach($tl->isTl($nik) as $t){
			if($no == 0){
				$in .="'".$t->WORKZONE."'";
			}else{
				$in .=",'".$t->WORKZONE."'";
			}
			$no++;
		}


		$criteria = new CDbCriteria();
		$need_validate = 0;
		$return_inbox = 0;

		$criteria->select 		= 'sum(if(t.status_approve = 0,1,0))AS need_validate,
								   sum(if(t.status_approve < 0,1,0))AS return_inbox';
		$criteria->join         = 'right join api.master_sto ms on t.sto = ms.sto';
		$criteria->condition 	= 't.isactive = "Y" and t.sto in ('.$in.') and t.migrasi not in (11,12,14,15,9)';

		$datas = $this->findAll($criteria);

		foreach($datas as $d){
			$need_validate   = $d->need_validate;
			$return_inbox 	 = $d->return_inbox;
		}

		return json_encode(array("need_validate"=>$need_validate,"return"=>$return_inbox));
	}

	public function dataInboxAso($nik){

		$criteria = new CDbCriteria();
		$user   		= new User();
		$data = $user->getDataUserOneRow($nik);

		$need_validate = 0;
		$return_inbox = 0;
		$need_approve = 0;

		foreach ($data as $k ) {

			$lokasi = $k->lokasi;
			$criteria->select 		= 'sum(if(t.status_approve = 1,1,0))AS need_approve';
			$criteria->join         = 'right join api.master_sto ms on t.sto = ms.sto';
			if($lokasi == "all"){
				$criteria->condition 	= 't.isactive = "Y" and  migrasi not in(11,12,14,15,9)';
			}else{
				$criteria->condition 	= 'migrasi not in(11,12,14,15,9) and t.isactive = "Y" and (t.sto = "'.$lokasi.'" OR ms.witel_versi_tactical = "'.$lokasi.'" OR ms.reg = "'.$lokasi.'")';
			}

			$datas = $this->findAll($criteria);

			foreach($datas as $d){
				$need_approve   = $d->need_approve ;
			}

		}

		return json_encode(array("need_approve"=>$need_approve));
	}

	// public function getjarak($odp,$pelanggan){
	// 		$json = file_get_contents('https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf624858fb49c3aa14483f9f68cf0686672a8f&start=8.681495,49.41461&end=8.687872,49.420318', false);
	// 		return $json;
	// }


}
