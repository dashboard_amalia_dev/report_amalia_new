<?php

/**
 * This is the model class for table "master_sto".
 *
 * The followings are the available columns in table 'master_sto':
 * @property integer $id
 * @property string $sto
 * @property string $witel_versi_tactical
 * @property string $witel_versi_kpro
 * @property string $teritory
 * @property string $reg
 * @property integer $id_witel
 * @property integer $id_teritory
 * @property integer $id_reg
 */
class LayananAmalia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mapping_layanan_amalia';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db;
	}

	public function layanan($layanan)
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition ='t.layanan = '.$layanan;
		$data = $this->findAll($criteria);
		$is_voice = false;

		if(count($data) > 0){
			$is_voice = true;
		}
		return $data;
	}

	public function layananPsb($layanan)
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition ='t.layanan = '.$layanan.' AND is_psb = 1';
		$data = $this->findAll($criteria);
		$is_voice = false;

		if(count($data) > 0){
			$is_voice = true;
		}
		return $data;
	}

	public function layananMigrasi($layanan)
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition ='t.layanan = '.$layanan.' AND is_migrasi = 1';
		$data = $this->findAll($criteria);
		$is_voice = false;

		if(count($data) > 0){
			$is_voice = true;
		}
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterSto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
