<?php

/**
 * This is the model class for table "master_sto".
 *
 * The followings are the available columns in table 'master_sto':
 * @property integer $id
 * @property string $sto
 * @property string $witel_versi_tactical
 * @property string $witel_versi_kpro
 * @property string $teritory
 * @property string $reg
 * @property integer $id_witel
 * @property integer $id_teritory
 * @property integer $id_reg
 */
class MasterSto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_sto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_witel, id_teritory, id_reg', 'numerical', 'integerOnly'=>true),
			array('sto', 'length', 'max'=>10),
			array('witel_versi_tactical, witel_versi_kpro, teritory, reg', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sto, witel_versi_tactical, witel_versi_kpro, teritory, reg, id_witel, id_teritory, id_reg', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sto' => 'Sto',
			'witel_versi_tactical' => 'Witel Versi Tactical',
			'witel_versi_kpro' => 'Witel Versi Kpro',
			'teritory' => 'Teritory',
			'reg' => 'Reg',
			'id_witel' => 'Id Witel',
			'id_teritory' => 'Id Teritory',
			'id_reg' => 'Id Reg',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sto',$this->sto,true);
		$criteria->compare('witel_versi_tactical',$this->witel_versi_tactical,true);
		$criteria->compare('witel_versi_kpro',$this->witel_versi_kpro,true);
		$criteria->compare('teritory',$this->teritory,true);
		$criteria->compare('reg',$this->reg,true);
		$criteria->compare('id_witel',$this->id_witel);
		$criteria->compare('id_teritory',$this->id_teritory);
		$criteria->compare('id_reg',$this->id_reg);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_api;
	}

	public function get_fzt()
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.id_teritory,t.teritory';
		$criteria->group ='t.teritory';
		$criteria->condition ='t.id_teritory is not null';
		$result = array();
		$data = $this->findAll($criteria);
		if(!empty($data)){
			foreach ($data as $value) {
				// code...
				$result[$value->id_teritory] = $value->teritory;
			}
		}

		return $result;
	}

	public function get_witel(){
		$criteria = new CDbCriteria();
		$criteria->select = 't.id_witel,t.witel_versi_tactical';
		$criteria->group ='t.id_witel';
		$criteria->condition ='t.id_witel is not null';
		$result = array();
		$data = $this->findAll($criteria);
		if(!empty($data)){
			foreach ($data as $value) {
				// code...
				$result[$value->id_witel] = $value->witel_versi_tactical;
			}
		}

		return $result;
	}


	public function isWitel($witel){
		$criteria = new CDbCriteria();
		$criteria->select = 't.witel_versi_tactical,t.reg,t.sto';
		$criteria->group ='t.witel_versi_tactical,t.reg,t.sto';
		$criteria->condition ='t.witel_versi_tactical is not null and t.witel_versi_tactical = "'.$witel.'"';
		$data = $this->findAll($criteria);
		return $data;
	}


	public function isRegional($regional){
		$criteria = new CDbCriteria();
		$criteria->select = 't.reg,t.witel_versi_tactical,t.sto';
		$criteria->group ='t.witel_versi_tactical,t.reg,t.sto';
		$criteria->condition ='t.reg is not null and t.reg = "'.$regional.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function isSto($sto){
		$criteria = new CDbCriteria();
		$criteria->select = 't.sto,t.reg,t.witel_versi_tactical';
		$criteria->group ='t.witel_versi_tactical,t.reg,t.sto';
		$criteria->condition ='t.sto is not null and t.sto = "'.$sto.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getReg($reg,$changed){

		$criteria = new CDbCriteria();

		$nik = Yii::app()->session['nik'];
		$user = User::model()->IsAso($nik);
		$detil = User::model()->getDataUserOneRow($nik);
		$level = User::model()->getLevel($nik);
		$no = 0;
		$in = "";
		$da = "";
		if(count($user) == 0){
			$user = User::model()->checkUser($nik);
		}
		foreach($user as $u){
			// if($level == 2){

				$is_regional = MasterSto::model()->isRegional($u->lokasi);
				$is_witel = MasterSto::model()->isWitel($u->lokasi);
				$is_sto = MasterSto::model()->isSto($u->lokasi);

				if(count($is_regional) > 0){

					foreach ($is_regional as $i) {
						if($no == 0){
							$in = "'".$i->reg."'";
						}else{
							$in .= ",'".$i->reg."'";
						}
						$no++;
					}

				}

				if(count($is_witel) > 0){

					foreach ($is_witel as $w) {
						// return $w->reg;
						if($no == 0){
							$in = "'".$w->reg."'";
						}else{
							$in .= ",'".$w->reg."'";
						}
						$no++;
					}

				}

				if(count($is_sto) > 0){
					foreach ($is_sto as $i) {
						if($no == 0){
							$in = "'".$i->reg."'";
						}else{
							$in .= ",'".$i->reg."'";
						}
						$no++;
					}
				}
			// }else{
			// 	if(count($is_reg) > 0){
			// 		foreach ($is_reg as $i) {
			// 			if($i->witel_versi_tactical == $wit){
			// 				if($no == 0){
			// 					$in = "'".$i->sto."'";
			// 				}else{
			// 					$in .= ",'".$i->sto."'";
			// 				}
			// 			}

			// 			$no++;
			// 		}
			// 	}
			// }
		}

		// return $da;

		$criteria->select = 't.reg';
		$criteria->group ='t.reg';
		$criteria->order = 't.reg asc';

		if($level == 1){
			$criteria->condition ='t.reg is not null';
		}else{
			$criteria->condition ='t.reg is not null and t.reg in ('.$in.')';
		}


		$data = $this->findAll($criteria);
		return $data;
	}

	public function getWitelNew($reg,$witel,$changed){
		$criteria = new CDbCriteria();

		$nik = Yii::app()->session['nik'];
		$user = User::model()->IsAso($nik);
		if(count($user) == 0){
			$user = User::model()->UserTa($nik);
		}

		$detil = User::model()->getDataUserOneRow($nik);
		$level = User::model()->getLevel($nik);
		$in = "";
		$no = 0;
		$is_witel = 0;
		if(count($user) == 0){
			$user = User::model()->checkUser($nik);
		}
		foreach($user as $u){

			$is_regional = MasterSto::model()->isRegional($u->lokasi);
			$is_witel = MasterSto::model()->isWitel($u->lokasi);
			$is_sto = MasterSto::model()->isSto($u->lokasi);

				if(count($is_witel) > 0){
					foreach ($is_witel as $i) {
						if($no == 0){
							$in = "'".$i->witel_versi_tactical."'";
						}else{
							$in .= ",'".$i->witel_versi_tactical."'";
						}
						$no++;
					}

				}

				if(count($is_sto) > 0){
					foreach ($is_sto as $i) {
						if($no == 0){
							$in = "'".$i->witel_versi_tactical."'";
						}else{
							$in .= ",'".$i->witel_versi_tactical."'";
						}
						$no++;
					}
				}


				if($in != ""){
					$jml_count = count(MasterSto::model()->checkWitelFromRegionalIn($reg,$in));
				}


		}

		$criteria->select = 't.id_witel,t.witel_versi_tactical';
		$criteria->group ='t.witel_versi_tactical';

		if($in != "" && $jml_count > 0){
			$criteria->condition ='t.witel_versi_tactical is not null and t.reg = "'.$reg.'" and t.witel_versi_tactical in('.$in.')';
		}else if($reg != "-"){
			$criteria->condition ='t.witel_versi_tactical is not null and t.reg = "'.$reg.'"';
		}else if((count($is_witel) < 1 || $level == 1) && ($in == "")){
			$criteria->condition ='t.witel_versi_tactical is not null';
		}else{
			$criteria->condition ='t.witel_versi_tactical is not null and t.witel_versi_tactical in ('.$in.')';
		}

		$data = $this->findAll($criteria);
		return $data;
	}

	public function getDataSto($reg,$wit,$sto,$changed){
		$criteria = new CDbCriteria();

		$nik = Yii::app()->session['nik'];
		$user = User::model()->IsAso($nik);
		if(count($user) == 0){
			$user = User::model()->UserTa($nik);
		}
		$detil = User::model()->getDataUserOneRow($nik);
		$level = User::model()->getLevel($nik);
		$arr_sto =  array();

		$no = 0;
		$in = "";
		$is_witel = 0;
		$witel_awal = "";
		if(count($user) == 0){
			$user = User::model()->checkUser($nik);
		}
		foreach($user as $u){
			$is_sto = MasterSto::model()->isSto($u->lokasi);

				foreach ($is_sto as $i) {
						if($no == 0){
							$in = "'".$i->sto."'";
						}else{
							$in .= ",'".$i->sto."'";
						}
						$no++;
				}
		}


		if($in != ""){
			$jml_count = count(MasterSto::model()->checkStoFromWitelIn($wit,$in));
		}

		$criteria->select = 't.id,t.sto';
		$criteria->group ='t.sto';


		if($in != "" && $jml_count > 0){
			$criteria->condition ='t.sto is not null and t.witel_versi_tactical = "'.$wit.'" and t.sto in('.$in.')';
		}else if($changed != "-"){
			$criteria->condition ='t.sto is not null and t.witel_versi_tactical = "'.$wit.'" ';
		}else if($wit != "-"){
			$criteria->condition ='t.sto is not null and t.witel_versi_tactical = "'.$wit.'" ';
		}else{
			// $criteria->condition ='t.sto is not null';
			$criteria->condition ='t.sto is not null and t.sto in ("")';
		}
		$data = $this->findAll($criteria);
		return $data;
	}

	public function get_sto_new($reg,$witel,$sto,$level){
		$criteria = new CDbCriteria();
		$criteria->select = 't.sto';
		$criteria->order ='t.sto asc';
		$result = array();
		$data = $this->findAll($criteria);
		if(!empty($data)){
			foreach ($data as $value) {
				// code...
				$result[$value->sto] = $value->sto;
			}
		}
		return $result;
	}

	public function get_sto(){
		$criteria = new CDbCriteria();
		$criteria->select = 't.sto';
		$criteria->order ='t.sto asc';
		//$criteria->group ='t.id_witel';
		$result = array();
		$data = $this->findAll($criteria);
		if(!empty($data)){
			foreach ($data as $value) {
				// code...
				$result[$value->sto] = $value->sto;
			}
		}

		return $result;
	}

	public function checkWitelFromRegional($regional){

		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->group ='t.witel_versi_tactical';
		if($regional != "-"){
			$criteria->condition ='t.witel_versi_tactical is not null and t.reg = "'.$regional.'"';
		}else{
			$criteria->condition ='t.witel_versi_tactical is not null';
		}

		$result = array();
		$data = $this->findAll($criteria);
		return $data;
	}

	public function checkWitelFromRegionalIn($regional,$in){

		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->group ='t.witel_versi_tactical';
		$criteria->condition ='t.witel_versi_tactical is not null and t.reg = "'.$regional.'" and t.witel_versi_tactical in('.$in.')';
		$result = array();
		$data = $this->findAll($criteria);
		return $data;

	}

	public function checkStoFromWitel($witel){

		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->group ='t.sto';
		$criteria->condition ='t.sto is not null and t.witel_versi_tactical = "'.$witel.'"';
		$result = array();
		$data = $this->findAll($criteria);
		return $data;

	}

	public function checkStoFromWitelIn($witel,$in){

		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->group ='t.sto';
		$criteria->condition ='t.sto is not null and t.witel_versi_tactical = "'.$witel.'" and t.sto in('.$in.')';
		$result = array();
		$data = $this->findAll($criteria);
		return $data;

	}

	public function check_reg($reg){
		$criteria = new CDbCriteria();
		$criteria->select = 't.reg';
		$criteria->group ='t.reg';
		$criteria->order = 't.reg asc';
		$criteria->condition ='t.reg is not null and t.reg = "'.$reg.'"';
		$result = array();
		$data = $this->findAll($criteria);
		return $data;
	}

	public function check_wit($wit){
		$criteria = new CDbCriteria();
		$criteria->select = 't.witel_versi_tactical,t.reg';
		$criteria->group ='t.witel_versi_tactical';
		$criteria->order = 't.witel_versi_tactical asc';
		$criteria->condition ='t.witel_versi_tactical is not null and t.witel_versi_tactical = "'.$wit.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function check_st($st){
		$criteria = new CDbCriteria();
		$criteria->select = 't.sto';
		$criteria->group ='t.sto';
		$criteria->order = 't.sto asc';
		$criteria->condition ='t.sto is not null and t.sto = "'.$st.'"';
		$result = array();
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getDataRegionalOneRow($lokasi){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition ='t.reg = "'.$lokasi.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getDataWitelOneRow($lokasi){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition ='t.witel_versi_tactical = "'.$lokasi.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getDataStoOneRow($lokasi){
		$criteria = new CDbCriteria();
		$criteria->select = 't.*';
		$criteria->condition ='t.sto = "'.$lokasi.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function isReg2($lokasi){
		if ($lokasi == 'REG 2') {
			return 1;
		}elseif ($lokasi == 'All Reg') {
			return 3;
		}else {
			$criteria = new CDbCriteria();
			$criteria->select = 't.reg,t.witel_versi_tactical,t.sto';
			$criteria->condition ='t.reg is not null and t.reg = "REG 2" AND t.witel_versi_tactical= "'.$lokasi.'"';
			$data = $this->findAll($criteria);
			if (count($data) > 0) {
				return 1;
			}else {
				$criteria = new CDbCriteria();
				$criteria->select = 't.reg,t.witel_versi_tactical,t.sto';
				$criteria->condition ='t.reg is not null and t.reg = "REG 2" AND t.sto= "'.$lokasi.'"';
				$data = $this->findAll($criteria);
				// if (count($data)>0) {
				// 	$criteria = new CDbCriteria();
				// 	$criteria->select = 't.reg,t.witel_versi_tactical,t.sto';
				// 	$criteria->condition ='t.reg is not null and t.reg = "REG 2" AND t.sto= "'.$lokasi.'"';
				// 	$data = $this->findAll($criteria);
					if (count($data)>0) {
						return 1;
					}else {
						return 0;
					}
				//}
			}
			//return $data;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterSto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
