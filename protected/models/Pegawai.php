<?php

/**
 * This is the model class for table "t_master_employee_ta".
 *
 * The followings are the available columns in table 't_master_employee_ta':
 * @property string $nik
 * @property string $nama
 * @property string $tgl_lahir
 * @property integer $id_kota_lahir
 * @property integer $id_agama
 * @property integer $id_jk
 * @property string $jalan
 * @property string $kota
 * @property integer $id_suku
 * @property integer $id_status_nikah
 * @property string $tgl_mulai_bekerja
 * @property string $tgl_calon_pegawai
 * @property string $tgl_pegprus
 * @property string $tgl_perkiraan_pensiun
 * @property integer $id_divisi
 * @property string $tgl_masuk_divisi
 * @property integer $objectid_posisi
 * @property string $tgl_object_id
 * @property string $tgl_band_posisi
 * @property integer $id_bpp
 * @property string $tgl_fas_perumahan
 * @property string $tgl_selesai_fas_perumahan
 * @property integer $id_masa_kerja
 * @property integer $id_kelompok_usia
 * @property integer $id_status_kerja
 * @property string $tgl_status_kerja
 * @property integer $id_employee_group
 * @property integer $id_employee_sub_group
 * @property string $gadas
 * @property string $tgl_gadas
 * @property string $tudas
 * @property string $tgl_tudas
 * @property string $tgl_tupos
 * @property integer $tahun_ski
 * @property string $nilai_ski
 * @property integer $tahun_kompetensi
 * @property string $nilai_kompetensi
 * @property integer $id_level_pendidikan
 * @property string $tgl_level_pendidikan
 * @property string $nama_pendidikan
 * @property string $penyelenggara_pendidikan
 * @property string $nilai_b
 * @property string $tahun_b
 * @property integer $adt_kesehatan
 * @property string $id_susunan_kel
 */
class Pegawai extends CActiveRecord {

    public $jnsuser;

    public function getDbConnection() {
        return Yii::app()->dbpegawai;
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Pegawai the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 't_master_employee_ta';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nik, id_employee_sub_group', 'required'),
            array('id_kota_lahir, id_agama, id_jk, id_suku, id_status_nikah, id_divisi, objectid_posisi, id_bpp, id_masa_kerja, id_kelompok_usia, id_status_kerja, id_employee_group, id_employee_sub_group, tahun_ski, tahun_kompetensi, id_level_pendidikan, adt_kesehatan', 'numerical', 'integerOnly' => true),
            array('nik', 'length', 'max' => 8),
            array('nama, jalan, kota', 'length', 'max' => 50),
            array('gadas, tudas', 'length', 'max' => 20),
            array('nilai_ski, nilai_kompetensi, nilai_b', 'length', 'max' => 3),
            array('nama_pendidikan', 'length', 'max' => 100),
            array('penyelenggara_pendidikan', 'length', 'max' => 300),
            array('tahun_b', 'length', 'max' => 4),
            array('id_susunan_kel', 'length', 'max' => 2),
            array('tgl_lahir, tgl_mulai_bekerja, tgl_calon_pegawai, tgl_pegprus, tgl_perkiraan_pensiun, tgl_masuk_divisi, tgl_object_id, tgl_band_posisi, tgl_fas_perumahan, tgl_selesai_fas_perumahan, tgl_status_kerja, tgl_gadas, tgl_tudas, tgl_tupos, tgl_level_pendidikan', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('nik, nama, tgl_lahir, id_kota_lahir, id_agama, id_jk, jalan, kota, id_suku, id_status_nikah, tgl_mulai_bekerja, tgl_calon_pegawai, tgl_pegprus, tgl_perkiraan_pensiun, id_divisi, tgl_masuk_divisi, objectid_posisi, tgl_object_id, tgl_band_posisi, id_bpp, tgl_fas_perumahan, tgl_selesai_fas_perumahan, id_masa_kerja, id_kelompok_usia, id_status_kerja, tgl_status_kerja, id_employee_group, id_employee_sub_group, gadas, tgl_gadas, tudas, tgl_tudas, tgl_tupos, tahun_ski, nilai_ski, tahun_kompetensi, nilai_kompetensi, id_level_pendidikan, tgl_level_pendidikan, nama_pendidikan, penyelenggara_pendidikan, nilai_b, tahun_b, adt_kesehatan, id_susunan_kel', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'nik' => 'Nik',
            'nama' => 'Nama',
            'tgl_lahir' => 'Tgl Lahir',
            'id_kota_lahir' => 'Id Kota Lahir',
            'id_agama' => 'Id Agama',
            'id_jk' => 'Id Jk',
            'jalan' => 'Jalan',
            'kota' => 'Kota',
            'id_suku' => 'Id Suku',
            'id_status_nikah' => 'Id Status Nikah',
            'tgl_mulai_bekerja' => 'Tgl Mulai Bekerja',
            'tgl_calon_pegawai' => 'Tgl Calon Pegawai',
            'tgl_pegprus' => 'Tgl Pegprus',
            'tgl_perkiraan_pensiun' => 'Tgl Perkiraan Pensiun',
            'id_divisi' => 'Id Divisi',
            'tgl_masuk_divisi' => 'Tgl Masuk Divisi',
            'objectid_posisi' => 'Objectid Posisi',
            'tgl_object_id' => 'Tgl Object',
            'tgl_band_posisi' => 'Tgl Band Posisi',
            'id_bpp' => 'Id Bpp',
            'tgl_fas_perumahan' => 'Tgl Fas Perumahan',
            'tgl_selesai_fas_perumahan' => 'Tgl Selesai Fas Perumahan',
            'id_masa_kerja' => 'Id Masa Kerja',
            'id_kelompok_usia' => 'Id Kelompok Usia',
            'id_status_kerja' => 'Id Status Kerja',
            'tgl_status_kerja' => 'Tgl Status Kerja',
            'id_employee_group' => 'Id Employee Group',
            'id_employee_sub_group' => 'Id Employee Sub Group',
            'gadas' => 'Gadas',
            'tgl_gadas' => 'Tgl Gadas',
            'tudas' => 'Tudas',
            'tgl_tudas' => 'Tgl Tudas',
            'tgl_tupos' => 'Tgl Tupos',
            'tahun_ski' => 'Tahun Ski',
            'nilai_ski' => 'Nilai Ski',
            'tahun_kompetensi' => 'Tahun Kompetensi',
            'nilai_kompetensi' => 'Nilai Kompetensi',
            'id_level_pendidikan' => 'Id Level Pendidikan',
            'tgl_level_pendidikan' => 'Tgl Level Pendidikan',
            'nama_pendidikan' => 'Nama Pendidikan',
            'penyelenggara_pendidikan' => 'Penyelenggara Pendidikan',
            'nilai_b' => 'Nilai B',
            'tahun_b' => 'Tahun B',
            'adt_kesehatan' => 'Adt Kesehatan',
            'id_susunan_kel' => 'Id Susunan Kel',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($ket=null) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('nik', $this->nik, true);
        $criteria->compare('nama', $this->nama, true);
        $criteria->compare('tgl_lahir', $this->tgl_lahir, true);
        $criteria->compare('id_kota_lahir', $this->id_kota_lahir);
        $criteria->compare('id_agama', $this->id_agama);
        $criteria->compare('id_jk', $this->id_jk);
        $criteria->compare('jalan', $this->jalan, true);
        $criteria->compare('kota', $this->kota, true);
        $criteria->compare('id_suku', $this->id_suku);
        $criteria->compare('id_status_nikah', $this->id_status_nikah);
        $criteria->compare('tgl_mulai_bekerja', $this->tgl_mulai_bekerja, true);
        $criteria->compare('tgl_calon_pegawai', $this->tgl_calon_pegawai, true);
        $criteria->compare('tgl_pegprus', $this->tgl_pegprus, true);
        $criteria->compare('tgl_perkiraan_pensiun', $this->tgl_perkiraan_pensiun, true);
        $criteria->compare('id_divisi', $this->id_divisi);
        $criteria->compare('tgl_masuk_divisi', $this->tgl_masuk_divisi, true);
        $criteria->compare('objectid_posisi', $this->objectid_posisi);
        $criteria->compare('tgl_object_id', $this->tgl_object_id, true);
        $criteria->compare('tgl_band_posisi', $this->tgl_band_posisi, true);
        $criteria->compare('id_bpp', $this->id_bpp);
        $criteria->compare('tgl_fas_perumahan', $this->tgl_fas_perumahan, true);
        $criteria->compare('tgl_selesai_fas_perumahan', $this->tgl_selesai_fas_perumahan, true);
        $criteria->compare('id_masa_kerja', $this->id_masa_kerja);
        $criteria->compare('id_kelompok_usia', $this->id_kelompok_usia);
        $criteria->compare('id_status_kerja', $this->id_status_kerja);
        $criteria->compare('tgl_status_kerja', $this->tgl_status_kerja, true);
        $criteria->compare('id_employee_group', $this->id_employee_group);
        $criteria->compare('id_employee_sub_group', $this->id_employee_sub_group);
        $criteria->compare('gadas', $this->gadas, true);
        $criteria->compare('tgl_gadas', $this->tgl_gadas, true);
        $criteria->compare('tudas', $this->tudas, true);
        $criteria->compare('tgl_tudas', $this->tgl_tudas, true);
        $criteria->compare('tgl_tupos', $this->tgl_tupos, true);
        $criteria->compare('tahun_ski', $this->tahun_ski);
        $criteria->compare('nilai_ski', $this->nilai_ski, true);
        $criteria->compare('tahun_kompetensi', $this->tahun_kompetensi);
        $criteria->compare('nilai_kompetensi', $this->nilai_kompetensi, true);
        $criteria->compare('id_level_pendidikan', $this->id_level_pendidikan);
        $criteria->compare('tgl_level_pendidikan', $this->tgl_level_pendidikan, true);
        $criteria->compare('nama_pendidikan', $this->nama_pendidikan, true);
        $criteria->compare('penyelenggara_pendidikan', $this->penyelenggara_pendidikan, true);
        $criteria->compare('nilai_b', $this->nilai_b, true);
        $criteria->compare('tahun_b', $this->tahun_b, true);
        $criteria->compare('adt_kesehatan', $this->adt_kesehatan);
        $criteria->compare('id_susunan_kel', $this->id_susunan_kel, true);

        if (!empty($ket)) {
            $hasil = $this->findAll($criteria);
            return $hasil;
        } else {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }
    }

    public function searchlistpegawai() {
        $criteria = new CDbCriteria;
        $criteria->select = 'nik, nama, nama_posisi, email';
        $criteria->condition = 'nik not like "1%" and nik not like "2%" and nik not like "3%"';
        $criteria->compare('nama', $this->nama, true);
        $criteria->limit = 3;
        $hasil = $this->findAll($criteria);
        return $hasil;
    }

    public function getNamaPegawai($nik){
      $nama = "";
      $conn = Yii::app()->db;
      $query = "SELECT nik, name FROM naker.master_employee WHERE nik = '".$nik."'";
      $cc = $conn->createCommand($query);
      $data = $cc->queryRow($cc);
      if(!empty($data)){
        $nama = $data['name'];
      }else{
        $query = "SELECT nik, name FROM naker.master_employee_mitra WHERE nik = '".$nik."'";
        $cc = $conn->createCommand($query);
        $data = $cc->queryRow($cc);
        if(!empty($data)){
          $nama = $data['name'];
        }
      }

      return $nama;

    }

    public function getPosisiPegawai($nik){
        $geturlapi = Alamatapi::model()->findByAttributes(array('menu' => 'datapegawai'));
        $urlapi = $geturlapi->url_api;
        $nama = "";
        $optsDataPemeriksa = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => 'nik=' . $nik
            )
        );

        $contextDataPemeriksa = stream_context_create($optsDataPemeriksa);
        $json = file_get_contents($urlapi, false, $contextDataPemeriksa);

        $result = json_decode($json, true);
        if (!empty($result)) {
            $nama = $result['nama_posisi'];
        }else{
		$nama = "-";
		}

        return $nama;
    }

    public function getRegionalWitelKaryawan($nik, $ket){
        $geturlapi = Alamatapi::model()->findByAttributes(array('menu' => 'datapegawai'));
        $urlapi = $geturlapi->url_api;
        $nama = "";
        $optsDataPemeriksa = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => 'nik=' . $nik
            )
        );

        $contextDataPemeriksa = stream_context_create($optsDataPemeriksa);
        $json = file_get_contents($urlapi, false, $contextDataPemeriksa);

        $result = json_decode($json, true);
        if (!empty($result)) {
            if($ket == 'regional'){
              $nama = $result['id_reg'];
            }else{
              $nama = $result['id_witel'];
            }
        }else{
          $nama = "-";
        }

        return $nama;
    }


    public function getPosisiPegawaiMgrSupport($nik){
        $geturlapi = Alamatapi::model()->findByAttributes(array('menu' => 'posisisupport'));
        $urlapi = $geturlapi->url_api;
        $nama = "";
        $optsDataPemeriksa = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => 'nik=' . $nik
            )
        );

        $contextDataPemeriksa = stream_context_create($optsDataPemeriksa);
        $json = file_get_contents($urlapi, false, $contextDataPemeriksa);

        $result = json_decode($json, true);
        if (!empty($result)) {
            $nama = $result['data_all']['pos_supp'];
        }

        return $nama;
    }

    public function getNikVpFinop() {
        $geturlapi = Alamatapi::model()->findByAttributes(array('menu' => 'vpfinop'));
        $urlapi = $geturlapi->url_api;
        $nama = "";
        $optsDataPemeriksa = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded'
            )
        );

        $contextDataPemeriksa = stream_context_create($optsDataPemeriksa);
        $json = file_get_contents($urlapi, false, $contextDataPemeriksa);

        $result = json_decode($json, true);
        if (!empty($result)) {
            $nama = $result['nik'];
        }

        return $nama;
    }

    public function getEmailPegawai($nik) {
        $geturlapi = Alamatapi::model()->findByAttributes(array('menu' => 'email'));
        $urlapi = $geturlapi->url_api;
        $email = file_get_contents($urlapi . '=' . $nik);
        if (!empty($email)) {
            $nama = $email;
        }

        return $nama;
    }

    public function cekPMProject($nik) {
        $cekpm = Projectpmleadsponsor::model()->findByAttributes(array('pm' => $nik));
        if (!empty($cekpm)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function cekMgrArea($nik) {
        $cekpm = Housewitel::model()->findByAttributes(array('manager_opr' => $nik));
        if (!empty($cekpm)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getNamaPM($pid) {
        $data = Project::model()->findByAttributes(array('project_id' => $pid));
        if (!empty($data)) {
            $idlocation = $data->id_location;
            $idgroupname = $data->id_group_name;
        }

        $getpm = Projectpmleadsponsor::model()->findByAttributes(array('id_location' => $idlocation, 'id_group_name' => $idgroupname));

        $nikpm = $getpm->pm;
        $namapm = Pegawai::model()->getNamaPegawai($nikpm);
        return $namapm;
    }

    public function getLinkViewEditUser($nik, $itemname){
      $linkview = CHtml::image('images/find.png', 'linkview', array('title'=>'Display / Edit User','style'=>'cursor:pointer','onclick'=>'viewuser("'.$nik.'", "'.$itemname.'")'));
      $link = '<center>'.$linkview.' '.$linkedit.'</center>';

      return $link;
    }

    public function getjnsinstalasi($nik){
      $hasil = array();
      $cekisteknisi = SmTlTeknisiTactical::model()->findByAttributes(array('nik_teknisi'=>$nik));
      if(!empty($cekisteknisi)){
        $hasil[] = 'Y';
      }

      $cekisteknisi = SmTlTeknisiTactical::model()->findByAttributes(array('nik_tl'=>$nik));
      if(!empty($cekisteknisi)){
        $hasil[] = 'Y';
      }

      if(!empty($hasil)){
        $ket = 'ops';
      }else{
        $ket = 'cons';
      }

      return $ket;
    }

    public function getInitiateName($nik){
      $nama = "";
      $nik = trim($nik);
      $geturlapi = Alamatapi::model()->findByAttributes(array('menu' => 'datapegawai'));
      $urlapi = $geturlapi->url_api;
      $optsDataPemeriksa = array('http' =>
          array(
              'method' => 'POST',
              'header' => 'Content-type: application/x-www-form-urlencoded',
              'content' => 'nik=' . $nik
          )
      );

      $contextDataPemeriksa = stream_context_create($optsDataPemeriksa);
      $json = file_get_contents($urlapi, false, $contextDataPemeriksa);

      $result = json_decode($json, true);
      if (!empty($result)) {
          $nama = $result['name'];
      }else{
          $datamitra = Mitra::model()->findByPk($nik);
          if(!empty ($datamitra)){
              $nama = $datamitra->nama_mitra;
          }else{
            $optsDataKaryawan = array('http' =>
                array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => 'nik='.$nik
                )
            );

            $contextDataKaryawan = stream_context_create($optsDataKaryawan);
            $urlapi = Alamatapi::model()->findByAttributes(array('menu'=>'getnikteknisitactical'));
            $json = file_get_contents($urlapi->url_api, false, $contextDataKaryawan);
            $result = json_decode($json, true);
            if(!empty($result)){
              for($i=0;$i<count($result);$i++){
                $nama = $result[$i]['nama'];
              }
            }else{
              $nama = "";
            }
          }
      }

      return $nama;
    }

    public function checkAbsensi($nik){
      // $contextDataKaryawan = stream_context_create($optsDataKaryawan);
      $urlapi = Alamatapi::model()->findByAttributes(array('menu'=>'check_absensi'));
      $text = file_get_contents($urlapi->url_api.'?nik='.$nik);
      return $text;
      // return $text;
    }

}
