<?php

/**
 * This is the model class for table "master_sto".
 *
 * The followings are the available columns in table 'master_sto':
 * @property integer $nik
 * @property string $reg
 * @property string $witel
 * @property string $sto
 * @property integer $level
 */
class Naker extends CActiveRecord
{
		public $id,$reg,$witel;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'area_ta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Nik',
			'reg' => 'Regional',
			'witel' => 'Witel',
			'sto' => 'Sto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('reg',$this->reg,true);
		$criteria->compare('witel',$this->witel,true);
		$criteria->compare('sto',$this->sto,true);
		$criteria->compare('level',$this->level,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));	
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_naker;
	}


	public function getDataRegional($regional){
		$criteria = new CDbCriteria();
		$criteria->select  	= 'distinct(ta4.ta_area) as reg,ta4.id ';
		$criteria->join 	= 'LEFT JOIN area_ta ta2 ON t.parent = ta2.ta_area LEFT JOIN area_ta ta3 ON ta2.parent = ta3.ta_area LEFT JOIN area_ta ta4 ON ta3.parent = ta4.ta_area';
					$criteria->condition = 'ta4.id != "" and ta4.id < 10';

		if($regional != ''){
		//	$criteria->condition = 'ta4.ta_area = "'.$regional.'"';
		}else{
		//	$criteria->condition = 'ta4.id != "" and ta4.id < 10';
		}
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getDataWitel($witel,$regional){
		$criteria = new CDbCriteria();
		$criteria->select  	= 'distinct
								ta4.id id_reg,
								ta4.ta_area regional,
								ta2.id id,
								ta2.ta_area witel';
		$criteria->join 	= 'INNER JOIN area_ta ta2
								ON t.parent = ta2.ta_area
								INNER JOIN area_ta ta3
								ON ta2.parent = ta3.ta_area
								INNER JOIN area_ta ta4
								ON ta3.parent = ta4.ta_area';
		$criteria->condition = 'ta4.ta_area = "'.$regional.'"';
		$criteria->order = "ta2.ta_area ASC";
		$data = $this->findAll($criteria);
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterSto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
