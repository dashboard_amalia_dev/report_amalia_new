<?php

/**
 * This is the model class for table "master_sto".
 *
 * The followings are the available columns in table 'master_sto':
 * @property integer $nik
 * @property string $reg
 * @property string $witel
 * @property string $sto
 * @property integer $level
 */
class master_employee extends CActiveRecord
{
		public $id,$reg,$witel;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_employee';
    }
    
    public function getDataTa($nik){
       $criteria=new CDbCriteria;
       $criteria->select = "t.*";
       return $criteria->findAll($criteria);
    }

}