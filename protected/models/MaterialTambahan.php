<?php

/**
 * This is the model class for table "master_sto".
 *
 * The followings are the available columns in table 'master_sto':
 * @property integer $id
 * @property string $sto
 * @property string $witel_versi_tactical
 * @property string $witel_versi_kpro
 * @property string $teritory
 * @property string $reg
 * @property integer $id_witel
 * @property integer $id_teritory
 * @property integer $id_reg
 */
class MaterialTambahan extends CActiveRecord
{
	public $satuan,$reg_tactical,$witel_tactical,$sto;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detil_material_tambahan_amalia';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db;
	}

	public function getMaterialTambahan($no_wo)
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.*,mt.satuan';
		$criteria->join  = "left join material_tambahan_amalia mt on t.designator = mt.designator";
		$criteria->condition ='t.no_wo = "'.$no_wo.'"';
		$data = $this->findAll($criteria);
		return $data;
	}

	public function getMaterialTambahanFromDashboard($regional,$witel,$sto,$date1,$date2)
	{
		$criteria = new CDbCriteria();
		$criteria->select = 't.*,mt.satuan satuan,p.reg_tactical,p.witel_tactical,p.sto';
		$criteria->join  = "left join material_tambahan_amalia mt on t.designator = mt.designator left join pemakaian p on t.no_wo = p.no_wo";
		
		$criteria->condition = " t.volume > 0 ";

		if($date1 != "-" && $date2 != "-" && $date1 != "" && $date2 != ""){
			$criteria->condition .= 'and date_format(p.create_dtm, "%Y-%m-%d") between "'.$date1.'" and "'.$date2.'" and p.isactive = "Y"';
		}

		if($regional != "-" && $witel !="-" && $sto != "-" ){
			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.witel_tactical = "'.$witel.'" and p.sto = "'.$sto.'" and date_format(p.create_dtm, "%Y-%m-%d") between "'.$date1.'" and "'.$date2.'" and p.isactive = "Y"';

		}else if($regional != "-" && $witel !="-" && $sto != "-" ){

			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.witel_tactical = "'.$witel.'" and p.sto = "'.$sto.'" and p.isactive = "Y"';

		}else if($regional != "-" && $witel !="-"  ){
			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.witel_tactical = "'.$witel.'"  and date_format(p.create_dtm, "%Y-%m-%d") between "'.$date1.'" and "'.$date2.'" and p.isactive = "Y"';

		}else if($regional != "-" && $witel !="-" ){
			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.witel_tactical = "'.$witel.'"  and p.isactive = "Y"';

		}else if($regional != "-" ){

			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and date_format(p.create_dtm, "%Y-%m-%d") between "'.$date1.'" and "'.$date2.'" and p.isactive = "Y"';

		}else if($regional != "-" ){

			$criteria->condition .= 'and p.reg_tactical = "'.$regional.'" and p.isactive = "Y"';

		}
		// $criteria->limit = 10;
 		$data = $this->findAll($criteria);
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterSto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
